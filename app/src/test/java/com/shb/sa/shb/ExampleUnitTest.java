package com.shb.sa.shb;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void divide_isCorrect() throws Exception {
        System.out.println((30 / 1000) * 100);
        assertEquals(4, 2 + 2);
    }

    @Test
    public void shift(){

        String s = "abcd";

        int left = 1;
        int right = 2;

        char[] chars = s.toCharArray();
        char[] newChar = new char[chars.length];
        for (int i = 0; i < chars.length; i ++){

            System.out.println(mod((i - left) % chars.length, chars.length));
            newChar[(i + right) % chars.length] = chars[i];
        }

        System.out.println(String.valueOf(newChar));
    }

    private int mod(int x, int n){
        int r = x % n;
        if (r < 0)
        {
            r += n;
        }
        return r;
    }
}