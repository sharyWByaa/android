package com.shb.sa.shb;


import com.google.gson.JsonObject;

import org.junit.Test;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import com.shb.sa.shb.REST.UserService;
import com.shb.sa.shb.REST.models.User;

/**
 * Created by mujtaba on 30/03/2017.
 */

public class RestUserServiceUnitTest {

    @Test
    public void create_user() throws Exception {

        final User user = new User();
        user.setUid("dsflllds");
        user.setPhoneNumber("8948sd843294923");
        user.setUsername("jjjjiiollldd");



        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://10.0.2.2:9000")
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();



        UserService service = retrofit.create(UserService.class);

        Call<JsonObject> userCall = service.createUser(user);

        System.out.println("Start" + "\n"  + userCall.request().toString());




       // userCall.execute(); //this work





        System.out.println("finished" + userCall.isExecuted());




    }

    @Test
    public void jwt_validation(){

        String jwt = "eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE0OTExNTY0NTgsImlzcyI6InNoYiIsInVpZCI6IjIzNDMyNDMiLCJwaG9uZV9udW1iZXIiOiI0MzU0MzI1MzQiLCJ1c2VybmFtZSI6Imhza2RmamRzYSJ9.wFAsXr0R5tc31IZL5lXKWkifm5wIZgb71IZi23R4cJs";

        /*
        JwtConsumer jwtConsumer = new JwtConsumerBuilder()
                .setRequireExpirationTime() // the JWT must have an expiration time
                .setMaxFutureValidityInMinutes(300) // but the  expiration time can't be too crazy
                .setAllowedClockSkewInSeconds(30) // allow some leeway in validating time based claims to account for clock skew
                .setRequireSubject() // the JWT must have a subject claim
                .setExpectedIssuer("shb") // whom the JWT needs to have been issued by
                .setExpectedAudience("Audience") // to whom the JWT is intended for
                .setVerificationKey(rsaJsonWebKey.getKey()) // verify the signature with the public key
                .build(); // create the JwtConsumer instance
                */
    }
}
