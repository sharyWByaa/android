package com.shb.sa.shb;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;

import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.iconics.context.IconicsContextWrapper;
import com.shb.sa.shb.model.Media;

public class ViewerActivity extends AppCompatActivity {

    Media mediaItem;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);


        setContentView(R.layout.activity_viewer);

        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);
        //getSupportActionBar().hide();


        ImageButton imgBtn = findViewById(R.id.cancel_btn);

        imgBtn.setImageDrawable(new IconicsDrawable(this)
                .icon(FontAwesome.Icon.faw_times)
                .color(Color.WHITE)
                .sizeDp(24));
        imgBtn.setOnClickListener((view)-> finish());

        final String media = getIntent().getExtras().getString("media");
        final int type = getIntent().getExtras().getInt("type");

        if (type == Media.TYPE_IMAGE)  mediaItem = getIntent().getExtras().getParcelable("mediaObj");

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Intent intent = new Intent();
                if (type == Media.TYPE_VIDEO) {
                    intent.putExtra("media", media);
                    setResult(111, intent);
                } else if (type == Media.TYPE_IMAGE){
                    intent.putExtra("mediaObj", mediaItem);
                    setResult(112, intent);
                }
                finish();

            }
        });

        fab.setImageDrawable(new IconicsDrawable(this)
                .icon(FontAwesome.Icon.faw_plus_square_o)
                .color(Color.WHITE)
                .sizeDp(48));





        if (type == Media.TYPE_VIDEO) getSupportFragmentManager().beginTransaction().replace(R.id.fragment, ViewerActivityFragment.newInstance(media)).commit();

        else if (type == Media.TYPE_IMAGE) getSupportFragmentManager().beginTransaction().replace(R.id.fragment, ViewerImageActivityFragment.newInstance(mediaItem)).commit();

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(IconicsContextWrapper.wrap(newBase));
    }
}
