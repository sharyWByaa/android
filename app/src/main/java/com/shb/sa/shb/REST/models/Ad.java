package com.shb.sa.shb.REST.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mujtaba on 09/06/2017.
 */

public class Ad implements Serializable,  Parcelable {

    @Override
    public String toString() {
        return "Ad{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", desc='" + desc + '\'' +
                ", price=" + price +
                ", medias=" + medias +
                ", pictures=" + pictures +
                ", user=" + user +
                ", category=" + category +
                ", latLng=" + latLng +
                ", created=" + created +
                '}';
    }

    private String id;
    private String title;
    @SerializedName("description")
    private String desc;
    private BigDecimal price;
    private List<Media> medias;
    private List<Media> pictures;
    private User user;
    private Category category;
    private LatLng latLng;

    private Long created;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }


    public List<Media> getMedias() {
        return medias;
    }

    public void setMedias(List<Media> medias) {
        this.medias = medias;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }

    public Ad(){

    }



    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Media> getPictures() {
        return pictures;
    }

    public void setPictures(List<Media> pictures) {
        this.pictures = pictures;
    }

    public Long getCreated() {
        return created;
    }

    public void setCreated(Long created) {
        this.created = created;
    }


    protected Ad(Parcel in) {
        id = in.readString();
        title = in.readString();
        desc = in.readString();
        price = (BigDecimal) in.readValue(BigDecimal.class.getClassLoader());
        if (in.readByte() == 0x01) {
            medias = new ArrayList<Media>();
            in.readList(medias, Media.class.getClassLoader());
        } else {
            medias = null;
        }
        if (in.readByte() == 0x01) {
            pictures = new ArrayList<Media>();
            in.readList(pictures, Media.class.getClassLoader());
        } else {
            pictures = null;
        }
        user = (User) in.readValue(User.class.getClassLoader());
        category = (Category) in.readValue(Category.class.getClassLoader());
        latLng = (LatLng) in.readValue(LatLng.class.getClassLoader());
        created = in.readByte() == 0x00 ? null : in.readLong();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(title);
        dest.writeString(desc);
        dest.writeValue(price);
        if (medias == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(medias);
        }
        if (pictures == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(pictures);
        }
        dest.writeValue(user);
        dest.writeValue(category);
        dest.writeValue(latLng);
        if (created == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeLong(created);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Ad> CREATOR = new Parcelable.Creator<Ad>() {
        @Override
        public Ad createFromParcel(Parcel in) {
            return new Ad(in);
        }

        @Override
        public Ad[] newArray(int size) {
            return new Ad[size];
        }
    };


}

