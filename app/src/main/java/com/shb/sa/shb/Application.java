package com.shb.sa.shb;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.multidex.MultiDex;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.anupcowkur.reservoir.Reservoir;
import com.anupcowkur.reservoir.ReservoirClearCallback;

import com.crashlytics.android.Crashlytics;
import com.danikula.videocache.HttpProxyCacheServer;

import com.facebook.common.logging.FLog;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.imagepipeline.core.ImagePipelineConfig;
import com.facebook.imagepipeline.listener.RequestListener;
import com.facebook.imagepipeline.listener.RequestLoggingListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.instagram.igdiskcache.IgDiskCache;
import com.marcinorlowski.fonty.Fonty;
import co.chatsdk.firebase.push.FirebasePushModule;

import com.nytimes.android.external.fs3.FileSystemPersister;
import com.nytimes.android.external.fs3.PathResolver;
import com.nytimes.android.external.fs3.filesystem.FileSystemFactory;
import com.nytimes.android.external.store3.base.impl.BarCode;
import com.nytimes.android.external.store3.base.impl.Store;
import com.nytimes.android.external.store3.base.impl.StoreBuilder;
import com.nytimes.android.external.store3.middleware.GsonParserFactory;
import com.shb.sa.shb.REST.ShbService;
import com.shb.sa.shb.REST.UserService;
import com.shb.sa.shb.REST.models.AdResult;
import com.shb.sa.shb.REST.models.Category;
import com.shb.sa.shb.REST.models.Favorite;
import com.shb.sa.shb.user.model.BarCodeWithCursor;
import com.shb.sa.shb.util.AndroidUtil;
import com.shb.sa.shb.util.UriAdapter;

import com.vanniktech.emoji.EmojiManager;
import com.vanniktech.emoji.ios.IosEmojiProvider;

import io.fabric.sdk.android.Fabric;
import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;

import javax.annotation.Nonnull;

import co.chatsdk.core.session.ChatSDK;
//import co.chatsdk.firebase.FirebaseModule;
import co.chatsdk.firebase.FirebaseModule;
//import co.chatsdk.firebase.push.FirebasePushModule;

import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.BufferedSource;

import static junit.framework.Assert.assertEquals;

public class Application extends android.app.Application {

	private Locale locale = null;
	private static IgDiskCache igCache;
	public static OkHttpClient client;
	private static Context context;
	private static boolean sIsChatActivityOpen = false;
	public final static String baseStoreageUrl = "https://firebasestorage.googleapis.com/v0/b/sharibaya-196013/o/";


	PathResolver<BarCodeWithCursor> pathResolver = new PathResolver<BarCodeWithCursor>() {
		@Nonnull
		@Override
		public String resolve(@Nonnull BarCodeWithCursor barCodeWithCursor) {
			if (barCodeWithCursor.getCity() == null)
			return "feature/everywhere/" + String.valueOf(barCodeWithCursor.hashCode());
			else return "feature/" +  barCodeWithCursor.getCity() + "/" + barCodeWithCursor.hashCode();
		}
	};


	private  PathResolver<BarCode> resolver = new PathResolver<BarCode>() {
		@Nonnull
		@Override
		public String resolve(@Nonnull BarCode key) {
			return key.getType() +"/"+ key.getKey().hashCode();
		}
	};
	private Store<Map<String,AdResult>, BarCodeWithCursor> featureStore;
	private Store<AdResult, BarCodeWithCursor> adStore;
	private Store<List<Favorite>, BarCode> favStore;
	private Map<String, Category> newCatList;
	private StringBuilder catsAsString;


	@Override
	protected void attachBaseContext(Context base) {
		super.attachBaseContext(base);
		MultiDex.install(this);
	}

	@Override
    public void onCreate() {
		super.onCreate();
		Fabric.with(this, new Crashlytics());
		//Fabric.with(this, new Crashlytics());

		/*
		if (LeakCanary.isInAnalyzerProcess(this)) {
			// This process is dedicated to LeakCanary for heap analysis.
			// You should not init your app in this process.
			return;
		/}
		*/
		//LeakCanary.install(this);
		context = getApplicationContext();
		Fonty.init(this)
				.regularTypeface("Dubai-Regular.ttf")
				.boldTypeface("Dubai-Bold.ttf");
		EmojiManager.install(new IosEmojiProvider());

		Cache cache = new Cache(new File(getBaseContext().getCacheDir(), "http-cache"), 10 * 1024 * 1024);
		client = new OkHttpClient.Builder()
				//.addNetworkInterceptor(REWRITE_RESPONSE_INTERCEPTOR.get())
				//.addInterceptor(OFFLINE_INTERCEPTOR.get())
				//.cache(cache)
				.build();

		Set<RequestListener> requestListeners = new HashSet<>();
		requestListeners.add(new RequestLoggingListener());
		ImagePipelineConfig configPip = com.shb.sa.shb.util.network.OkHttpImagePipelineConfigFactory
				.newBuilder(getApplicationContext(), client)
		.setRequestListeners(requestListeners)
    	.build();

		Fresco.initialize(this,configPip);
		FLog.setMinimumLoggingLevel(FLog.VERBOSE);


		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);

		Configuration config = getBaseContext().getResources().getConfiguration();




		try {
            final GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.registerTypeAdapter(Uri.class, new UriAdapter());
            final Gson gson = gsonBuilder.create();
			Reservoir.init(this, 2048, gson); //in bytes
		} catch (IOException e) {
			//failure
            Log.d("Mushy", e.getLocalizedMessage());
		}

       Reservoir.clearAsync(new ReservoirClearCallback() {
            @Override
            public void onSuccess() {
                try {
                    //assertEquals(0, Reservoir.bytesUsed());
                } catch (Exception e) {

                }
            }

            @Override
            public void onFailure(Exception e) {

            }
        });

		/*

		String lang = settings.getString(getString(R.string.pref_locale), "ar");
		if (! "".equals(lang) && ! config.locale.getLanguage().equals(lang))
		{
			locale = new Locale(lang);
			Locale.setDefault(locale);
			config.locale = locale;
			getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
		}
		*/

		// Create a new configuration
		co.chatsdk.core.session.Configuration.Builder builder = new co.chatsdk.core.session.Configuration.Builder(this);


		// Initialize the Chat SDK
		ChatSDK chatsdk = ChatSDK.initialize(builder.build());

		// Activate the Firebase module
		FirebaseModule.activate();

		FirebasePushModule.activateForFirebase();


		// init store
		initFeatureStore();
		intAdsCatStore();
		initFavStore();

		// init catList
		intCatList();



	}

	private HttpProxyCacheServer proxy;

	public static HttpProxyCacheServer getProxy(Context context) {
		Application app = (Application) context.getApplicationContext();
		return app.proxy == null ? (app.proxy = app.newProxy()) : app.proxy;
	}

	private HttpProxyCacheServer newProxy() {
		return new HttpProxyCacheServer.Builder(this)
				.cacheDirectory(AndroidUtil.getVideoCacheDir(this))
				.build();
	}

	public static FirebaseStorage getStorage(){
		FirebaseStorage storage = FirebaseStorage.getInstance("gs://sharibaya-196013");
		return storage;
	}

	public static boolean isChatActivityOpen() {
		return sIsChatActivityOpen;
	}

	public static void setChatActivityOpen(boolean isChatActivityOpen) {
		Application.sIsChatActivityOpen = isChatActivityOpen;
	}

	public static IgDiskCache getInstance(Context context){
		if (igCache == null) {
			igCache = new IgDiskCache(new File(ContextCompat.getDataDir(context), "temp"));
			return igCache;
		} else {
			return igCache;
		}
	}



	@Override
	public void onConfigurationChanged(Configuration newConfig)
	{
		super.onConfigurationChanged(newConfig);
		if (locale != null)
		{
			newConfig.locale = locale;
			Locale.setDefault(locale);
			getBaseContext().getResources().updateConfiguration(newConfig, getBaseContext().getResources().getDisplayMetrics());
		}
	}

	private static final ThreadLocal<Interceptor> REWRITE_RESPONSE_INTERCEPTOR = new ThreadLocal<Interceptor>() {
		@Override
		protected Interceptor initialValue() {
			return new Interceptor() {
				@Override
				public Response intercept(Chain chain) throws IOException {
					AtomicReference<Response> originalResponse = new AtomicReference<>(chain.proceed(chain.request()));
					AtomicReference<String> cacheControl = new AtomicReference<>(originalResponse.get().header("Cache-Control"));



					if (cacheControl.get() == null || cacheControl.get().contains("no-store") || cacheControl.get().contains("no-cache") ||
							cacheControl.get().contains("must-revalidate") || cacheControl.get().contains("max-age=0")) {
						return originalResponse.get().newBuilder()
								.header("Cache-Control", "public, max-age=" + 10)
								.build();
					} else {
						return originalResponse.get();
					}
				}
			};
		}
	};

	private static final ThreadLocal<Interceptor> OFFLINE_INTERCEPTOR = new ThreadLocal<Interceptor>() {
		@Override
		protected Interceptor initialValue() {
			return new Interceptor() {
				@Override
				public Response intercept(Chain chain) throws IOException {
					Request request = chain.request();

					if (!isOnline()) {
						Log.d("Application", "rewriting request");

						int maxStale = 60 * 60 * 24 * 28; // tolerate 4-weeks stale
						request = request.newBuilder()
								.header("Cache-Control", "public, only-if-cached, max-stale=" + maxStale)
								.build();
					}

					return chain.proceed(request);
				}
			};
		}
	};

	public static boolean isOnline() {
		ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		return netInfo != null && netInfo.isConnectedOrConnecting();
	}


	private void initFeatureStore(){

		try {
			featureStore = StoreBuilder.<BarCodeWithCursor, BufferedSource, Map<String,AdResult>>parsedWithKey()
                    .fetcher((barCode) -> {
                        if (barCode.getCity() != null) {
                            return ShbService.getRetrofitRx().create(UserService.class).getAdsByListofCatAndCityRx(barCode.getCity(), barCode.getType())
									.map(ResponseBody::source);
                        } else {
                            return ShbService.getRetrofitRx().create(UserService.class).getAdsByListofCatRx(barCode.getType())
									.map(ResponseBody::source);
                        }
                    }).persister(FileSystemPersister.create(FileSystemFactory.create(this.getFilesDir()),pathResolver))
                    .parser(GsonParserFactory.createSourceParser(new GsonBuilder().create(), new TypeToken<Map<String,AdResult>>(){}.getType()))
                    .networkBeforeStale()
					.open();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void intAdsCatStore(){

		try {
			adStore = StoreBuilder.<BarCodeWithCursor, BufferedSource , AdResult>parsedWithKey()
                    .fetcher((BarCodeWithCursor barCode) -> {
                        if (barCode.getCity() != null) {
                            return ShbService.getRetrofitRx().create(UserService.class).getAdsByCatAndCityRx(barCode.getType(), barCode.getCity(), barCode.getCursor())
									.map(ResponseBody::source);
                        } else {
                            return ShbService.getRetrofitRx().create(UserService.class).getAdsByCatRxStore(barCode.getType(), barCode.getCursor())
									.map(ResponseBody::source);
                        }
                    })
                    .persister(FileSystemPersister.create(FileSystemFactory.create(this.getFilesDir()),pathResolver))
                    .parser(GsonParserFactory.createSourceParser(new GsonBuilder().create(), AdResult.class))
                    .open();
		} catch (IOException e) {
			e.printStackTrace();
		}


	}

	private void initFavStore(){

		try {
			favStore =  StoreBuilder.<BarCode, BufferedSource , List<Favorite>>parsedWithKey()
                    .fetcher( barCode -> ShbService.getRetrofitRx().create(UserService.class).getMyFavRx(barCode.getKey()).map(ResponseBody::source))
                    .persister(FileSystemPersister.create(FileSystemFactory.create(this.getFilesDir()),resolver))
					.parser(GsonParserFactory.createSourceParser(new GsonBuilder().create(), new TypeToken<List<Favorite>>(){}.getType()))
                    .open();
		} catch (IOException e) {
			Log.d("mushy", e.getLocalizedMessage());
		}

	}

	public Store<Map<String, AdResult>, BarCodeWithCursor> getFeatureStore(){
		return featureStore;
	}

	public Store<AdResult, BarCodeWithCursor> getAdStore(){
		return adStore;
	}

	public Store<List<Favorite>, BarCode> getFavStore(){
		return favStore;
	}

	private void intCatList(){
		//initalise Cat
		String[] catid = getResources().getStringArray(R.array.cat_id);
		String[] cats = getResources().getStringArray(R.array.cat);

		catsAsString = new StringBuilder();

		newCatList = new LinkedHashMap<>();



		for (int i = 0; catid.length > i; i++){
			if (i == 0) catsAsString.append(catid[i]);
			else catsAsString.append(",").append(catid[i]);
			newCatList.put(catid[i], new Category(catid[i], cats[i]));
		}

	}

	public String getCatsAsString(){
		return catsAsString.toString();

	}

	public Map<String, Category> getCatMap(){
		return newCatList;
	}



}