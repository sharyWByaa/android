package com.shb.sa.shb.chat;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.shb.sa.shb.BaseFragment;
import com.shb.sa.shb.R;
import com.shb.sa.shb.chat.logic.ChatPresenter;
import com.shb.sa.shb.chat.model.Chat;
import com.vanniktech.emoji.EmojiEditText;
import com.vanniktech.emoji.EmojiPopup;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.chatsdk.core.dao.Message;
import co.chatsdk.core.dao.Thread;
import co.chatsdk.core.dao.User;
import co.chatsdk.core.events.EventType;
import co.chatsdk.core.events.NetworkEvent;
import co.chatsdk.core.session.NM;
import co.chatsdk.core.session.StorageManager;
import io.reactivex.Single;
import io.reactivex.SingleOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ChatSDKFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ChatSDKFragment extends BaseFragment implements  TextView.OnEditorActionListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    EmojiPopup emojiPopup;
    //firebase token
    //firebase reciverUID
    // firebase reciver

    List<Disposable> disposableList = new ArrayList<>();

    private final RecyclerView.RecycledViewPool recycledViewPool = new RecyclerView.RecycledViewPool();

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private ChatPresenter mChatPresenter;
    private String receiverUid;
    private View view;
    @BindView(R.id.list)
    RecyclerView list;
    @BindView(R.id.fragment_chat_message)
    EditText messageEditText;
    @BindView(R.id.fragment_smiley_face_btn)
    Button smilyFaceBtn;

    private ArrayAdapter<String> adapter;

    private LinearLayoutManager mLinearLayoutManager;
    private ArrayList<Chat> chats;
    private Thread thread;
    private final ChatSDKController controller2 = new ChatSDKController(recycledViewPool,FirebaseAuth.getInstance().getCurrentUser().getUid());
    private Set<Message> messseges = new TreeSet<>((message, t1) -> message.getDate().compareTo(t1.getDate()));
    private User otherUser;
    private String threadId;


    public ChatSDKFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *

     * @return A new instance of fragment ChatFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ChatSDKFragment newInstance(String receiver, String receiverUid, String firebaseToken) {
        ChatSDKFragment fragment = new ChatSDKFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, receiver);
        args.putString(ARG_PARAM2, receiverUid);
        fragment.setArguments(args);
        return fragment;
    }

    public static ChatSDKFragment newInstance(String threadId) {
        ChatSDKFragment fragment = new ChatSDKFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, threadId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            threadId = getArguments().getString(ARG_PARAM1);
            receiverUid = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_chat, container, false);
        ButterKnife.bind(this, view);
        emojiPopup = EmojiPopup.Builder.fromRootView(view).build((EmojiEditText) messageEditText);
        smilyFaceBtn.setText("{faw-smile-o}");

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);



        mLinearLayoutManager = new LinearLayoutManager(getActivity());
        controller2.setSpanCount(2);
        //mLinearLayoutManager.setSpanSizeLookup(controller.getSpanSizeLookup());

        mLinearLayoutManager.setStackFromEnd(true);
        //mLinearLayoutManager.setReverseLayout(true);
        //mLinearLayoutManager.setStackFromEnd(true);
        list.setLayoutManager(mLinearLayoutManager);

        list.setRecycledViewPool(recycledViewPool);
        list.setHasFixedSize(true);
        list.setAdapter(controller2.getAdapter());


        chats = new ArrayList<>();




        view.findViewById(R.id.btn_send).setOnClickListener(v -> {
            sendMessage(messageEditText.getText().toString());
            messageEditText.getText().clear();
        });

        smilyFaceBtn.setOnClickListener((v) -> {

            if (emojiPopup.isShowing()){
                emojiPopup.dismiss();
                smilyFaceBtn.setText("{faw-smile-o}");

            } else {
                emojiPopup.toggle();
                smilyFaceBtn.setText("{faw-keyboard-o}");

            }
        });



       disposableList.add(
               Single.create((SingleOnSubscribe<Thread>) e -> {
            this.thread = StorageManager.shared().fetchThreadWithEntityID(threadId);
            e.onSuccess(thread);
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
        .flatMap((th)-> NM.thread().loadMoreMessagesForThread(null,thread))
                .flatMap(messagesList-> Single.create( (SingleOnSubscribe<Set<Message>>) item ->{
                            messseges.addAll(messagesList);
                            item.onSuccess(messseges);

                }
                )).doFinally(() -> disposableList.add(

                NM.events().sourceOnMain()
                        .filter(NetworkEvent.filterType(EventType.MessageAdded, EventType.ThreadReadReceiptUpdated))
                        .filter(NetworkEvent.filterThreadEntityID(thread.getEntityID())).subscribe(networkEvent -> {
                    Message message = networkEvent.message;


                    // Check that the message is relevant to the current thread.
                    if (message.getThreadId() != thread.getId().intValue()) {
                        return;
                    }

                    // Toast.makeText(getContext(), "new message", Toast.LENGTH_LONG).show();

                    message.setRead(true);
                    message.update();

                    messseges.add(message);
                    controller2.setData(messseges,false);
                    list.smoothScrollToPosition(messseges.size() - 1);


                })
                )).subscribe((result) -> {
            controller2.setData(result,false);
            thread.markRead();
            thread.update();

        }, err -> Log.d("err", err.getMessage()))
       );






        /*
        disposableList.add(Single.create( emitter -> {



            messseges.addAll(StorageManager.shared().fetchMessagesForThreadWithID(thread.getId(), 100));

            disposableList.add(
                    NM.thread().loadMoreMessagesForThread(thread.lastMessage(),thread).observeOn(AndroidSchedulers.mainThread())
                            .subscribe((messages -> {

                                messseges.addAll(messages);
                                controller2.setData(messseges,false);
                                thread.markRead();
                                thread.update();
                                controller2.setData(messseges,false);
                            })));

            disposableList.add(
                    NM.events().sourceOnMain()
                            .filter(NetworkEvent.filterType(EventType.MessageAdded, EventType.ThreadReadReceiptUpdated))
                            .filter(NetworkEvent.filterThreadEntityID(thread.getEntityID())).subscribe(networkEvent -> {
                        Message message = networkEvent.message;


                        // Check that the message is relevant to the current thread.
                        if (message.getThreadId() != thread.getId().intValue()) {
                            return;
                        }

                        // Toast.makeText(getContext(), "new message", Toast.LENGTH_LONG).show();

                        message.setRead(true);
                        message.update();

                        messseges.add(message);
                        controller2.setData(messseges,false);
                        list.smoothScrollToPosition(messseges.size() - 1);


                    }));
            emitter.onSuccess(true);
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
        .subscribe((result)->{}, (error)->{})
        );





*/


        /*

        otherUser = co.chatsdk.core.session.StorageManager.shared().fetchOrCreateEntityWithEntityID(User.class ,receiverUid);

        disposableList.add(new ChatSDKExtention().get1to1Thread(NM.currentUser(), otherUser).subscribe((thread -> {

            this.thread = thread;




           messseges.addAll( StorageManager.shared().fetchMessagesForThreadWithID(thread.getId(), 100));
           disposableList.add(
            NM.thread().loadMoreMessagesForThread(thread.lastMessage(),thread).observeOn(AndroidSchedulers.mainThread())
                    .subscribe((messages -> {

                messseges.addAll(messages);
                controller2.setData(messseges,false);
                thread.markRead();
                thread.update();
                //controller2.setData(messseges,false);
            })));





        })));

        */

    }

    void sendMessage(String message){
        if (message == null || message.trim().isEmpty()) return; // ignore empty message or null
        NM.thread().sendMessageWithText(message.trim(),thread).subscribe((ms)->{


                messseges.add(ms.message);
                controller2.setData(messseges,false);

                list.smoothScrollToPosition(messseges.size() - 1);

        });



    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        return false;
    }


    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onStop() {
        super.onStop();



    }




    @Override
    public void onDestroy() {
        super.onDestroy();
        for (Disposable disposable: disposableList){
            disposable.dispose();
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        if (NM.currentUser() == null) NM.auth().authenticateWithCachedToken().subscribe(()->{});


    }
}
