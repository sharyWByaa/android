package com.shb.sa.shb.chat.model;

/**
 * Created by mujtaba on 06/07/2017.
 */

public class Chat {
    public String sender;
    public String senderUid;
    public String reciver;
    public String reciverUid;
    public String message;
    public long timestamp;

    public Chat(){}

    public Chat(String sender, String senderUid, String reciver, String reciverUid, String message, long timestamp) {
        this.sender = sender;
        this.senderUid = senderUid;
        this.reciver = reciver;
        this.reciverUid = reciverUid;
        this.message = message;
        this.timestamp = timestamp;
    }


    @Override
    public String toString() {
        return "Chat{" +
                "sender='" + sender + '\'' +
                ", senderUid='" + senderUid + '\'' +
                ", reciver='" + reciver + '\'' +
                ", reciverUid='" + reciverUid + '\'' +
                ", message='" + message + '\'' +
                ", timestamp=" + timestamp +
                '}';
    }
}
