package com.shb.sa.shb.user;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.anupcowkur.reservoir.Reservoir;
import com.anupcowkur.reservoir.ReservoirPutCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GetTokenResult;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.gson.JsonObject;
import com.instagram.igdiskcache.IgDiskCache;
import com.instagram.igdiskcache.SnapshotInputStream;
import com.shb.sa.shb.Application;
import com.shb.sa.shb.BaseActivity;
import com.shb.sa.shb.R;
import com.shb.sa.shb.REST.ShbService;
import com.shb.sa.shb.REST.UserService;
import com.shb.sa.shb.REST.models.Ad;
import com.shb.sa.shb.REST.models.Category;
import com.shb.sa.shb.UploadService;
import com.shb.sa.shb.model.Media;
import com.shb.sa.shb.user.wizard.LocationFragment;
import com.shb.sa.shb.user.wizard.PostAdCatFragment;
import com.shb.sa.shb.user.wizard.ReviewAdFragment;
import com.shb.sa.shb.user.wizard.SelectImagesFragment;
import com.shb.sa.shb.util.CacheUtil;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;
import com.stepstone.stepper.adapter.AbstractFragmentStepAdapter;
import com.stepstone.stepper.viewmodel.StepViewModel;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.shb.sa.shb.CameraViewFragment.CACHE_VIDEO_PICTURES;

public class AdInfoActivity extends BaseActivity implements StepperLayout.StepperListener,
        SelectImagesFragment.MediaSelection, AddAdFragment.Detail, PostAdCatFragment.CategorySelection, LocationFragment.LocationSelection {

    private ViewPager mPager;
    private StepperLayout mStepperLayout;
    private ArrayList<Media> mediaArrayList = new ArrayList<>();
    private Ad ad;
    private HashSet<Uri> images;
    private Category category;
    private LatLng latlong;
    private ArrayList<Media> pictureArrayList = new ArrayList<>();
    private ArrayList<Media> allMedia;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ad_info);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        //mediaArrayList = getIntent().getParcelableArrayListExtra("media");
        //pictureArrayList = getIntent().getParcelableArrayListExtra("picture");
        allMedia = getIntent().getParcelableArrayListExtra("allMedia");

        for (Media media: allMedia){
            switch (media.getType()){
                case Media.TYPE_IMAGE:
                    pictureArrayList.add(media);
                    break;
                case Media.TYPE_VIDEO:
                    mediaArrayList.add(media);
                    break;
            }
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, AddAdFragment.newInstance(mediaArrayList)).commit();

        // = (ViewPager) findViewById(R.id.pager);
        //mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        //mPager.setAdapter(mPagerAdapter);

        mStepperLayout = (StepperLayout) findViewById(R.id.stepperLayout);
        mStepperLayout.setAdapter(new MyStepperAdapter(getSupportFragmentManager(), this, mediaArrayList, pictureArrayList));

        mStepperLayout.setListener(this);


    }

    @Override
    public void onCompleted(View completeButton) {

        //finish();

        Toast.makeText(this, "Upload started", Toast.LENGTH_SHORT).show();


        // make sure the cat is set
        ad.setCategory(category);


        ArrayList<Uri> imagesList = new ArrayList<Uri>();
        imagesList.addAll(images);



            Reservoir.putAsync(CACHE_VIDEO_PICTURES, new ArrayList<>(), new ReservoirPutCallback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onFailure(Exception e) {

                }
            });



        UploadService.startActionFoo(this, mediaArrayList, ad, imagesList);
        finish();


        //upload(mediaArrayList);
    }

    @Override
    public void onError(VerificationError verificationError) {

    }

    @Override
    public void onStepSelected(int newStepPosition) {

    }

    @Override
    public void onReturn() {

    }

    @Override
    public void selectedImages(HashSet<Uri> images) {

        this.images = images;
    }

    @Override
    public void selectedMedia(ArrayList<Media> mediaArrayList) {
        this.mediaArrayList = mediaArrayList;
    }

    @Override
    public void informationSubmited(Ad ad) {
        this.ad = ad;
    }

    @Override
    public void catSelected(Category category) {
        this.category = category;

    }

    @Override
    public void setLatLong(LatLng latLong) {
        this.latlong = latLong;
        if (ad != null) ad.setLatLng(latLong);
    }


    public static class MyStepperAdapter extends AbstractFragmentStepAdapter {

        private final Step[] fragments;
        private final ArrayList<Media> mediaList;

        public MyStepperAdapter(FragmentManager fm, Context context, ArrayList<Media> mediaArrayList, ArrayList<Media> pictures) {
            super(fm, context);
            fragments = new Step[] {SelectImagesFragment.newInstance(pictures,mediaArrayList),
                    PostAdCatFragment.newInstance("",""),AddAdFragment.newInstance(null),
                    LocationFragment.newInstance("",""),
                    ReviewAdFragment.newInstance("","")};

            this.mediaList = mediaArrayList;
        }

        @Override
        public Step createStep(int position) {
            //final StepFragmentSample step = new StepFragmentSample();
            //Bundle b = new Bundle();
            //b.putInt(CURRENT_STEP_POSITION_KEY, position);
            //step.setArguments(b);

            return fragments[position];
        }

        @Override
        public int getCount() {
            return fragments.length;
        }

        @NonNull
        @Override
        public StepViewModel getViewModel(@IntRange(from = 0) int position) {
            //Override this method to set Step title for the Tabs, not necessary for other stepper types

            return new StepViewModel.Builder(context)
                    .setTitle("blah") //can be a CharSequence instead
                    .create();
        }
    }



    private void postAd(List<com.shb.sa.shb.REST.models.Media> mediaList){

        final Ad ad = this.ad;
        ad.setCategory(category);

        ad.setMedias(mediaList);


        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        if (mAuth.getCurrentUser() != null){
            mAuth.getCurrentUser().getToken(true).addOnSuccessListener(new OnSuccessListener<GetTokenResult>() {
                @Override
                public void onSuccess(GetTokenResult getTokenResult) {
                    ShbService.getInstence().getRetrofit().create(UserService.class).createAd(getTokenResult.getToken(), ad).enqueue(new Callback<JsonObject>() {
                        @Override
                        public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                            try {
                                CacheUtil.clearCache();
                                Observable.just(deleteTempFiles(mediaArrayList))
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribeOn(Schedulers.io());

                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                            setResult(100, new Intent());
                            finish();
                            //Toast.makeText(getContext(), "Post the ad", Toast.LENGTH_LONG).show();
                        }

                        @Override
                        public void onFailure(Call<JsonObject> call, Throwable t) {

                        }
                    });
                }
            });
        }





    }

    private void upload(ArrayList<Media> medias){

        AsyncTask<Media, String, List<Media>> task = new AsyncTask<Media, String, List<Media>>() {


            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                //progrssDialog.show();
            }

            @Override
            protected List<Media> doInBackground(Media... params) {


                List<Media> mediaArrayList = new ArrayList<Media>();
                for (Media media : params) {
                    SnapshotInputStream mediaStreamitem = Application.getInstance(AdInfoActivity.this).get(media.getMediaKey()).get();
                    SnapshotInputStream thumnailStreamitem = Application.getInstance(AdInfoActivity.this).get(media.getThumnailKey()).get();

                    media.mediaStream = mediaStreamitem;
                    media.thumnailStream = thumnailStreamitem;
                    mediaArrayList.add(media);
                }

                return mediaArrayList;
            }

            @Override
            protected void onPostExecute(List<Media> s) {
                super.onPostExecute(s);


                FirebaseStorage storage = Application.getStorage();


                // Create a storage reference from our app
                final StorageReference storageRef = storage.getReference();

                ArrayList<Task> allTask = new ArrayList<>();

                String dateString = new SimpleDateFormat("yyyyy-MM-dd", Locale.ENGLISH).format(new Date());

                //upload Images
                List<com.shb.sa.shb.REST.models.Media> pictures = new ArrayList<>();
                ad.setPictures(pictures);
                for (Uri imgUrl: images){
                    String uuid = UUID.randomUUID().toString().replaceAll("-", "");
                    String imageRefFirebase = mAuth.getCurrentUser().getUid() + "/" + dateString + "/images/" + uuid + "_" + imgUrl.getLastPathSegment();
                    StorageReference ref = storageRef.child(imageRefFirebase);
                    com.shb.sa.shb.REST.models.Media picture = new com.shb.sa.shb.REST.models.Media();
                    picture.setUrl(imageRefFirebase);
                    picture.setThumbnail(imageRefFirebase);
                    picture.setUid(uuid);
                    pictures.add(picture);

                    UploadTask imagesTask = ref.putFile(imgUrl);
                    allTask.add(imagesTask);

                }

                final List<com.shb.sa.shb.REST.models.Media> medias1 = new ArrayList<>();

                for (Media inputStream : s) {
                    String uuid = UUID.randomUUID().toString().replaceAll("-", "");
                    final com.shb.sa.shb.REST.models.Media media = new com.shb.sa.shb.REST.models.Media();
                    media.setUid(uuid);

                    StorageReference mountainsRef = storageRef.child(mAuth.getCurrentUser().getUid() +"/"+dateString+ "/video/" +uuid + ".mp4");


                    StorageReference mountainsThumnailRef = storageRef.child(mAuth.getCurrentUser().getUid() + "/"+dateString +"/video/images/" + uuid + ".png");


                    UploadTask uploadtask = mountainsRef.putStream(inputStream.mediaStream);
                    UploadTask uploadImagetask = mountainsThumnailRef.putStream(inputStream.thumnailStream);

                    allTask.add(uploadtask);
                    allTask.add(uploadImagetask);



                    uploadtask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            if (storageRef.getActiveUploadTasks().size() == 0){

                                media.setUrl(taskSnapshot.getDownloadUrl().getPath());



                                //postAd(medias1);


                            }else {

                                media.setUrl(taskSnapshot.getDownloadUrl().getPath());

                            }
                        }
                    });

                    uploadImagetask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            if (storageRef.getActiveUploadTasks().size() == 0){
                                media.setThumbnail(taskSnapshot.getDownloadUrl().getPath());
                                //postAd(medias1);

                            }else {

                                media.setThumbnail(taskSnapshot.getDownloadUrl().getPath());

                            }
                        }
                    });

                    medias1.add(media);





                }

                Tasks.whenAll(allTask.toArray(new Task[allTask.size()])).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        postAd(medias1);
                    }
                });





            }
        };

        task.execute(medias.toArray(new Media[medias.size()]));
    }


    public Boolean deleteTempFiles(ArrayList<Media> mediaArrayList){

        IgDiskCache igdk = Application.getInstance(getApplication());
        for (Media media: mediaArrayList){
            igdk.remove(media.getMediaKey());
            igdk.remove(media.getThumnailKey());
        }
        return true;
    }
}
