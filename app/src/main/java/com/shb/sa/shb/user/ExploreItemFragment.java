package com.shb.sa.shb.user;


import android.Manifest;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.marcinorlowski.fonty.Fonty;


import com.nytimes.android.external.store3.base.impl.Store;
import com.nytimes.android.external.store3.base.impl.StoreBuilder;

import com.shb.sa.shb.Application;
import com.shb.sa.shb.R;
import com.shb.sa.shb.REST.ShbService;
import com.shb.sa.shb.REST.UserService;
import com.shb.sa.shb.REST.models.Ad;
import com.shb.sa.shb.REST.models.AdResult;
import com.shb.sa.shb.REST.models.Category;
import com.shb.sa.shb.user.model.AdController;
import com.shb.sa.shb.user.model.BarCodeWithCursor;


import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.RuntimePermissions;
import pl.charmas.android.reactivelocation2.ReactiveLocationProvider;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ExploreItemFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
@RuntimePermissions
public class ExploreItemFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "cat";
    private static final String ARG_PARAM2 = "param2";

    private static final String CAROUSEL_DATA_KEY = "carousel_data_key";

    private final RecyclerView.RecycledViewPool recycledViewPool = new RecyclerView.RecycledViewPool();
    private AdController controller;

    private Store<AdResult, BarCodeWithCursor> articleStore;

    private Category category;
    private View view;
    private RecyclerView list;
    private GridLayoutManager mLinearLayoutManager;
    private RecyclerAdapter mAdapter;
    private List<Ad> adList = new ArrayList<>();
    private Map<String, Ad> adMap = new LinkedHashMap<>();
    private SwipeRefreshLayout swipeLayout;
    private BarCodeWithCursor barcode;
    private String cursor;
    private boolean mIsLoading = false;
    private String city;

    private final static int REQUEST_CHECK_SETTINGS = 0;


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param category Category is the category details
     * @return A new instance of fragment BlankFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ExploreItemFragment newInstance(Category category, String city) {
        ExploreItemFragment fragment = new ExploreItemFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM1, category);
        args.putString(ARG_PARAM2, city);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        Comparable<Ad> compare = new Comparable<Ad>() {
            @Override
            public int compareTo(@NonNull Ad ad) {
                return 0;
            }
        };

        SortedMap<String, Ad> treemap = Collections.synchronizedSortedMap(new TreeMap<String, Ad>());

        controller = new AdController(recycledViewPool, getActivity());

        if (getArguments() != null) {
            category = getArguments().getParcelable(ARG_PARAM1);
                //barcode = new BarCode(category.getTitle(),category.getUid());
            city = getArguments().getString(ARG_PARAM2);
            barcode = new BarCodeWithCursor(category.getTitle(), category.getUid(), "");
            if (city != null) barcode.setCity(city);
            if (barcode.getCity() == null && !barcode.getType().equals("nearme")) {
                articleStore = StoreBuilder.<BarCodeWithCursor, AdResult>key()
                        .fetcher((barCode) -> ShbService.getRetrofitRx().create(UserService.class).getAdsByCatRx(barCode.getType(), barCode.getCursor()))
                        .open();
            } else {

                ExploreItemFragmentPermissionsDispatcher.findLocationWithCheck(this);
            }

        }


    }

    @NeedsPermission(Manifest.permission.ACCESS_FINE_LOCATION)
    public void findLocation() {
        ReactiveLocationProvider locationProvider = new ReactiveLocationProvider(getActivity());
        final LocationRequest locationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY)
                .setNumUpdates(5)
                .setInterval(100);
        locationProvider
                .checkLocationSettings(new LocationSettingsRequest.Builder()
                        .addLocationRequest(locationRequest)
                        .setAlwaysShow(true)  //Refrence: http://stackoverflow.com/questions/29824408/google-play-services-locationservices-api-new-option-never
                        .build())
                .doOnNext(locationSettingsResult -> {

                    Status status = locationSettingsResult.getStatus();
                    if (status.getStatusCode() == LocationSettingsStatusCodes.RESOLUTION_REQUIRED) {
                        try {
                            status.startResolutionForResult(getActivity(), REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException th) {
                            //Log.e("MainActivity", "Error opening settings activity.", th);
                        }
                    }
                })
                .flatMap(locationSettingsResult -> {

                    if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return null;
                    }
                    return locationProvider.getUpdatedLocation(locationRequest);
                })
                .observeOn(AndroidSchedulers.mainThread())
        .subscribe(location ->
                new ReactiveLocationProvider(getActivity()).getReverseGeocodeObservable(Locale.ENGLISH,location.getLatitude(), location.getLongitude(), 1)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<Address>>() {
                    @Override
                    public void onSubscribe(@io.reactivex.annotations.NonNull Disposable disposable) {

                    }

                    @Override
                    public void onNext(@io.reactivex.annotations.NonNull List<Address> addresses) {

                        if (addresses.size() > 0){
                            //Toast.makeText(getContext(), addresses.get(0).getLocality(), Toast.LENGTH_SHORT).show();

                            if (addresses.get(0).getLocality() == null) return; //ignore
                            city = addresses.get(0).getLocality().toLowerCase();
                            barcode.setCity(city.toLowerCase());
                            articleStore = StoreBuilder.<BarCodeWithCursor, AdResult>key()
                                    .fetcher((barCode) -> ShbService.getRetrofitRx().create(UserService.class).getAdsByCityRx(barcode.getCity(), barCode.getCursor()))
                                    .open();
                            mIsLoading = true;
                            articleStore.get(barcode)
                                    .subscribeOn(io.reactivex.schedulers.Schedulers.newThread())
                                    .observeOn(io.reactivex.android.schedulers.AndroidSchedulers.mainThread())
                                    .subscribe(ExploreItemFragment.this::showPosts, throwable -> Log.w("mush", throwable.getLocalizedMessage()));
                        }
                    }

                    @Override
                    public void onError(@io.reactivex.annotations.NonNull Throwable throwable) {

                        Log.d(ExploreItemFragment.class.getName(), throwable.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                }));

        /*
        new ReactiveLocationProvider(getActivity()).getLastKnownLocation()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(location -> new ReactiveLocationProvider(getActivity()).getReverseGeocodeObservable(Locale.ENGLISH,location.getLatitude(), location.getLongitude(), 1)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<Address>>() {
                    @Override
                    public void onSubscribe(@io.reactivex.annotations.NonNull Disposable disposable) {

                    }

                    @Override
                    public void onNext(@io.reactivex.annotations.NonNull List<Address> addresses) {

                        if (addresses.size() > 0){
                            Toast.makeText(getContext(), addresses.get(0).getLocality(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onError(@io.reactivex.annotations.NonNull Throwable throwable) {

                        Toast.makeText(getContext(), throwable.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onComplete() {

                    }
                })
                );
                */

    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        view = inflater.inflate(R.layout.fragment_explore_item, container, false);

        controller.onRestoreInstanceState(savedInstanceState);

        swipeLayout = (SwipeRefreshLayout) view.findViewById(R.id.swiperefresh);

        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeLayout.setRefreshing(false);
                if (articleStore != null) {
                articleStore.clear(barcode);
                articleStore.clear();




                    articleStore.fetch(barcode).subscribeOn(io.reactivex.schedulers.Schedulers.newThread())
                            .observeOn(io.reactivex.android.schedulers.AndroidSchedulers.mainThread())
                            .subscribe(ExploreItemFragment.this::showPosts, throwable -> Log.w("mush", throwable.getLocalizedMessage()));
                    controller.setData(adList, true);
                    mIsLoading = true;
                }
            }
        });


        /*

        ((NestedScrollView) view.findViewById(R.id.nested_scroll_view)).setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (scrollY > oldScrollY) {
                    //Log.i(TAG, "Scroll DOWN");
                    if (getActivity() instanceof HomePageActivity){
                        ((HomePageActivity)getActivity()).hideNavBar();
                    }
                }
                if (scrollY < oldScrollY) {
                    //Log.i(TAG, "Scroll UP");
                    if (getActivity() instanceof HomePageActivity){
                        ((HomePageActivity)getActivity()).showNavBar();
                    }
                }

                if (scrollY == 0) {
                    //Log.i(TAG, "TOP SCROLL");
                    if (getActivity() instanceof HomePageActivity){
                        ((HomePageActivity)getActivity()).showNavBar();
                    }
                }

                if (scrollY == (v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight())) {
                    //Log.i(TAG, "BOTTOM SCROLL");
                    if (getActivity() instanceof HomePageActivity){
                        ((HomePageActivity)getActivity()).hideNavBar();
                    }
                }
            }
        });
*/

        list = (RecyclerView)view.findViewById(R.id.list);

        mLinearLayoutManager = new GridLayoutManager(getActivity(),2);
        controller.setSpanCount(2);
        mLinearLayoutManager.setSpanSizeLookup(controller.getSpanSizeLookup());
        list.setLayoutManager(mLinearLayoutManager);
        list.setRecycledViewPool(recycledViewPool);
        list.setHasFixedSize(true);
        list.setAdapter(controller.getAdapter());



        list.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });

        /*
        ShbService.getInstence().getRetrofit().create(UserService.class).getAds().enqueue(new Callback<List<Ad>>() {
            @Override
            public void onResponse(Call<List<Ad>> call, Response<List<Ad>> response) {
                mAdapter = new RecyclerAdapter(response.body());
                list.setAdapter(mAdapter);
                Toast.makeText(getContext(),"Ads Loaded", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<List<Ad>> call, Throwable t) {

            }
        });
        */







        if (savedInstanceState != null) {
            adList =  savedInstanceState.getParcelableArrayList(CAROUSEL_DATA_KEY) ;
            if (adList != null) controller.setData(adList, false);

            if (adList == null) {

                if (articleStore !=null) {

                    mIsLoading = true;

                    articleStore.get(barcode)
                            .subscribeOn(io.reactivex.schedulers.Schedulers.newThread())
                            .observeOn(io.reactivex.android.schedulers.AndroidSchedulers.mainThread())
                            .subscribe(this::showPosts, throwable -> Log.w("mush", throwable.getLocalizedMessage()));
                }

            }
        } else {


            if (articleStore != null) {
                mIsLoading = true;
                articleStore.get(barcode)
                        .subscribeOn(io.reactivex.schedulers.Schedulers.newThread())
                        .observeOn(io.reactivex.android.schedulers.AndroidSchedulers.mainThread())
                        .subscribe(this::showPosts, throwable -> Log.w("mush", throwable.getLocalizedMessage()));
            }



        }


        RecyclerView.OnScrollListener mScrollListener = new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (mIsLoading)
                    return;
                int visibleItemCount = mLinearLayoutManager.getChildCount();
                int totalItemCount = mLinearLayoutManager.getItemCount();
                int pastVisibleItems = mLinearLayoutManager.findFirstVisibleItemPosition();
                if (pastVisibleItems + visibleItemCount >= totalItemCount) {
                    //End of list

                    if(cursor != null) {
                        mIsLoading = true;
                        BarCodeWithCursor barcodeNext = new BarCodeWithCursor(category.getTitle(), category.getUid(), cursor);
                        if (city != null) barcodeNext.setCity(city);
                        articleStore.get(barcodeNext)
                                .subscribeOn(io.reactivex.schedulers.Schedulers.newThread())
                                .observeOn(io.reactivex.android.schedulers.AndroidSchedulers.mainThread())
                                .subscribe(ExploreItemFragment.this::showPosts, throwable -> Log.w("mush", throwable.getLocalizedMessage()));
                    }
                }
            }
        };


        list.addOnScrollListener(mScrollListener);

        Fonty.setFonts((ViewGroup) view);



        return view;
    }



    private void showPosts(AdResult adResult){

        mIsLoading = false;


        cursor = adResult.getEndCursor().encoded;


        for(Ad ad: adResult.getResults()){
            adMap.put(ad.getId(), ad);

        }

        this.adList = new ArrayList<>(adMap.values());


        //mAdapter = new RecyclerAdapter(adList);

        controller.setData(adList,false);

        //controller.requestModelBuild();







    }


    @Override
    public void onSaveInstanceState(Bundle state) {
        super.onSaveInstanceState(state);

        state.putParcelableArrayList(CAROUSEL_DATA_KEY, (ArrayList<? extends Parcelable>) adList);
        controller.onSaveInstanceState(state);
    }

    class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.StoryHolder> implements View.OnClickListener
    {
        private final List<Ad> adList;
        private Ad currentAd;
        final FirebaseStorage storage = Application.getStorage();


        public RecyclerAdapter(List<Ad> adList){

            this.adList = adList;
        }

        @Override
        public StoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View inflatedView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_explore_item, parent, false);
            //inflatedView.setOnClickListener(this);
            Fonty.setFonts((ViewGroup) inflatedView);
            return new StoryHolder(inflatedView);
        }

        @Override
        public void onBindViewHolder(StoryHolder holder, final int position) {
            currentAd = (adList.get(position));
            //Toast.makeText(getContext(), Uri.parse("https://firebasestorage.googleapis.com"+ media.getThumbnail() +"?alt=media").getPath(), Toast.LENGTH_LONG).show();
//holder.screenshot.setImageURI(Uri.parse("https://firebasestorage.googleapis.com"+ media.getThumbnail() +"?alt=media"));
//if(media.getThumbnail() != null) holder.screenshot.setImageURI("https://firebasestorage.googleapis.com"+ media.getThumbnail().replace("images/", "images%2F") +"?alt=media");
//Log.d("mushy", "https://firebasestorage.googleapis.com"+ media.getThumbnail() +"?alt=media");
            adList.get(position).getMedias().stream().filter(media -> media.getThumbnail() != null).forEach(media -> {
                StorageReference imgUrl = storage.getReferenceFromUrl("https://firebasestorage.googleapis.com" + media.getThumbnail() +"?alt=media");


                Log.d("mushy",  media.getThumbnail());

                imgUrl.getDownloadUrl().addOnSuccessListener(holder.screenshot::setImageURI);

                imgUrl.getDownloadUrl().addOnSuccessListener(uri -> {
                    Log.d("mushy", uri.getPath());
                    Toast.makeText(getContext(), "Img" + uri.getPath(), Toast.LENGTH_SHORT).show();
                }).addOnFailureListener(e -> {
                    Log.w("mushy", e.getLocalizedMessage());
                    Toast.makeText(getContext(), "ERROR" +  e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                });

            });

            holder.title.setText(adList.get(position).getTitle());
            holder.bindItem(adList.get(position));
            holder.itemAdView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(), ViewStoryActivity.class);
                    intent.putExtra("itemNo", position);
                    getActivity().startActivity(intent);
                }
            });

        }

        @Override
        public int getItemCount() {
            return adList.size();
        }

        @Override
        public void onClick(View v) {

        }

        class StoryHolder extends RecyclerView.ViewHolder{

            private final SimpleDraweeView screenshot;
            private final TextView title;
            private final View itemAdView;

            public StoryHolder(View itemView) {
                super(itemView);
                this.itemAdView = itemView;
                title = (TextView) itemView.findViewById(R.id.title_text);
                screenshot = (SimpleDraweeView) itemView.findViewById(R.id.screenshot_image_view);
            }

            protected void bindItem(Ad ad){

            }

        }
    }





}
