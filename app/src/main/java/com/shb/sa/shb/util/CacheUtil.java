package com.shb.sa.shb.util;

import com.anupcowkur.reservoir.Reservoir;
import com.shb.sa.shb.CameraViewFragment;
import com.shb.sa.shb.user.wizard.SelectImagesFragment;

import java.io.IOException;

/**
 * Created by mujtaba on 02/07/2017.
 */

public class CacheUtil {

    public static void clearCache() throws IOException {

        Reservoir.delete(SelectImagesFragment.CACHE_IMAGES);
        Reservoir.delete(CameraViewFragment.CACHE_MEDIA);


    }
}
