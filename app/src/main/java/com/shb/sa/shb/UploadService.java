package com.shb.sa.shb;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.gson.JsonObject;
import com.instagram.igdiskcache.IgDiskCache;
import com.instagram.igdiskcache.SnapshotInputStream;
import com.shb.sa.shb.REST.ShbService;
import com.shb.sa.shb.REST.UserService;
import com.shb.sa.shb.REST.models.Ad;
import com.shb.sa.shb.event.UploadServiceEvent;
import com.shb.sa.shb.model.Media;
import com.shb.sa.shb.util.CacheUtil;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class UploadService extends IntentService {

    private final IBinder mBinder = new LocalBinder();
    // TODO: Rename actions, choose action names that describe tasks that this
    // IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
    private static final String ACTION_FOO = "com.shb.sa.shwb.action.FOO";
    private static final String ACTION_BAZ = "com.shb.sa.shwb.action.BAZ";

    // TODO: Rename parameters
    private static final String EXTRA_PARAM1 = "com.shb.sa.shwb.extra.PARAM1";
    private static final String EXTRA_PARAM2 = "com.shb.sa.shwb.extra.PARAM2";
    private static final String EXTRA_PARAM3 = "com.shb.sa.shwb.extra.PARAM3";

    private boolean isRunning = false;

    public static final int STATUS_RUNNING = 0;
    public static final int STATUS_FINISHED = 1;
    public static final int STATUS_ERROR = 2;


    public UploadService() {
        super("UploadService");
    }

    /**
     * Starts this service to perform action Foo with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    // TODO: Customize helper method
    public static void startActionFoo(Context context, ArrayList<Media> param1, Ad param2, ArrayList<Uri> images) {
        Intent intent = new Intent(context, UploadService.class);
        intent.setAction(ACTION_FOO);
        intent.putExtra(EXTRA_PARAM1, param1);
        intent.putExtra(EXTRA_PARAM2, (Parcelable) param2);
        intent.putExtra(EXTRA_PARAM3,  images);

        context.startService(intent);


    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;

    }


    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_FOO.equals(action)) {

                final ArrayList<Media> param1 = intent.getParcelableArrayListExtra(EXTRA_PARAM1);
                final Ad param2 = intent.getParcelableExtra(EXTRA_PARAM2);
                final ArrayList<Uri> images = intent.getParcelableArrayListExtra(EXTRA_PARAM3);
                isRunning = true;
                UploadServiceEvent uploadEvent = new UploadServiceEvent(UploadServiceEvent.RUNNING);
                uploadEvent.setAd(param2);
                uploadEvent.setMediaList(param1);
                EventBus.getDefault().postSticky(uploadEvent);
                handleActionFoo(param1, param2, images);

                isRunning = false;
            }
        }
    }

    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */
    private void handleActionFoo(ArrayList<Media> params, Ad ad, ArrayList<Uri> images) {
        // TODO: Handle action Foo
        //throw new UnsupportedOperationException("Not yet implemented");



        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        //prepare


        List<Media> mediaArrayList = new ArrayList<Media>();
        for (Media media : params) {
            SnapshotInputStream mediaStreamitem = Application.getInstance(this).get(media.getMediaKey()).get();
            SnapshotInputStream thumnailStreamitem = Application.getInstance(this).get(media.getThumnailKey()).get();



            media.mediaStream = mediaStreamitem;
            media.thumnailStream = thumnailStreamitem;
            mediaArrayList.add(media);
        }



        FirebaseStorage storage = Application.getStorage();


        // Create a storage reference from our app
        final StorageReference storageRef = storage.getReference();

        ArrayList<Task> allTask = new ArrayList<>();

        String dateString = new SimpleDateFormat("yyyyy-MM-dd", Locale.ENGLISH).format(new Date());

        //upload Images
        List<com.shb.sa.shb.REST.models.Media> pictures = new ArrayList<>();
        ad.setPictures(pictures);
        for (Uri imgUrl: images){
            String uuid = UUID.randomUUID().toString().replaceAll("-", "");
            String imageRefFirebase = mAuth.getCurrentUser().getUid() + "/" + dateString + "/images/" + uuid + "_" + imgUrl.getLastPathSegment();
            StorageReference ref = storageRef.child(imageRefFirebase);
            com.shb.sa.shb.REST.models.Media picture = new com.shb.sa.shb.REST.models.Media();
            picture.setUrl(imageRefFirebase);
            picture.setThumbnail(imageRefFirebase);
            picture.setUid(uuid);
            pictures.add(picture);

            UploadTask imagesTask = ref.putFile(imgUrl);
            allTask.add(imagesTask);



        }

        final List<com.shb.sa.shb.REST.models.Media> medias1 = new ArrayList<>();

        for (Media inputStream : mediaArrayList) {
            String uuid = UUID.randomUUID().toString().replaceAll("-", "");
            final com.shb.sa.shb.REST.models.Media media = new com.shb.sa.shb.REST.models.Media();
            media.setUid(uuid);


            StorageReference mountainsRef = storageRef.child(mAuth.getCurrentUser().getUid() +"/"+dateString+ "/video/" +uuid + ".mp4");


            StorageReference mountainsThumnailRef = storageRef.child(mAuth.getCurrentUser().getUid() + "/"+dateString +"/video/images/" + uuid + ".png");


            UploadTask uploadtask = mountainsRef.putStream(inputStream.mediaStream);
            UploadTask uploadImagetask = mountainsThumnailRef.putStream(inputStream.thumnailStream);

            allTask.add(uploadtask);
            allTask.add(uploadImagetask);



            uploadtask.addOnSuccessListener(taskSnapshot -> {
                if (storageRef.getActiveUploadTasks().size() == 0){

                    media.setUrl(taskSnapshot.getDownloadUrl().getLastPathSegment());



                    //postAd(medias1);


                }else {


                    media.setUrl(taskSnapshot.getDownloadUrl().getLastPathSegment());

                }
            });

            uploadImagetask.addOnSuccessListener(taskSnapshot -> {



                if (storageRef.getActiveUploadTasks().size() == 0){
                    media.setThumbnail(taskSnapshot.getDownloadUrl().getLastPathSegment());
                    //postAd(medias1);

                }else {

                    media.setThumbnail(taskSnapshot.getDownloadUrl().getLastPathSegment());

                }
            });

            medias1.add(media);

        }

        Tasks.whenAll(allTask.toArray(new Task[allTask.size()])).addOnCompleteListener(task -> postAd(medias1, ad, params));






    }

    private void postAd(List<com.shb.sa.shb.REST.models.Media> mediaList, Ad ad, ArrayList<Media> mediaArrayList) {

        ad.setMedias(mediaList);


        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        if (mAuth.getCurrentUser() != null){
            mAuth.getCurrentUser().getToken(true).addOnSuccessListener(getTokenResult -> ShbService.getInstence().getRetrofit().create(UserService.class).createAd(getTokenResult.getToken(), ad).enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    EventBus.getDefault().postSticky(new UploadServiceEvent(UploadServiceEvent.FINISHED));

                    try {
                        CacheUtil.clearCache();

                        Observable.just(deleteTempFiles(mediaArrayList))
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribeOn(Schedulers.io());

                    } catch (IOException e) {
                        e.printStackTrace();
                    }



                    //Toast.makeText(getContext(), "Post the ad", Toast.LENGTH_LONG).show();
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                }
            }));
        }
    }

    public Boolean deleteTempFiles(ArrayList<Media> mediaArrayList){

        try {
            IgDiskCache igdk = Application.getInstance(getApplication());
            for (Media media : mediaArrayList) {
                igdk.remove(media.getMediaKey());
                igdk.remove(media.getThumnailKey());
            }
        } catch (IllegalStateException e){
            Log.d("mushy", e.getLocalizedMessage());
        }
        return true;
    }

    public boolean isRunning() {
        return isRunning;
    }

    public void setRunning(boolean running) {
        isRunning = running;
    }


    public class LocalBinder extends Binder {
        public UploadService getService() {
            // Return this instance of LocalService so clients can call public methods
            return UploadService.this;
        }
    }








}
