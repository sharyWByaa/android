package com.shb.sa.shb.user;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GetTokenResult;
import com.shb.sa.shb.BaseActivity;
import com.shb.sa.shb.R;
import com.shb.sa.shb.REST.ShbService;
import com.shb.sa.shb.REST.UserService;
import com.shb.sa.shb.REST.models.Ad;
import com.shb.sa.shb.REST.models.User;
import com.shb.sa.shb.chat.Chat2Activity;
import com.shb.sa.shb.chat.logic.ChatInteractor;
import com.shb.sa.shb.chat.logic.ChatSDKExtention;
import com.shb.sa.shb.user.model.ProfileController;

import java.util.ArrayList;
import java.util.List;

import co.chatsdk.core.session.NM;
import co.chatsdk.core.session.StorageManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserProfileActivity extends BaseActivity implements ProfileController.ProfileCallback {

    private RecyclerView list;
    private final RecyclerView.RecycledViewPool recycledViewPool = new RecyclerView.RecycledViewPool();
    private final ProfileController profileController = new ProfileController(this);
    private static final int SPAN_COUNT = 2;




    private List<Ad> adList;
    private User userInfo;
    private String uid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        if(getIntent().getStringExtra("uid") != null) uid = getIntent().getStringExtra("uid"); else return;

        list = (RecyclerView) findViewById(R.id.list);
        list.setRecycledViewPool(recycledViewPool);
        profileController.setSpanCount(SPAN_COUNT);
        list.setHasFixedSize(true);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2);
        gridLayoutManager.setSpanSizeLookup(profileController.getSpanSizeLookup());
        list.setLayoutManager(gridLayoutManager);
        list.setAdapter(profileController.getAdapter());
        profileController.setData(new ArrayList<Ad>(), userInfo);

        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        if (mAuth.getCurrentUser() != null){

            mAuth.getCurrentUser().getToken(true).addOnSuccessListener(new OnSuccessListener<GetTokenResult>() {
                @Override
                public void onSuccess(final GetTokenResult getTokenResult) {

                    ShbService.getInstence().getRetrofit().create(UserService.class).getUserInfo(getTokenResult.getToken(), uid).enqueue(new Callback<User>() {
                        @Override
                        public void onResponse(Call<User> call, Response<User> response) {
                            if (response.body() == null) {
                                // user does not exist
                                finish();
                                return;
                            }
                            UserProfileActivity.this.userInfo = response.body();
                            profileController.setData(adList, UserProfileActivity.this.userInfo);
                            getSupportActionBar().setTitle(response.body().getUsername());
                            myToolbar.setTitle(response.body().getUsername());
                            updateUI();
                        }

                        @Override
                        public void onFailure(Call<User> call, Throwable t) {
                            finish();
                        }
                    });
                    ShbService.getInstence().getRetrofit().create(UserService.class).getUserAds(getTokenResult.getToken(), uid).enqueue(new Callback<List<Ad>>() {
                        @Override
                        public void onResponse(Call<List<Ad>> call, Response<List<Ad>> response) {

                            adList = response.body();

                            profileController.setData(response.body(), userInfo);
                            updateUI();
                        }

                        @Override
                        public void onFailure(Call<List<Ad>> call, Throwable t) {


                        }
                    });
                }
            });
        }




    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (mAuth.getCurrentUser() != null && uid.equals(mAuth.getCurrentUser().getUid())) getMenuInflater().inflate(R.menu.user_profile_activity_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_settings:
                startActivity(new Intent(this, ViewProfileActivity.class));
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void callUser() {
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + userInfo.getPhoneNumber()));
        startActivity(intent);
    }

    @Override
    public void messageUser() {

        Intent intent= new Intent(this, Chat2Activity.class);
        co.chatsdk.core.dao.User otherUser = StorageManager.shared().fetchOrCreateEntityWithEntityID(co.chatsdk.core.dao.User.class, userInfo.getUid());
        new ChatSDKExtention().get1to1Thread(NM.currentUser(), otherUser).subscribe((thread)->{
            intent.putExtra(ChatInteractor.ARG_RECEIVER_UID, userInfo.getUid());
            intent.putExtra("thread", thread.getEntityID());
            startActivity(intent);
        }, error -> Log.d(this.getLocalClassName(), error.getMessage()));

    }

    @Override
    public void onAdClick(int pos) {
        Intent intent = new Intent(this, ViewStoryActivity.class);
        intent.putExtra("itemNo", pos);
        intent.putExtra("adsList", (ArrayList<Ad>)adList);
        startActivity(intent);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;

    }

    private void updateUI(){

        list.setVisibility(View.VISIBLE);
    }
}
