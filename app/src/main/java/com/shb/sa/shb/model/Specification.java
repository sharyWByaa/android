package com.shb.sa.shb.model;

/**
 * Created by mujtaba on 15/05/2017.
 */

public class Specification {


    public Specification(String title, String value) {
        this.title = title;
        this.value = value;
    }

    private String title;
    private String value;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }



}
