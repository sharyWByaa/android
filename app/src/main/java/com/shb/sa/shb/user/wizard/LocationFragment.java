package com.shb.sa.shb.user.wizard;


import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.location.places.ui.SupportPlaceAutocompleteFragment;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.shb.sa.shb.R;
import com.shb.sa.shb.util.SnackBar;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link LocationFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LocationFragment extends Fragment implements Step, OnMapReadyCallback {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String TAG =  "Location";

    private final LatLng latLng = new LatLng(24.65, 46.766667);

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private View view;
    private SupportPlaceAutocompleteFragment autocompleteFragment;
    private SupportMapFragment mapFragment;
    private GoogleMap mMap;
    private LatLng currentLatLng;
    private LocationSelection mListener;


    public LocationFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment LocationFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static LocationFragment newInstance(String param1, String param2) {
        LocationFragment fragment = new LocationFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        try {
            view = inflater.inflate(R.layout.fragment_location, container, false);

        } catch (Exception e){

            getChildFragmentManager().beginTransaction().remove(getChildFragmentManager().findFragmentByTag("autocomplete")).commitNow();
            getChildFragmentManager().beginTransaction().remove(getChildFragmentManager().findFragmentByTag("mapFragment")).commitNow();

            view = inflater.inflate(R.layout.fragment_location, container, false);

        }



        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        autocompleteFragment = (SupportPlaceAutocompleteFragment)
                getChildFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);


        mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentByTag("mapFragment");
        mapFragment.getMapAsync(this);







        autocompleteFragment.setHint("Enter City");

        AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                .setTypeFilter(AutocompleteFilter.TYPE_FILTER_CITIES)
                .setCountry("SA")
                .build();

        autocompleteFragment.setFilter(typeFilter);

        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                // TODO: Get info about the selected place.
                Log.i(TAG, "Place: " + place.getName() + " name in :"+  place.getLocale());

                setLatLng(place.getLatLng());
                if (mMap != null) {
                    mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(place.getViewport(), 1));


                }

            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                Log.i(TAG, "An error occurred: " + status);
            }
        });



    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();


    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);


    }

    @Override
    public void onStop() {
        super.onStop();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();


    }

    @Override
    public VerificationError verifyStep() {

        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {



        SnackBar.make(view, error.getErrorMessage(), Snackbar.LENGTH_SHORT).show();

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;


        MarkerOptions marker = new MarkerOptions().position(latLng).title("Location");

        Marker markerPos = mMap.addMarker(marker);

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 5));

        mMap.setOnMapClickListener(latLng -> {

                    Geocoder geocoder;
                    List<Address> addresses;
                    geocoder = new Geocoder(getContext(), new Locale("AR"));
                    try {
                        addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

                      setLatLng(latLng);

                        // locallity - city // getThoroughfare street - getSubLocality - حي - SubThoroughfare house no
                        try {
                            SnackBar.make(view, addresses.get(0).getSubThoroughfare() + " " + addresses.get(0).getThoroughfare() + " " + addresses.get(0).getSubLocality() + " " + addresses.get(0).getLocality() + "  " + addresses.get(0).getPostalCode(), Snackbar.LENGTH_SHORT).show();

                        } catch (java.lang.IndexOutOfBoundsException ex) {

                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    markerPos.setPosition(latLng);


        }


        );

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof LocationSelection) {
            mListener = (LocationSelection) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void setLatLng(LatLng latLng){
        this.currentLatLng = latLng;
        mListener.setLatLong(latLng);

    }

    public interface LocationSelection {
        void setLatLong(LatLng latLong);
    }
}
