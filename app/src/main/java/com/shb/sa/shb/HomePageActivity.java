package com.shb.sa.shb;

import android.animation.Animator;
import android.content.Context;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;

import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;


import com.marcinorlowski.fonty.Fonty;

import com.mikepenz.iconics.context.IconicsContextWrapper;


import com.shb.sa.shb.chat.Chat2Activity;
import com.shb.sa.shb.chat.ChatListFragment;
import com.shb.sa.shb.chat.logic.ChatInteractor;
import com.shb.sa.shb.user.FavoriteFragment;
import com.shb.sa.shb.user.ProfileFragment;
import com.shb.sa.shb.util.UnSwipeableViewPager;
import com.shb.sa.shb.util.ui.BottomNavigationViewHelper;

import static android.view.View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION;

public class HomePageActivity extends BaseActivity implements FavoriteFragment.OnFragmentInteractionListener, CameraViewFragment.ScreenLock{

    public static final String EXTRA_CIRCULAR_REVEAL_X = "EXTRA_CIRCULAR_REVEAL_X";
    public static final String EXTRA_CIRCULAR_REVEAL_Y = "EXTRA_CIRCULAR_REVEAL_Y";

    View rootLayout;

    private int revealX;
    private int revealY;

    private BottomNavigationView bottomNav;
    private Fragment exploreFragment;
    private ProfileFragment profileFragment;
    private CameraViewFragment camera;
    private UnSwipeableViewPager mPager;
    private ScreenSlidePagerAdapter mPagerAdapter;
    private FavoriteFragment favFragment;
    private Fragment[] fragments;
    private Window w;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        w = getWindow();



        w.setFlags(
                WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        //w.setFlags(SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION, SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
        //w.setFlags(SYSTEM_UI_FLAG_LAYOUT_STABLE, SYSTEM_UI_FLAG_LAYOUT_STABLE);


        setContentView(R.layout.activity_home_page);

        if (getIntent() != null) {

            if ( getIntent().getExtras() != null) {
                for (String s : getIntent().getExtras().keySet()) {
                    Log.d("muhsy", s);
                }
            }

            Intent passedIntent = getIntent().getParcelableExtra("shb_intent");

            if (passedIntent != null) {

                for (String s: passedIntent.getExtras().keySet()){
                    Log.d("mushy", s);
                }
                if (passedIntent.getExtras().getString("chat_sdk_thread_entity_id") != null
                        && passedIntent.getExtras().getString("chat_sdk_user_entity_id") != null) {

                    Log.d("mushy", passedIntent.getExtras().getString("chat_sdk_thread_entity_id"));
                    Log.d("mushy", passedIntent.getExtras().getString("chat_sdk_user_entity_id"));
                    String threadid = passedIntent.getExtras().getString("chat_sdk_thread_entity_id");

                    Intent intent = new Intent(this, Chat2Activity.class);
                    intent.putExtra("thread", threadid);
                    intent.putExtra(ChatInteractor.ARG_RECEIVER_UID, passedIntent.getExtras().getString("chat_sdk_user_entity_id"));
                    startActivity(intent);

                }
            }


        }


        // cericlar reveal

        final Intent intent = getIntent();

        rootLayout = findViewById(R.id.root_layout);

        if (savedInstanceState == null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP &&
                intent.hasExtra(EXTRA_CIRCULAR_REVEAL_X) &&
                intent.hasExtra(EXTRA_CIRCULAR_REVEAL_Y)) {
            rootLayout.setVisibility(View.INVISIBLE);

            revealX = intent.getIntExtra(EXTRA_CIRCULAR_REVEAL_X, 0);
            revealY = intent.getIntExtra(EXTRA_CIRCULAR_REVEAL_Y, 0);


            ViewTreeObserver viewTreeObserver = rootLayout.getViewTreeObserver();
            if (viewTreeObserver.isAlive()) {
                viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        revealActivity(revealX, revealY);
                        rootLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }
                });
            }
        } else {
            rootLayout.setVisibility(View.VISIBLE);
        }










        bottomNav = findViewById(R.id.bottom_navigation);

        // remove shifting
        BottomNavigationViewHelper.disableShiftMode(bottomNav);


        mPager = findViewById(R.id.pager);


        exploreFragment = FeatureFragment.newInstance("","");
        //exploreFragment = ExploreFragment.newInstance("","");
        profileFragment = ProfileFragment.newInstance("","");
        favFragment = FavoriteFragment.newInstance("","");
        camera = CameraViewFragment.newInstance("","");


        fragments = new Fragment[] {exploreFragment,favFragment ,camera , ChatListFragment.newInstance("",""), profileFragment};

        mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager(),fragments);
        mPager.setAdapter(mPagerAdapter);




        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {


                //Toast.makeText(HomePageActivity.this, String.valueOf(position) +"-"+ positionOffset+"-"+ positionOffsetPixels, Toast.LENGTH_SHORT).show();


                //bottomNav.setTranslationY(200 - (positionOffset*200));
            }

            @Override
            public void onPageSelected(int position) {

                Window w = getWindow();
                switch (position){
                    case 0:
                       // bottomNav.selectTabAtPosition(0);
                        bottomNav.setVisibility(View.VISIBLE);
                        bottomNav.setSelectedItemId(R.id.action_explore);
                       // bottomNav.selectTabWithId(R.id.action_explore);

                       // bottomNav.getShySettings().showBar();


                        w.clearFlags(
                                WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                        w.clearFlags(SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
                        //w.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                       showSystemUI();
                        break;
                    case 2:

                       // bottomNav.getShySettings().hideBar();
                        //bottomNav.selectTabAtPosition(2);
                       // bottomNav.selectTabWithId(R.id.action_capture);

                        bottomNav.setSelectedItemId(R.id.action_capture);
                        bottomNav.setVisibility(View.INVISIBLE);

                        w.setFlags(
                                WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                                WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

                        hideSystemUI();

                        break;
                    case 4:
                       // bottomNav.selectTabAtPosition(4);
                        bottomNav.setVisibility(View.VISIBLE);
                        bottomNav.setSelectedItemId(R.id.action_profile);
                       // bottomNav.selectTabWithId(R.id.action_profile);
                        //w.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                       // bottomNav.getShySettings().showBar();
                        w.clearFlags(
                                WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                        w.clearFlags(SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
                        showSystemUI();
                        break;
                    case 1:
                      //  bottomNav.selectTabAtPosition(1);
                        bottomNav.setVisibility(View.VISIBLE);
                        bottomNav.setSelectedItemId(R.id.action_fav);
                      //  bottomNav.selectTabWithId(R.id.action_fav);
                        //w.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                      //  bottomNav.getShySettings().showBar();
                        w.clearFlags(
                                WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                        w.clearFlags(SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
                        showSystemUI();
                        break;
                    case 3:
                       // bottomNav.selectTabAtPosition(3);
                        bottomNav.setVisibility(View.VISIBLE);
                        bottomNav.setSelectedItemId(R.id.action_chat);
                        //bottomNav.selectTabWithId(R.id.action_music);
                        //w.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                       // bottomNav.getShySettings().showBar();
                        w.clearFlags(
                                WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                        w.clearFlags(SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
                        showSystemUI();
                        break;

                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        bottomNav.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()){

                    case R.id.action_explore:
                        mPager.setCurrentItem(0,false);
                        break;
                    case R.id.action_fav:
                        mPager.setCurrentItem(1,false);
                        break;
                    case R.id.action_capture:
                        mPager.setCurrentItem(2,false);
                        fragments[2].setUserVisibleHint(true);
                        break;
                    case R.id.action_chat:
                        mPager.setCurrentItem(3,false);
                        break;
                    case R.id.action_profile:
                        mPager.setCurrentItem(4,false);
                        break;

                }
                return true;
            }
        });

        /*
        bottomNav.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelected(@IdRes int tabId) {
                switch (tabId){

                    case R.id.action_explore:
                        //getSupportFragmentManager().beginTransaction().replace(R.id.frame_layout, exploreFragment).addToBackStack(null).commit();
                        mPager.setCurrentItem(0,false);
                        break;
                    case R.id.action_profile:
                        //getSupportFragmentManager().beginTransaction().replace(R.id.frame_layout, profileFragment).addToBackStack(null).commit();
                        mPager.setCurrentItem(4,false);

                        break;

                    case R.id.action_fav:
                        //getSupportFragmentManager().beginTransaction().replace(R.id.frame_layout, profileFragment).addToBackStack(null).commit();
                        mPager.setCurrentItem(1,false);

                        break;

                    case R.id.action_capture:
                        mPager.setCurrentItem(2,false);
                        fragments[2].setUserVisibleHint(true);
                        //getSupportFragmentManager().beginTransaction().replace(R.id.frame_layout, camera).addToBackStack(null).commit();
                        //startActivity(new Intent(HomePageActivity.this, MainActivity.class));
                        //bottomNav.setVisibility(View.GONE);
                        break;

                    case R.id.action_music:
                        mPager.setCurrentItem(3,false);
                        break;
                }
                return;
            }
        });

*/
       // bottomNav.selectTabAtPosition(0);



        Fonty.setFonts(this);
    }



    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(IconicsContextWrapper.wrap(newBase));
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void lockSceen(boolean isLock) {
        if (isLock){
            mPager.setPagingEnabled(false);
        } else {
            mPager.setPagingEnabled(true);
        }
    }

    /**
     * A simple pager adapter that represents 5 ScreenSlidePageFragment objects, in
     * sequence.
     */
    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        private final Fragment[] fragments;

        public ScreenSlidePagerAdapter(FragmentManager fm, Fragment[] fragments) {
            super(fm);
            this.fragments = fragments;
        }

        @Override
        public Fragment getItem(int position) {
            return fragments[position];
        }

        @Override
        public int getCount() {
            return fragments.length;
        }
    }

    @Override
    public void onBackPressed() {
        if (mPager.getCurrentItem() == 0) {
            // If the user is currently looking at the first step, allow the system to handle the
            // Back button. This calls finish() on this activity and pops the back stack.
            super.onBackPressed();
        } else {
            // Otherwise, select the previous step.
            mPager.setCurrentItem(mPager.getCurrentItem() - 1);
        }
    }

    public void hideNavBar(){
        //if (bottomNav != null) bottomNav.getShySettings().hideBar();
    }

    public void showNavBar(){
        //if (bottomNav != null) bottomNav.getShySettings().showBar();

    }

    private void hideSystemUI() {
        // Set the IMMERSIVE flag.
        // Set the content to appear under the system bars so that the content
        // doesn't resize when the system bars hide and show.

        w.getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
    }

    // This snippet shows the system bars. It does this by removing all the flags
// except for the ones that make the content appear under the system bars.
    private void showSystemUI() {
        w.getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
    }

    protected void revealActivity(int x, int y) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            float finalRadius = (float) (Math.max(rootLayout.getWidth(), rootLayout.getHeight()) * 1.1);

            // create the animator for this view (the start radius is zero)
            Animator circularReveal = ViewAnimationUtils.createCircularReveal(rootLayout, x, y, 0, finalRadius);
            circularReveal.setDuration(400);
            circularReveal.setInterpolator(new AccelerateInterpolator());

            // make the view visible and start the animation
            rootLayout.setVisibility(View.VISIBLE);
            circularReveal.start();
        } else {
            finish();
        }
    }



    @Override
    protected void onNewIntent(Intent newintent) {
        super.onNewIntent(newintent);



        if ( newintent.getExtras() != null) {
            Intent passedIntent = newintent.getParcelableExtra("shb_intent");

            if (passedIntent != null && passedIntent.getExtras() != null) {


                if (passedIntent.getExtras().getString("chat_sdk_thread_entity_id") != null
                        && passedIntent.getExtras().getString("chat_sdk_user_entity_id") != null) {

                    String threadid = passedIntent.getExtras().getString("chat_sdk_thread_entity_id");

                    Intent intent = new Intent(this, Chat2Activity.class);
                    intent.putExtra("thread", threadid);
                    intent.putExtra(ChatInteractor.ARG_RECEIVER_UID, passedIntent.getExtras().getString("chat_sdk_user_entity_id"));
                    startActivity(intent);

                }
            }
        }

        Log.d("mushy", "activity Result - homepage");
    }
}
