package com.shb.sa.shb.Registration;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import com.marcinorlowski.fonty.Fonty;
import com.stephentuso.welcome.WelcomeHelper;

import com.shb.sa.shb.R;
import com.shb.sa.shb.WelcomeActivity;

import co.chatsdk.core.dao.User;
import co.chatsdk.core.session.NM;
import co.chatsdk.core.session.NetworkManager;
import co.chatsdk.core.session.StorageManager;

public class LoginActivity extends AppCompatActivity implements LoginFragment.OnFragmentInteractionListener{

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

    }

    private WelcomeHelper welcomeScreen;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().requestFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
        Window w = getWindow();

        w.setFlags(
                WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);


        welcomeScreen = new WelcomeHelper(this, WelcomeActivity.class);
        welcomeScreen.show(savedInstanceState);




        setContentView(R.layout.activity_login);

        NetworkManager.shared().a.auth.authenticateWithCachedToken().subscribe(()->{


            User user = StorageManager.shared().fetchOrCreateEntityWithEntityID(User.class, NM.currentUser().getEntityID());

        }, (error)-> Log.d("mush", error.getMessage()));


        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);


        Fragment fragment = LoginFragment.newInstance("","");
        getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, fragment).commit();

        Fonty.setFonts(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        welcomeScreen.onSaveInstanceState(outState);
    }


    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Log.d("mushy", "activity Result - login");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("mushy", "result "+ data.toString());
    }
}
