package com.shb.sa.shb.user;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.shb.sa.shb.BaseFragment;
import com.shb.sa.shb.R;
import com.shb.sa.shb.REST.models.Media;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ImagePagerFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ImagePagerFragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final int NUM_PAGES = 5;

    // TODO: Rename and change types of parameters
    private List<com.shb.sa.shb.REST.models.Media> picturesList;
    private View view;

    @BindView(R.id.view_pager)
    ViewPager mPager;
    private ScreenSlidePagerAdapter mPagerAdapter;


    public ImagePagerFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @return A new instance of fragment ImagePagerFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ImagePagerFragment newInstance(ArrayList<com.shb.sa.shb.REST.models.Media> pictures) {
        ImagePagerFragment fragment = new ImagePagerFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(ARG_PARAM1, pictures);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            picturesList = getArguments().getParcelableArrayList(ARG_PARAM1);

        }
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_image_pager, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // Instantiate a ViewPager and a PagerAdapter.

        if (picturesList != null){
        mPagerAdapter = new ScreenSlidePagerAdapter(getActivity().getSupportFragmentManager(), picturesList);
        mPager.setAdapter(mPagerAdapter);
        }
    }

    /**
     * A simple pager adapter that represents 5 ScreenSlidePageFragment objects, in
     * sequence.
     */
    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        private final List<Media> mediaList;

        ScreenSlidePagerAdapter(FragmentManager fm, List<Media> mediaList) {
            super(fm);
            this.mediaList = mediaList;
        }

        @Override
        public Fragment getItem(int position) {
            return ImageHolderFragment.newInstance(mediaList.get(position));
        }

        @Override
        public int getCount() {
            return mediaList.size();
        }
    }


}
