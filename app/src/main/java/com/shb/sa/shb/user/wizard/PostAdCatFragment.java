package com.shb.sa.shb.user.wizard;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.shb.sa.shb.R;
import com.shb.sa.shb.REST.ShbService;
import com.shb.sa.shb.REST.UserService;
import com.shb.sa.shb.REST.models.Category;
import com.shb.sa.shb.user.model.RadioButtonModel_;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the

 * to handle interaction events.
 * Use the {@link PostAdCatFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PostAdCatFragment extends Fragment  implements Step, CatController.AdapterCallbacks{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private View view;
    private RecyclerView list;

    private final RecyclerView.RecycledViewPool recycledViewPool = new RecyclerView.RecycledViewPool();
    private final CatController controller = new CatController(this);
    private static final int SPAN_COUNT = 2;

    private Category oldCat = null;
    private List<Category> catList = new ArrayList<>();
    private CategorySelection mListener;


    public PostAdCatFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PostAdCatFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PostAdCatFragment newInstance(String param1, String param2) {
        PostAdCatFragment fragment = new PostAdCatFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_post_ad_cat, container, false);
        if (view instanceof RecyclerView){
            list = (RecyclerView) view;
            list.setRecycledViewPool(recycledViewPool);
            controller.setSpanCount(SPAN_COUNT);
            list.setHasFixedSize(true);
            GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 2);
            gridLayoutManager.setSpanSizeLookup(controller.getSpanSizeLookup());
            list.setLayoutManager(gridLayoutManager);
            list.setAdapter(controller.getAdapter());

            /*
            String[] cats = getActivity().getResources().getStringArray(R.array.cat);

            catList = new ArrayList<>();
            for (String cat: cats){
                catList.add(new Cat(cat, false));
            }
            */

            controller.setData(catList);

            ShbService.getInstence().getRetrofit().create(UserService.class).getCats().enqueue(new Callback<List<Category>>() {
                @Override
                public void onResponse(Call<List<Category>> call, Response<List<Category>> response) {

                    catList = response.body();
                    controller.setData(catList);

                }

                @Override
                public void onFailure(Call<List<Category>> call, Throwable t) {

                }
            });

        }
        return view;
    }


    @Override
    public VerificationError verifyStep() {

        if (oldCat != null) {
            mListener.catSelected(oldCat);
            return null;
        }
        else {

            //TODO translastion res
            return new VerificationError("you must select on cat");
        }

    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }

    @Override
    public void catSelected(int pos, RadioButtonModel_ model_) {

        if (oldCat == null){
            oldCat = catList.get((int) model_.id());
            oldCat.setSelected(true);
        } else {
            oldCat.setSelected(false);
            oldCat = catList.get((int) model_.id());
            oldCat.setSelected(true);
        }

        controller.setData(catList);
        
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof SelectImagesFragment.MediaSelection) {
            mListener = (CategorySelection) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface CategorySelection{
        void catSelected(Category category);

    }


}
