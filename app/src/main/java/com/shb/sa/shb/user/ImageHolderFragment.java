package com.shb.sa.shb.user;


import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.common.ResizeOptions;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.shb.sa.shb.Application;
import com.shb.sa.shb.R;
import com.shb.sa.shb.REST.models.Media;
import com.shb.sa.shb.util.AndroidUtil;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ImageHolderFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ImageHolderFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private Media media;
    private View view;

    @BindView(R.id.image_holder_image_view)
    SimpleDraweeView image;


    public ImageHolderFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param picture Parameter 1.
     * @return A new instance of fragment ImageHolderFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ImageHolderFragment newInstance(Media picture) {
        ImageHolderFragment fragment = new ImageHolderFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM1, picture);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            media = getArguments().getParcelable(ARG_PARAM1);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        try {
            view = inflater.inflate(R.layout.fragment_image_holder, container, false);
            ButterKnife.bind(this, view);
        } catch (Exception e){
            Fresco.getImagePipeline().clearCaches();
            view = inflater.inflate(R.layout.fragment_image_holder, container, false);
            ButterKnife.bind(this, view);

        }
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        /*
        try {
            FirebaseStorage.getInstance()
                    .getReference().child(media.getUrl()).getDownloadUrl()
                    .addOnSuccessListener(uri -> {
                        image.setImageURI(uri);

                    });
        } catch (java.lang.IllegalArgumentException e){
            Log.w("ImageHolder", e.getLocalizedMessage());
        }

        */


        ImageRequest request = ImageRequestBuilder.newBuilderWithSource(Uri.parse(Application.baseStoreageUrl + media.getUrl()))
                .setResizeOptions(new ResizeOptions(300, AndroidUtil.getScreenWidth()))
                .build();
        image.setController(
                Fresco.newDraweeControllerBuilder()
                        .setOldController(image.getController())
                        .setImageRequest(request)
                        .build());


        //image.setImageURI(Application.baseStoreageUrl + media.getUrl());

        Log.d("Mooshy", Application.baseStoreageUrl + media.getUrl());
    }
}
