package com.shb.sa.shb.user;

import android.Manifest;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.GetTokenResult;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.marcinorlowski.fonty.Fonty;

import com.shb.sa.shb.Application;
import com.shb.sa.shb.BaseActivity;
import com.shb.sa.shb.R;
import com.shb.sa.shb.REST.ShbService;
import com.shb.sa.shb.REST.UserService;
import com.shb.sa.shb.REST.models.User;
import com.yalantis.ucrop.UCrop;
import com.zhihu.matisse.Matisse;
import com.zhihu.matisse.MimeType;
import com.zhihu.matisse.engine.impl.PicassoEngine;
import com.zhihu.matisse.internal.entity.CaptureStrategy;

import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.RuntimePermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@RuntimePermissions
public class ViewProfileActivity extends BaseActivity {

    private static final int REQUEST_CODE_CHOOSE = 1233;
    private static final int REQUEST_UPDATE_BIO = 153;

    private User user;
    @BindView(R.id.view_profile_activity_avatar)
    SimpleDraweeView avatar;
    //@BindView(R.id.view_profile_edit_about_me_btn)
    //Button editAboutme;

    @BindView(R.id.view_profile_edit_about_me_text)
    TextView aboutmeTextView;

    @BindView(R.id.view_profile_edit_username_text)
    TextView usernameText;
    @BindView(R.id.view_profile_phone_no_text)
    TextView phoneNoText;

    @BindView(R.id.view_profile_email_text)
    TextView email;

    @BindView(R.id.view_profile_switch_show_phone_no)
    Switch showPhoneNo;

    @BindView(R.id.view_edit_avatar_btn)
    Button editAvatar;

    @BindView(R.id.view_profile_edit_about_me_fab)
    FloatingActionButton editAboutMeFab;

    @BindView(R.id.screen)
    View screen;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_profile);
        ButterKnife.bind(this);

        FirebaseStorage storage = Application.getStorage();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);




        avatar.setOnClickListener(v ->
                ViewProfileActivityPermissionsDispatcher.showGalleryWithCheck(ViewProfileActivity.this));

        //editAboutme.setOnClickListener(v ->
        //        startActivityForResult(new Intent(ViewProfileActivity.this, EditUserInfoActivity.class), REQUEST_UPDATE_BIO));

        editAboutMeFab.setOnClickListener(v ->
                startActivityForResult(new Intent(ViewProfileActivity.this, EditUserInfoActivity.class), REQUEST_UPDATE_BIO));

        editAvatar.setOnClickListener(v ->
                ViewProfileActivityPermissionsDispatcher.showGalleryWithCheck(ViewProfileActivity.this));

        avatar.setImageURI(Application.baseStoreageUrl + mAuth.getCurrentUser().getUid() + "/profile_avatar/avatar.jpeg");


        mAuth.getCurrentUser().getIdToken(true).addOnSuccessListener(new OnSuccessListener<GetTokenResult>() {
            @Override
            public void onSuccess(GetTokenResult getTokenResult) {

                ShbService.getInstence().getRetrofit().create(UserService.class).getMyInfo(getTokenResult.getToken()).enqueue(new Callback<User>() {
                    @Override
                    public void onResponse(Call<User> call, Response<User> response) {

                        user = response.body();

                        updateUser();
                    }

                    @Override
                    public void onFailure(Call<User> call, Throwable t) {

                    }
                });
            }
        });

        showPhoneNo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mAuth.getCurrentUser().getIdToken(true).addOnCompleteListener(new OnCompleteListener<GetTokenResult>() {
                    @Override
                    public void onComplete(@NonNull Task<GetTokenResult> task) {
                        if (task.isSuccessful()){
                            ShbService.getInstence().getRetrofit().create(UserService.class).updateField(task.getResult().getToken(),"show_phone_no",String.valueOf(isChecked)).enqueue(new Callback<User>() {
                                @Override
                                public void onResponse(Call<User> call, Response<User> response) {

                                    user = response.body();
                                    updateUser();
                                }

                                @Override
                                public void onFailure(Call<User> call, Throwable t) {

                                }
                            });
                        }
                    }
                });
            }
        });
        Fonty.setFonts(this);
    }

    @NeedsPermission({Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE})
    void showGallery(){

        Matisse.from(this)
                .choose(MimeType.allOf())
                .countable(true)
                .capture(true)
                .captureStrategy(
                        new CaptureStrategy(true, "com.shb.sa.shwb.fileprovider"))
                .maxSelectable(1)
                .restrictOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
                .thumbnailScale(0.85f)
                .imageEngine(new PicassoEngine()).forResult(REQUEST_CODE_CHOOSE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_CHOOSE && resultCode == RESULT_OK){
            Log.d("mushy", String.valueOf(resultCode));
            List<Uri> mSelected = Matisse.obtainResult(data);
            if (mSelected.size() > 0) {
                UCrop.of(mSelected.get(0), Uri.parse(getExternalFilesDir(Environment.DIRECTORY_PICTURES).toURI().toString() + new Random().nextInt() +"png"))
                        .withAspectRatio(1,1)
                        .withMaxResultSize(100, 100)
                        .start(this);
            }
        }

        if (resultCode == RESULT_OK && requestCode == UCrop.REQUEST_CROP) {
            final Uri resultUri = UCrop.getOutput(data);
            Toast.makeText(this, resultUri.toString(), Toast.LENGTH_LONG).show();

            StorageReference storageRef = Application.getStorage().getReference();
            StorageReference ref = storageRef.child(mAuth.getCurrentUser().getUid() + "/profile_avatar/avatar.jpeg");
            UploadTask task = ref.putFile(resultUri);
            task.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    Toast.makeText(ViewProfileActivity.this, "Done Uploading", Toast.LENGTH_LONG).show();
                }
            });
        } else if (resultCode == UCrop.RESULT_ERROR) {
            final Throwable cropError = UCrop.getError(data);
        }


        if (resultCode == EditUserInfoActivity.RESULT_USER){
            Toast.makeText(this, "Got Result", Toast.LENGTH_SHORT).show();
            user = data.getParcelableExtra("user");
            updateUser();
        }
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;

    }


    private void updateUser(){
        if (user == null) return;
        // user info
        aboutmeTextView.setText(user.getAbout());
        usernameText.setText(user.getUsername());
        email.setText(user.getEmail());
        phoneNoText.setText(user.getPhoneNumber());
        showPhoneNo.setChecked(user.isShowPhoneNo());
        screen.setVisibility(View.VISIBLE);
    }
}
