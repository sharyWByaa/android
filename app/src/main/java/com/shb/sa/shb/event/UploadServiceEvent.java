package com.shb.sa.shb.event;

import com.shb.sa.shb.REST.models.Ad;
import com.shb.sa.shb.model.Media;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by mujtaba on 31/10/2017.
 */

public class UploadServiceEvent {

    public static final int NOT_RUNNING = 100;
    public static final int RUNNING = 102;
    public static final int FINISHED = 104;

    private int status;

    private Ad ad;

    private List<com.shb.sa.shb.model.Media> mediaList;

    public UploadServiceEvent(int status) {
        this.status = status;
    }


    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public List<Media> getMediaList() {
        return mediaList;
    }

    public void setMediaList(ArrayList<com.shb.sa.shb.model.Media> mediaList) {
        this.mediaList = mediaList;
    }

    public Ad getAd() {
        return ad;
    }

    public void setAd(Ad ad) {
        this.ad = ad;
    }
}
