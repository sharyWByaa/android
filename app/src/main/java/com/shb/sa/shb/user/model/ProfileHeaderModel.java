package com.shb.sa.shb.user.model;

import android.net.Uri;
import android.view.View;
import android.widget.TextView;

import com.airbnb.epoxy.EpoxyAttribute;
import com.airbnb.epoxy.EpoxyModelClass;
import com.airbnb.epoxy.EpoxyModelWithHolder;
import com.facebook.drawee.view.SimpleDraweeView;
import com.shb.sa.shb.R;

import butterknife.BindView;

import static com.airbnb.epoxy.EpoxyAttribute.Option.DoNotHash;

/**
 * Created by mujtaba on 20/06/2017.
 */
@EpoxyModelClass(layout = R.layout.model_profile_header)
public abstract class ProfileHeaderModel extends EpoxyModelWithHolder<ProfileHeaderModel.ImageHolder> {
    @EpoxyAttribute
    String username;
    @EpoxyAttribute
    String aboutme;
    @EpoxyAttribute(DoNotHash)
    View.OnClickListener clickListener;

    @EpoxyAttribute
    Uri imageUri;

    @Override
    public void bind(ImageHolder holder) {
        super.bind(holder);

        holder.profileArea.setOnClickListener(clickListener);
        holder.usernameTextView.setText(username);
        holder.aboutmeTextView.setText(aboutme);

        holder.avatar.setImageURI(imageUri);

    }

    @Override
    public int getSpanSize(int totalSpanCount, int position, int itemCount) {
        return totalSpanCount;
    }

    public static class ImageHolder extends BaseEpoxyHolder {
        @BindView(R.id.image_profile)
        SimpleDraweeView avatar;
        @BindView(R.id.model_profile_header_on_click)
        View profileArea;
        @BindView(R.id.model_profile_header_username_text)
        TextView usernameTextView;
        @BindView(R.id.model_profile_header_about_me_text)
        TextView aboutmeTextView;


    }

}
