package com.shb.sa.shb;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.shb.sa.shb.Registration.LoginActivity;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        Intent intent = null;


        if (FirebaseAuth.getInstance().getCurrentUser() == null) {
            Log.d("mushy", "Login");
            intent = new Intent(this, LoginActivity.class);
            if (intent.getExtras() != null) intent.putExtra("shb_intent", getIntent());
        }
        else {

            Log.d("mushy", "Homepage");
            intent = new Intent(this, HomePageActivity.class);

            if (getIntent().getExtras() != null) {
                intent.putExtra("shb_intent", getIntent());
                
            }
        }

        startActivity(intent);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("mushy", "on Activity result");
    }
}
