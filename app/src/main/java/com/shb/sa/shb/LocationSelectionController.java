package com.shb.sa.shb;

import com.airbnb.epoxy.Typed2EpoxyController;
import com.shb.sa.shb.model.citiy.City;
import com.shb.sa.shb.user.model.TextTitleModel_;

import java.util.List;
import java.util.Locale;

/**
 * Created by mujtaba on 18/01/2018.
 */

public class LocationSelectionController extends Typed2EpoxyController<List<City>, String> {

    private final Callback callback;

    public LocationSelectionController(Callback callback){
        this.callback = callback;
    }

    @Override
    protected void buildModels(List<City> data1,  String filter) {


        if (filter != null && !filter.trim().isEmpty()) {
            for (City s : data1) {
                if ((s.getArabicName().toLowerCase().contains(filter.toLowerCase().trim()) || s.getEnglishName().toLowerCase().contains(filter.toLowerCase().trim()))) {
                    new TextTitleModel_()
                            .id(s.getSaudiId())
                            .title(s.getName())
                            .onItemClicked((model, parentView, clickedView, position) -> callback.onCityClicked(s))
                            .addTo(this);

                }
            }
        } else {
            if (Locale.getDefault().getLanguage().trim().equals("ar")) {
                new TextTitleModel_().id("everywhere").title("جميع المناطق")
                        .onItemClicked((model, parentView, clickedView, position) -> {
                        callback.onCityClicked(null);
                        })
                        .addTo(this);
            }else {
                new TextTitleModel_().id("everywhere").title("every where")
                        .onItemClicked((model, parentView, clickedView, position) -> {
                            callback.onCityClicked(null);
                        })
                        .addTo(this);
            }

            for (City s : data1) {
                new TextTitleModel_().id(s.getSaudiId()).title(s.getName())
                        .onItemClicked((model, parentView, clickedView, position) -> callback.onCityClicked(s))
                        .addTo(this);

            }
        }
    }

    interface Callback {
        void onCityClicked(City city);
    }
}
