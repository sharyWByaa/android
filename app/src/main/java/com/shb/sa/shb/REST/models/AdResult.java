package com.shb.sa.shb.REST.models;

import java.io.Serializable;
import java.util.List;

/**
 * Created by mujtaba on 20/08/2017.
 */

public class AdResult implements Serializable{

    private List<Ad> results;
    private Cursor startCursor;
    private Cursor endCursor;



    public List<Ad> getResults() {
        return results;
    }

    public void setResults(List<Ad> results) {
        this.results = results;
    }

    public Cursor getStartCursor() {
        return startCursor;
    }

    public void setStartCursor(Cursor startCursor) {
        this.startCursor = startCursor;
    }

    public Cursor getEndCursor() {
        return endCursor;
    }

    public void setEndCursor(Cursor endCursor) {
        this.endCursor = endCursor;
    }



    public class Cursor implements Serializable{


        public Cursor(){}

        public String encoded;

    }
}
