package com.shb.sa.shb.user.model;

import com.nytimes.android.external.cache3.Preconditions;

import javax.annotation.Nonnull;

/**
 * Created by mujtaba on 20/08/2017.
 */




import java.io.Serializable;

/**
 * {@link BarCode Barcode} is used as a unique
 * identifier for a particular {@link Store  Store}
 * <p/>
 * Barcode will be passed to   Fetcher
 * and {@link   Persister}
 **/
@SuppressWarnings("PMD.SimplifyBooleanReturns")
public final class BarCodeWithCursor implements Serializable {

    private static final BarCodeWithCursor EMPTY_BARCODE = new BarCodeWithCursor("", "", "");

    @Nonnull
    private final String key;
    @Nonnull
    private final String type;

    private String city;

    @Nonnull
    private final String cursor;

    public BarCodeWithCursor(@Nonnull String key,@Nonnull String type, @Nonnull String cursor) {
        this.key = Preconditions.checkNotNull(key);
        this.type = Preconditions.checkNotNull(type);
        this.cursor = Preconditions.checkNotNull(cursor);
    }

    @Nonnull
    public static BarCodeWithCursor empty() {
        return EMPTY_BARCODE;
    }

    @Nonnull
    public String getKey() {
        return key;
    }

    @Nonnull
    public String getType() {
        return type;
    }

    @Nonnull
    public String getCursor() {
        return cursor;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }

        if (!(object instanceof BarCodeWithCursor)) {
            return false;
        }

        BarCodeWithCursor barCode = (BarCodeWithCursor) object;

        if (!key.equals(barCode.key)) {
            return false;
        }

        if (!type.equals(barCode.type)) {
            return false;
        }

        if (!type.equals(barCode.cursor)) {
            return false;
        }

        return true;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }


    @Override
    public int hashCode() {
        int result = key.hashCode();
        if (city == null) result = 31 * result + type.hashCode();
        else result = 31 * result + type.hashCode() + city.hashCode();
        result += cursor.hashCode();
        return result;
    }


    @Override
    public String toString() {
        return "BarCodeWithCursor{" +
                "key='" + key + '\'' +
                ", type='" + type + '\'' +
                ", city='" + city + '\'' +
                ", cursor='" + cursor + '\'' +
                '}';
    }



}