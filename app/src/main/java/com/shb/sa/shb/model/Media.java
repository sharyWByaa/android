package com.shb.sa.shb.model;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

import java.io.InputStream;

/**
 * Created by mujtaba on 28/05/2017.
 */

public class Media implements Parcelable {

    public static final int TYPE_IMAGE = 0;
    public final static int TYPE_VIDEO = 1;
    private int type;
    private Long duration;
    private String mediaKey;
    private String thumnailKey;
    private Uri thumnailKeyUri;
    public transient InputStream mediaStream;
    public transient InputStream thumnailStream;


    public Media(int type, Long duration, String mediaKey, String thumnailKey) {
        this.type = type;
        this.duration = duration;
        this.mediaKey = mediaKey;
        this.thumnailKey = thumnailKey;
    }

    public static int getTypeImage() {
        return TYPE_IMAGE;
    }

    public static int getTypeVideo() {
        return TYPE_VIDEO;
    }



    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public String getMediaKey() {
        return mediaKey;
    }

    public void setMediaKey(String mediaKey) {
        this.mediaKey = mediaKey;
    }

    public String getThumnailKey() {
        return thumnailKey;
    }

    public void setThumnailKey(String thumnailKey) {
        this.thumnailKey = thumnailKey;
    }

    public Uri getThumnailKeyUri() {
        return thumnailKeyUri;
    }

    public void setThumnailKeyUri(Uri thumnailKeyUri) {
        this.thumnailKeyUri = thumnailKeyUri;
    }



    protected Media(Parcel in) {
        type = in.readInt();
        duration = in.readByte() == 0x00 ? null : in.readLong();
        mediaKey = in.readString();
        thumnailKey = in.readString();
        thumnailKeyUri = (Uri) in.readValue(Uri.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(type);
        if (duration == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeLong(duration);
        }
        dest.writeString(mediaKey);
        dest.writeString(thumnailKey);
        dest.writeValue(thumnailKeyUri);

    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Media> CREATOR = new Parcelable.Creator<Media>() {
        @Override
        public Media createFromParcel(Parcel in) {
            return new Media(in);
        }

        @Override
        public Media[] newArray(int size) {
            return new Media[size];
        }
    };


    /*
    private void writeObject(ObjectOutputStream oos) throws IOException{
        // This will serialize all fields that you did not mark with 'transient'
        // (Java's default behaviour)
        oos.defaultWriteObject();
        // Now, manually serialize all transient fields that you want to be serialized
        if(screenshot!=null){
            ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
            boolean success = screenshot.compress(Bitmap.CompressFormat.PNG, 100, byteStream);
            if(success){
                oos.writeObject(byteStream.toByteArray());
            }
        }
    }

    private void readObject(ObjectInputStream ois) throws IOException, ClassNotFoundException{
        // Now, all again, deserializing - in the SAME ORDER!
        // All non-transient fields
        ois.defaultReadObject();
        // All other fields that you serialized
        byte[] image = (byte[]) ois.readObject();
        if(image != null && image.length > 0){
            screenshot = BitmapFactory.decodeByteArray(image, 0, image.length);
        }
    }

       public Bitmap getScreenshot() {
        return screenshot;
    }

    public void setScreenshot(Bitmap screenshot) {
        this.screenshot = screenshot;
    }
    */

}
