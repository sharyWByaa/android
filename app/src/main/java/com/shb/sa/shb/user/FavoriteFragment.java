package com.shb.sa.shb.user;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.GetTokenResult;
import com.nytimes.android.external.store3.base.impl.BarCode;
import com.nytimes.android.external.store3.base.impl.Store;
import com.shb.sa.shb.Application;
import com.shb.sa.shb.BaseFragment;
import com.shb.sa.shb.R;
import com.shb.sa.shb.REST.models.Ad;
import com.shb.sa.shb.REST.models.Favorite;
import com.shb.sa.shb.user.model.AdItemModel_;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FavoriteFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FavoriteFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FavoriteFragment extends BaseFragment implements FavController.AdapterCallbacks{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    private View view;

    private final RecyclerView.RecycledViewPool recycledViewPool = new RecyclerView.RecycledViewPool();
    private FavController favController;

    @BindView(R.id.list)
    RecyclerView list;
    private GridLayoutManager mLinearLayoutManager;
    private List<Favorite> favList;
    private ArrayList<Ad> adList;
    private Store<List<Favorite>, BarCode> favStore;

    public FavoriteFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FavoriteFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FavoriteFragment newInstance(String param1, String param2) {
        FavoriteFragment fragment = new FavoriteFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        favController = new FavController(recycledViewPool, getContext(), this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_favorite, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mLinearLayoutManager = new GridLayoutManager(getActivity(),2);
        favController.setSpanCount(2);
        mLinearLayoutManager.setSpanSizeLookup(favController.getSpanSizeLookup());
        list.setLayoutManager(mLinearLayoutManager);
        list.setRecycledViewPool(recycledViewPool);
        list.setHasFixedSize(true);
        list.setAdapter(favController.getAdapter());

        mAuth.getCurrentUser().getIdToken(true).addOnCompleteListener(new OnCompleteListener<GetTokenResult>() {
            @Override
            public void onComplete(@NonNull Task<GetTokenResult> task) {

                if (task.isSuccessful()){

                    if (getContext() != null) favStore = ((Application)getContext().getApplicationContext()).getFavStore();

                    if (favStore!= null && task.getResult() != null) {

                        favStore.get(new BarCode("fav", task.getResult().getToken())).subscribeOn(Schedulers.newThread())
                                .observeOn(AndroidSchedulers.mainThread()).subscribe((favorites -> {

                            if (favorites == null) return;
                            favList = favorites;
                            favController.setData(favList, true);
                            adList = new ArrayList<>();
                            for (Favorite favorite: favList){
                                adList.add(favorite.getAd());
                            }

                        }),(er) -> {
                                    //Log.d("mushy", ""er.getCause().getLocalizedMessage())
                                }
                        );
                    }
                    /*
                    ShbService.getInstence().getRetrofit().create(UserService.class).getMyFav(task.getResult().getToken()).enqueue(new Callback<List<Favorite>>() {
                        @Override
                        public void onResponse(Call<List<Favorite>> call, Response<List<Favorite>> response) {

                            if (response.body() == null) return;
                            favList = response.body();
                            favController.setData(favList, true);
                            adList = new ArrayList<>();
                            for (Favorite favorite: favList){
                                adList.add(favorite.getAd());
                            }
                        }

                        @Override
                        public void onFailure(Call<List<Favorite>> call, Throwable t) {

                        }
                    });
                    */
                }

            }
        });


    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void itemClicked(int pos, AdItemModel_ model_) {
        Ad fav = adList.get(pos);
        Intent intent = new Intent(getActivity(), ViewStoryActivity.class);
        intent.putExtra("itemNo", pos);
        intent.putExtra("adsList", adList);
        getActivity().startActivity(intent);

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
