package com.shb.sa.shb.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.util.DisplayMetrics;

import com.google.android.gms.appinvite.AppInviteInvitation;
import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.shb.sa.shb.R;

import java.io.File;
import java.io.IOException;

/**
 * Created by mujtaba on 22/08/2017.
 */

public class AndroidUtil {
    public static int dpToPx(int dp)
    {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static int pxToDp(int px)
    {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }

    public static int getWidth(Activity context){
        DisplayMetrics displayMetrics = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;

        return width;
    }

    public static int getScreenWidth() {
        return Resources.getSystem().getDisplayMetrics().widthPixels;
    }

    public static int getScreenHeight() {
        return Resources.getSystem().getDisplayMetrics().heightPixels;
    }

    public static File getVideoCacheDir(Context context) {
        // catch available media
        if (context.getExternalCacheDir() != null) return new File(context.getExternalCacheDir(), "video-cache");
        else return new File(context.getCacheDir(), "video-cache");
    }

    public static void cleanVideoCacheDir(Context context) throws IOException {
        File videoCacheDir = getVideoCacheDir(context);
        cleanDirectory(videoCacheDir);
    }

    private static void cleanDirectory(File file) throws IOException {
        if (!file.exists()) {
            return;
        }
        File[] contentFiles = file.listFiles();
        if (contentFiles != null) {
            for (File contentFile : contentFiles) {
                delete(contentFile);
            }
        }
    }

    private static void delete(File file) throws IOException {
        if (file.isFile() && file.exists()) {
            deleteOrThrow(file);
        } else {
            cleanDirectory(file);
            deleteOrThrow(file);
        }
    }

    private static void deleteOrThrow(File file) throws IOException {
        if (file.exists()) {
            boolean isDeleted = file.delete();
            if (!isDeleted) {
                throw new IOException(String.format("File %s can't be deleted", file.getAbsolutePath()));
            }
        }
    }

    public static Intent openMap(Context context, Double lat, Double lon) {

        String url = "https://www.google.com/maps/search/";
        Uri mapUri = Uri.parse(url + "?api=1&query="+ String.valueOf(lat) + "," + String.valueOf(lon));
        return new Intent(Intent.ACTION_VIEW, mapUri);
    }

    public static void sendInvite(Activity context, Uri deepLink){
        Intent intent = new AppInviteInvitation.IntentBuilder(context.getString(R.string.invitation_title))
                .setMessage(context.getString(R.string.invitation_message))
                .setDeepLink(deepLink)
//                .setCustomImage(Uri.parse(context.getString(R.string.invitation_custom_image)))
//                .setCallToActionText(context.getString(R.string.invitation_cta))
                .build();

        context.startActivityForResult(intent, 2321);
    }

    public static DynamicLink createDeepLink(Activity context){
        return FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLink(Uri.parse(context.getString(R.string.dynamic_link_website)))
                .setDynamicLinkDomain(context.getString(R.string.dynamic_link_domain))
                .setAndroidParameters(
                        new DynamicLink.AndroidParameters.Builder(context.getString(R.string.dynamic_link_android_package))
                                .setFallbackUrl(Uri.parse("https://shb-app.com"))
                                .build())
                .setSocialMetaTagParameters(
                        new DynamicLink.SocialMetaTagParameters.Builder()
                                .setTitle("Example of a Dynamic Link")
                                .setDescription("This link works whether the app is installed or not!")
                                .build())
                .buildDynamicLink();  // Or buildShortDynamicLink()

    }
}
