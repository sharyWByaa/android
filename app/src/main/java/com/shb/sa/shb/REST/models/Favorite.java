package com.shb.sa.shb.REST.models;

/**
 * Created by mujtaba on 15/08/2017.
 */

public class Favorite {

    String id;
    Ad ad;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Ad getAd() {
        return ad;
    }

    public void setAd(Ad ad) {
        this.ad = ad;
    }


}
