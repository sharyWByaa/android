package com.shb.sa.shb.user.model;

import com.airbnb.epoxy.DataBindingEpoxyModel;
import com.airbnb.epoxy.EpoxyAttribute;
import com.airbnb.epoxy.EpoxyModelClass;
import com.shb.sa.shb.R;

/**
 * Created by mujtaba on 10/08/2017.
 */
@EpoxyModelClass(layout = R.layout.model_chat_box)
public abstract class ChatBoxModel extends DataBindingEpoxyModel {
    @EpoxyAttribute
    String message;

    @Override
    public int getSpanSize(int totalSpanCount, int position, int itemCount) {
        return totalSpanCount;
    }
}
