package com.shb.sa.shb.chat;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.shb.sa.shb.Application;
import com.shb.sa.shb.R;
import com.shb.sa.shb.chat.model.ChatRoom;

import org.ocpsoft.prettytime.PrettyTime;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MyRecyclerViewAdapter extends RecyclerView.Adapter<MyRecyclerViewAdapter.ViewHolder> {

        private List<ChatRoom> mData = Collections.emptyList();
        private LayoutInflater mInflater;
        private ItemClickListener mClickListener;
        final PrettyTime p =new PrettyTime();

        // data is passed into the constructor
        public MyRecyclerViewAdapter(Context context, List<ChatRoom> data) {
            this.mInflater = LayoutInflater.from(context);
            this.mData = data;


        }

        public void setData(List<ChatRoom> mData){
            this.mData = mData;
            notifyDataSetChanged();
        }

        // inflates the row layout from xml when needed
        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = mInflater.inflate(R.layout.item_chat_list, parent, false);
            ViewHolder viewHolder = new ViewHolder(view);
            return viewHolder;
        }

        // binds the data to the textview in each row
        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            ChatRoom animal = mData.get(position);

            //TODO
            if (animal != null) {
                FirebaseDatabase.getInstance().getReference().child("users").child(animal.getOtherUserId()).child("username").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        holder.myTextView.setText(dataSnapshot.getValue().toString());
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
                holder.myTextView.setText(animal.getOtherUserId());
                holder.lastMessageTextView.setText(animal.getLastMessage());
                holder.avatar.setImageURI(Application.baseStoreageUrl + animal.getOtherUserId() + "/profile_avatar/avatar.jpeg");
                holder.timeStampTextView.setText(p.format(new Date(animal.getTimeStamp())));
                if (animal.getReadByMe()) holder.newMessage.setVisibility(View.INVISIBLE);
                else holder.newMessage.setVisibility(View.VISIBLE);

            }
        }

        // total number of rows
        @Override
        public int getItemCount() {
            return mData.size();
        }


        // stores and recycles views as they are scrolled off screen
        public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            @BindView(R.id.item_chat_list_text)
            public TextView myTextView;
            @BindView(R.id.item_chat_list_last_message_text)
            public TextView lastMessageTextView;
            @BindView(R.id.item_chat_list_timestamp)
            public TextView timeStampTextView;

            @BindView(R.id.item_chat_list_avatar)
            public SimpleDraweeView avatar;

            @BindView(R.id.item_chat_notification)
            TextView newMessage;

            public ViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);

                itemView.setOnClickListener(this);


            }

            @Override
            public void onClick(View view) {
                if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
            }
        }

        // convenience method for getting data at click position
        public ChatRoom getItem(int id) {
            return mData.get(id);
        }

        // allows clicks events to be caught
        public void setClickListener(ItemClickListener itemClickListener) {
            this.mClickListener = itemClickListener;
        }

        // parent activity will implement this method to respond to click events
        public interface ItemClickListener {
            void onItemClick(View view, int position);
        }
    }