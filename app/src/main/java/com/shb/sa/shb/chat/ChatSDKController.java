package com.shb.sa.shb.chat;

import android.support.v7.widget.RecyclerView;

import com.airbnb.epoxy.Typed2EpoxyController;
import com.shb.sa.shb.user.model.ChatBoxModel_;
import com.shb.sa.shb.user.model.ChatBoxNotModel_;

import java.util.Set;

import co.chatsdk.core.dao.Message;

/**
 * Created by mujtaba on 10/08/2017.
 */

public class ChatSDKController extends Typed2EpoxyController<Set<Message>, Boolean> {

    private final RecyclerView.RecycledViewPool recycledViewPool;
    private final String meUid;

    public ChatSDKController(RecyclerView.RecycledViewPool recycledViewPool,String meUid){
        this.recycledViewPool = recycledViewPool;
        this.meUid = meUid;

    }
    @Override
    protected void buildModels(Set<Message> data1, Boolean data2) {




        for(Message chat: data1){
            if(chat.getSender().getEntityID().equals(meUid)) {
                add(new ChatBoxModel_().id(chat.getId()).message(chat.getTextString()));
            }else {
                add(new ChatBoxNotModel_().id(chat.getId()).message(chat.getTextString()));
            }

        }
    }
}
