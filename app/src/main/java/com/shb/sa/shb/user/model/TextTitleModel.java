package com.shb.sa.shb.user.model;

import android.view.View;

import com.airbnb.epoxy.DataBindingEpoxyModel;
import com.airbnb.epoxy.EpoxyAttribute;
import com.airbnb.epoxy.EpoxyModelClass;
import com.shb.sa.shb.R;

import static com.airbnb.epoxy.EpoxyAttribute.Option.DoNotHash;

/**
 * Created by mujtaba on 20/06/2017.
 */
@EpoxyModelClass(layout = R.layout.model_text_title)
public abstract class TextTitleModel extends DataBindingEpoxyModel {
    @EpoxyAttribute
    String title;

    @EpoxyAttribute(DoNotHash)
    View.OnClickListener onItemClicked;

    @Override
    public int getSpanSize(int totalSpanCount, int position, int itemCount) {
        return totalSpanCount;
    }
}
