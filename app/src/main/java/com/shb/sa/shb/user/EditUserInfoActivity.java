package com.shb.sa.shb.user;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.GetTokenResult;
import com.shb.sa.shb.BaseActivity;
import com.shb.sa.shb.R;
import com.shb.sa.shb.REST.ShbService;
import com.shb.sa.shb.REST.UserService;
import com.shb.sa.shb.REST.models.User;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditUserInfoActivity extends BaseActivity {


    public static final int RESULT_USER = 325445;
    @BindView(R.id.edit_user_info_title_edit_text)
    EditText editTextInput;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_user_info);
        ButterKnife.bind(this);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_edit_user_info, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_main_setting:

                Toast.makeText(EditUserInfoActivity.this, "click", Toast.LENGTH_LONG).show();
                mAuth.getCurrentUser().getIdToken(true).addOnSuccessListener(new OnSuccessListener<GetTokenResult>() {
                    @Override
                    public void onSuccess(GetTokenResult getTokenResult) {
                        ShbService.getInstence().getRetrofit().create(UserService.class).updateField(getTokenResult.getToken(),
                                "about",editTextInput.getText().toString()).enqueue(new Callback<User>() {
                            @Override
                            public void onResponse(Call<User> call, Response<User> response) {
                                Toast.makeText(EditUserInfoActivity.this, "updated", Toast.LENGTH_LONG).show();

                                Intent intent = new Intent();
                                intent.putExtra("user", response.body());


                                setResult(RESULT_USER, intent);
                                finish();
                            }

                            @Override
                            public void onFailure(Call<User> call, Throwable t) {

                            }
                        });

                    }
                });

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
