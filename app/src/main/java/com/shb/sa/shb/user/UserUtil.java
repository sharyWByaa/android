package com.shb.sa.shb.user;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.StorageReference;
import com.shb.sa.shb.Application;

/**
 * Created by mujtaba on 11/08/2017.
 */

public class UserUtil {

    public static StorageReference getUserAvatar(){
        if (FirebaseAuth.getInstance().getCurrentUser() != null)
            return Application.getStorage().getReference().child(FirebaseAuth.getInstance().getCurrentUser().getUid()+"/profile_avatar/avatar.jpeg");
        else return null;
    }
}
