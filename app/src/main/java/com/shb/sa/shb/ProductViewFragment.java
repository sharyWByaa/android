package com.shb.sa.shb;


import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.danikula.videocache.HttpProxyCacheServer;


import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.decoder.DecoderCounters;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ConcatenatingMediaSource;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.cache.LeastRecentlyUsedCacheEvictor;
import com.google.android.exoplayer2.upstream.cache.SimpleCache;
import com.google.android.exoplayer2.util.Util;

import com.google.android.exoplayer2.video.VideoRendererEventListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.auth.GetTokenResult;
import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.storage.FirebaseStorage;
import com.google.gson.JsonObject;
import com.like.LikeButton;
import com.like.OnLikeListener;
import com.marcinorlowski.fonty.Fonty;

import java.io.File;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.iconics.IconicsDrawable;
import com.mypopsy.maps.StaticMap;
import com.shb.sa.shb.REST.ShbService;
import com.shb.sa.shb.REST.UserService;
import com.shb.sa.shb.REST.models.Ad;
import com.shb.sa.shb.REST.models.Media;
import com.shb.sa.shb.chat.Chat2Activity;
import com.shb.sa.shb.chat.logic.ChatInteractor;
import com.shb.sa.shb.chat.logic.ChatSDKExtention;
import com.shb.sa.shb.model.Specification;
import com.shb.sa.shb.user.ImageHolderFragment;
import com.shb.sa.shb.user.UserProfileActivity;
import com.shb.sa.shb.util.AndroidUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.chatsdk.core.session.NM;
import co.chatsdk.core.session.StorageManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.shb.sa.shb.Application.baseStoreageUrl;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ProductViewFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProductViewFragment extends BaseFragment implements Player.EventListener, OnMapReadyCallback, VideoRendererEventListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private View view;
    private TextureView videoView;
    private SimpleExoPlayer player;
    private Timeline.Window window;
    private ControlDispatcher controlDispatcher;
    private BottomSheetBehavior<NestedScrollView> behavior;
    private boolean is_played = false;

    private static final long MAX_POSITION_FOR_SEEK_TO_PREVIOUS = 3000;
    private RecyclerView listView;
    private Ad ad;

    private final FirebaseStorage storage = Application.getStorage();

    @BindView(R.id.msg_btn)
    Button msgBtn;
    @BindView(R.id.fav_btn)
    LikeButton favBtn;
    @BindView(R.id.call_btn)
    Button callBtn;

    @BindView(R.id.view_pager)
    ViewPager viewImagePager;

    @BindView(R.id.avatar)
    SimpleDraweeView avatar;

    @BindView(R.id.image_map)
    SimpleDraweeView map;

    @BindView(R.id.share_btn)
    TextView shareBtn;

    Boolean isFav;
    private ScreenSlidePagerAdapter mPagerAdapter;

    HttpProxyCacheServer proxy;
    private StoryControl mListener;
    private SupportMapFragment mapFragment;


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *

     * @return A new instance of fragment ProductViewFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ProductViewFragment newInstance(Ad ad) {
        ProductViewFragment fragment = new ProductViewFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM1, ad);


        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            ad = (Ad)getArguments().getSerializable(ARG_PARAM1);


        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (player != null) controlDispatcher.dispatchSetPlayWhenReady(player, false);


    }

    @Override
    public void onPause() {
        super.onPause();
        //if (player != null)  player.setPlayWhenReady(false);
        if (player != null) controlDispatcher.dispatchSetPlayWhenReady(player, false);


    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (!isVisibleToUser) {
            //do sth..
            if (player != null) controlDispatcher.dispatchSetPlayWhenReady(player, false);
        } else {
            if (player != null && behavior.getState() != BottomSheetBehavior.STATE_EXPANDED) controlDispatcher.dispatchSetPlayWhenReady(player, true);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        view = inflater.inflate(R.layout.fragment_product_view, container, false);
        ButterKnife.bind(this, view);
        proxy = Application.getProxy(getActivity());
        //videoView = (com.devbrackets.android.exomedia.ui.widget.VideoView) view.findViewById(R.id.video_view);
        //videoView.setOnPreparedListener(this);


        ((TextView)view.findViewById(R.id.text_title)).setText(ad.getTitle());
        ((TextView)view.findViewById(R.id.text_username)).setText(ad.getUser().getUsername());
        ((TextView)view.findViewById(R.id.text_desc)).setText(ad.getDesc());

        videoView = view.findViewById(R.id.video_view);



        //videoView.hideController();

        //videoView.setVideoURI(Uri.parse("https://s3-eu-west-1.amazonaws.com/etlaq-shb-ireland/Snapchat-1734828977.mp4"));




        try {
            initPlayer();
        } catch (IllegalArgumentException e){

            // Could not load the media
        }

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                next();
            }
        });


        initUi();

        return view;
    }

    private void initUi(){

        final FrameLayout framelayout = (FrameLayout) view.findViewById(R.id.frame_layout);
        final NestedScrollView scrollView = (NestedScrollView) view.findViewById(R.id.scroll_view);
        final NestedScrollView controll = (NestedScrollView) view.findViewById(R.id.scroll_view_controller);
        final View contactLayout = view.findViewById(R.id.contact_layout);
        //contactLayout.setTranslationY(100);


        listView = (RecyclerView)view.findViewById(R.id.list_view);
        LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(getActivity());
        listView.setLayoutManager(mLinearLayoutManager);
        RecyclerAdapter mAdapter = new RecyclerAdapter();
        listView.setAdapter(mAdapter);


        final CardView header = (CardView) view.findViewById(R.id.header_card_view);
        behavior = BottomSheetBehavior.from(scrollView);
        BottomSheetBehavior<NestedScrollView> controllBehavior = BottomSheetBehavior.from(controll);

        controllBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);


        controllBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {

                switch (newState){
                    case BottomSheetBehavior.STATE_COLLAPSED:
                        getActivity().finish();
                        break;

                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

                framelayout.setForeground(new ColorDrawable(Color.argb(255 - (int)Math.ceil((255)*slideOffset),255,255,255)));

            }
        });

        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {

                switch (newState){
                    case BottomSheetBehavior.STATE_EXPANDED:
                        if (player != null) controlDispatcher.dispatchSetPlayWhenReady(player, false);
                        //Toast.makeText(getActivity(), "expanded", Toast.LENGTH_SHORT).show();
                        break;
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_COLLAPSED:
                        //if (player != null) player.setPlayWhenReady(true);
                        //Toast.makeText(getActivity(), "Collapsed", Toast.LENGTH_SHORT).show();
                        if (player != null && isResumed()) controlDispatcher.dispatchSetPlayWhenReady(player, true);

                        scrollView.smoothScrollTo(0,0);

                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                }

            }


            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

                header.setCardBackgroundColor(Color.argb((int)Math.ceil((255)*slideOffset),27,171,221));
                header.setCardElevation(20*slideOffset);

                contactLayout.setVisibility(View.VISIBLE);

                //contactLayout.setBottom((int)Math.ceil(-10*slideOffset));
                contactLayout.setTranslationY(150 - (slideOffset*150));
                contactLayout.setAlpha(slideOffset);




            }
        });

        view.findViewById(R.id.arrow_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });

        view.findViewById(R.id.button_next).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                next();
            }
        });

        view.findViewById(R.id.button_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                previous();
            }
        });






        scrollView.getParent().requestChildFocus(scrollView, scrollView);

        view.findViewById(R.id.profile_pic).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), UserProfileActivity.class);
                intent.putExtra("uid", ad.getUser().getUid());
                startActivity(intent);
            }
        });

        if (mAuth != null && ad.getUser().getUid().equals(mAuth.getUid())) {
            msgBtn.setEnabled(false);
            callBtn.setEnabled(false);

        }else {
            msgBtn.setEnabled(true);
            callBtn.setEnabled(true);
        }
        msgBtn.setOnClickListener(v -> {
            Intent intent= new Intent(getActivity(), Chat2Activity.class);
            co.chatsdk.core.dao.User otherUser = StorageManager.shared().fetchOrCreateEntityWithEntityID(co.chatsdk.core.dao.User.class, ad.getUser().getUid());
            new ChatSDKExtention().get1to1Thread(NM.currentUser(), otherUser).subscribe((thread)->{
                intent.putExtra(ChatInteractor.ARG_RECEIVER_UID, ad.getUser().getUid());
                intent.putExtra("thread", thread.getEntityID());
                startActivity(intent);
            }, error -> Log.d("ProductViewFragment", error.getMessage()));


            //Thread th = StorageManager.shared().fetchThreadWithUsers(Collections.singletonList(otherUser));
            //Toast.makeText(getContext(),th.getEntityID(), Toast.LENGTH_LONG).show();



            //NetworkManager.shared().a.thread.createThread("test", Arrays.asList(otherUser, NM.currentUser()), ThreadType.Private1to1).subscribe((thread) -> {}, (error)->{});
        });

        favBtn.setOnLikeListener(new OnLikeListener() {
            @Override
            public void liked(LikeButton likeButton) {

                mAuth.getCurrentUser().getIdToken(true).addOnCompleteListener(task -> {
                    if (task.isSuccessful()){

                        ShbService.getInstence().getRetrofit().create(UserService.class).createFav(task.getResult().getToken(), ad.getId(), ad.getUser().getUid()).enqueue(new Callback<JsonObject>() {
                            @Override
                            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                                //Toast.makeText(getContext(), "Done", Toast.LENGTH_LONG).show();
                                isFav = true;
                                updateFav();
                                favBtn.setLiked(true);

                            }

                            @Override
                            public void onFailure(Call<JsonObject> call, Throwable t) {

                                favBtn.setLiked(false);
                                //Toast.makeText(getContext(), "failed to like" + t.getMessage(), Toast.LENGTH_SHORT).show();
                                Log.d("mushy", t.fillInStackTrace().toString());
                            }
                        });
                    } else {
                        favBtn.setLiked(false);
                        //Toast.makeText(getContext(), "failed to like Not succ", Toast.LENGTH_SHORT).show();
                    }
                });

            }

            @Override
            public void unLiked(LikeButton likeButton) {


                mAuth.getCurrentUser().getIdToken(true).addOnCompleteListener(task -> {

                    if (task.isSuccessful()){

                            ShbService.getInstence().getRetrofit().create(UserService.class).deleteAdFromFav(task.getResult().getToken(), ad.getId()).enqueue(new Callback<JsonObject>() {
                                @Override
                                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                                    isFav = false;
                                    updateFav();
                                    favBtn.setLiked(false);

                                }

                                @Override
                                public void onFailure(Call<JsonObject> call, Throwable t) {

                                    favBtn.setLiked(true);

                                }
                            });


                    }
                });

            }
        });

        shareBtn.setOnClickListener(view -> {
            DynamicLink deepLink = AndroidUtil.createDeepLink(getActivity());
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT,  ad.getTitle() + " " + deepLink.getUri().toString());
            sendIntent.setType("text/plain");
            startActivity(sendIntent);
            //AndroidUtil.sendInvite(getActivity(), deepLink.getUri());
        });

        //TODO to delete
        /*
        favBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                mAuth.getCurrentUser().getIdToken(true).addOnCompleteListener(new OnCompleteListener<GetTokenResult>() {
                    @Override
                    public void onComplete(@NonNull Task<GetTokenResult> task) {
                        if (task.isSuccessful()){
                            if (isFav){
                                ShbService.getInstence().getRetrofit().create(UserService.class).deleteAdFromFav(task.getResult().getToken(), ad.getId()).enqueue(new Callback<JsonObject>() {
                                    @Override
                                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                                        Toast.makeText(getContext(), "Delete", Toast.LENGTH_SHORT).show();
                                        isFav = false;
                                        updateFav();
                                        favBtn.setLiked(false);

                                    }

                                    @Override
                                    public void onFailure(Call<JsonObject> call, Throwable t) {

                                    }
                                });

                            }else {
                                ShbService.getInstence().getRetrofit().create(UserService.class).createFav(task.getResult().getToken(), ad).enqueue(new Callback<JsonObject>() {
                                    @Override
                                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                                        Toast.makeText(getContext(), "Done", Toast.LENGTH_LONG).show();
                                        isFav = true;
                                        updateFav();
                                        favBtn.setLiked(true);

                                    }

                                    @Override
                                    public void onFailure(Call<JsonObject> call, Throwable t) {

                                    }
                                });
                            }
                        }
                    }
                });

            }
        });

        */

        mAuth.getCurrentUser().getIdToken(true).addOnCompleteListener(new OnCompleteListener<GetTokenResult>() {
            @Override
            public void onComplete(@NonNull Task<GetTokenResult> task) {

                if (task.isSuccessful()){
                    ShbService.getInstence().getRetrofit().create(UserService.class).findFav(task.getResult().getToken(), String.valueOf(ad.getId())).enqueue(new Callback<JsonObject>() {
                        @Override
                        public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {



                            if (response.body() != null) {
                                isFav = response.body().get("value").getAsBoolean();
                                updateFav();
                            }


                        }

                        @Override
                        public void onFailure(Call<JsonObject> call, Throwable t) {

                        }
                    });
                }
            }
        });


        //getChildFragmentManager().beginTransaction()
         //       .add(R.id.product_view_frame_image_gallery,
         //               ImagePagerFragment.newInstance((ArrayList<Media>) ad.getPictures()))
         //       .commit();



        if (ad.getPictures() != null && ad.getPictures().size() > 0) {
            viewImagePager.setVisibility(View.VISIBLE);
            mPagerAdapter = new ScreenSlidePagerAdapter(getChildFragmentManager(), ad.getPictures());
            viewImagePager.setAdapter(mPagerAdapter);
        } else {
            viewImagePager.setVisibility(View.GONE);
        }

        scrollView.getParent().requestChildFocus(view.findViewById(R.id.arrow_btn),view.findViewById(R.id.arrow_btn));

        scrollView.setFocusableInTouchMode(true);
        scrollView.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);

        // setup avatar

        avatar.setImageURI(baseStoreageUrl + ad.getUser().getUid() + "/profile_avatar/avatar.jpeg");




        map.getLayoutParams().width = AndroidUtil.getScreenWidth();
        map.requestLayout();


        favBtn.setLikeDrawable(toBitMap(new IconicsDrawable(getActivity())
                .icon(FontAwesome.Icon.faw_heart)
                .color(Color.RED)
                .sizeDp(24).shadowDp(1,1,1,Color.BLACK)));

        favBtn.setUnlikeDrawable(toBitMap(new IconicsDrawable(getActivity())
                .icon(FontAwesome.Icon.faw_heart_o)
                .color(Color.WHITE)
                .sizeDp(24)));


        Fonty.setFonts((ViewGroup) view);


    }

    private BitmapDrawable toBitMap(Drawable drawable){

        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(),
                drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return new BitmapDrawable(getResources(),bitmap);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // init map

        // if no location hide it
        if (ad.getLatLng() == null) {

            // remove Map

            map.setVisibility(View.GONE);

        }else {

            map.setVisibility(View.VISIBLE);

            StaticMap staticMap = new StaticMap()
                    .center(ad.getLatLng().latitude, ad.getLatLng().longitude)
                    .size(AndroidUtil.getScreenWidth()  , AndroidUtil.dpToPx(250))
                    .marker(ad.getLatLng().latitude, ad.getLatLng().longitude)
                    .zoom(16);

            try {


                map.setImageURI(Uri.parse(staticMap.toURL().toString()));
                map.setOnClickListener( view1 ->
                        startActivity(AndroidUtil.openMap(getActivity(), ad.getLatLng().latitude, ad.getLatLng().longitude)));
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }


        }


    }

    void updateFav(){
        favBtn.setEnabled(true);
        if(getContext() != null) {
            if (isFav) {

                favBtn.setLiked(true);

            } else {

                favBtn.setLiked(false);

            }
        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);


        if (context instanceof StoryControl) {
            mListener = (StoryControl) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }

    }



    @Override
    public void onResume() {
        super.onResume();

        if(getUserVisibleHint() && player != null && behavior.getState() != BottomSheetBehavior.STATE_EXPANDED) controlDispatcher.dispatchSetPlayWhenReady(player, true);




    }

    private void initPlayer(){


        // 1. Create a default TrackSelector
        Handler mainHandler = new Handler();
        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();

        TrackSelection.Factory videoTrackSelectionFactory =
                new AdaptiveTrackSelection.Factory(bandwidthMeter);

        TrackSelector trackSelector =
                new DefaultTrackSelector(videoTrackSelectionFactory);

        window = new Timeline.Window();
        controlDispatcher = DEFAULT_CONTROL_DISPATCHER;



// 2. Create the player
        player =
                ExoPlayerFactory.newSimpleInstance(getActivity(), trackSelector);


        player.setVideoTextureView(videoView);
        // Bind the player to the view.
        //videoView.setPlayer(player);



        // Measures bandwidth during playback. Can be null if not required.
        DefaultBandwidthMeter bandwidthMeterDefault = new DefaultBandwidthMeter();
// Produces DataSource instances through which media data is loaded.

        SimpleCache cache = new SimpleCache(new File(getActivity().getCacheDir(), "responses"), new LeastRecentlyUsedCacheEvictor(1024 * 1024 * 10));
        DefaultDataSourceFactory defultDatasource = new DefaultDataSourceFactory(getActivity(), Util.getUserAgent(getActivity(), "yourApplicationName"), bandwidthMeterDefault);
        //DataSource.Factory dataSourceFactory = new CacheDataSourceFactory(cache,defultDatasource, CacheDataSource.FLAG_IGNORE_CACHE_ON_ERROR);
        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(getActivity(),
                Util.getUserAgent(getActivity(), "yourApplicationName"), bandwidthMeterDefault);
        //DataSource.Factory dataSourceFactory = new CacheDataSourceFactory(getActivity(),
        //        Util.getUserAgent(getActivity(), "yourApplicationName"), bandwidthMeterDefault);
// Produces Extractor instances for parsing the media data.
        ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();
// This is the MediaSource representing the media to be played.
        List<MediaSource> mediaSourceList = new ArrayList<>();

        List<Task<Uri>> tasks = new ArrayList<>();
        for (Media media: ad.getMedias()){


            Task<Uri> task = storage.getReferenceFromUrl(baseStoreageUrl + media.getUrl()).getDownloadUrl();
            task.addOnSuccessListener(new OnSuccessListener<Uri>() {
                @Override
                public void onSuccess(Uri uri) {

                    MediaSource videoSource = new ExtractorMediaSource(Uri.parse(proxy.getProxyUrl(uri.toString())),
                            dataSourceFactory, extractorsFactory, null, null);

                    mediaSourceList.add(videoSource);
                }
            });

             tasks.add(task);

            //MediaSource videoSource = new ExtractorMediaSource(Uri.parse(proxy.getProxyUrl("https://firebasestorage.googleapis.com" + media.getUrl() + "?alt=media")),
            //        dataSourceFactory, extractorsFactory, null, null);

        }

        Tasks.whenAll(tasks).addOnSuccessListener(aVoid -> {

            ConcatenatingMediaSource concatenatedSource =
                    new ConcatenatingMediaSource(mediaSourceList.toArray(new MediaSource[mediaSourceList.size()]));
            // Prepare the player with the source.
            player.prepare(concatenatedSource);

            player.addListener(ProductViewFragment.this);


        });





    }


    @Override
    public void onDetach() {
        super.onDetach();
        if (player != null) player.release();
        mListener = null;

    }





    @Override
    public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {

    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {


    }

    @Override
    public void onLoadingChanged(boolean isLoading) {

    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {

        switch (playbackState){

            case Player.STATE_ENDED:
                is_played = true;
                mListener.goToNextStory();
                controlDispatcher.dispatchSetPlayWhenReady(player, false);
                player.seekToDefaultPosition();
                /*
                behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                //previous();
                player.seekToDefaultPosition();
                */

                break;
            case Player.STATE_IDLE:
                break;
            case Player.STATE_BUFFERING:
                break;
            case Player.STATE_READY:
                break;
        }

    }

    @Override
    public void onRepeatModeChanged(int i) {

    }

    @Override
    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {

    }

    @Override
    public void onPositionDiscontinuity(int reason) {

    }



    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

    }

    @Override
    public void onSeekProcessed() {

    }

    private void next() {
        Timeline timeline = player.getCurrentTimeline();
        if (timeline.isEmpty()) {
            return;
        }
        int windowIndex = player.getCurrentWindowIndex();
        if (windowIndex < timeline.getWindowCount() - 1) {
            seekTo(windowIndex + 1, C.TIME_UNSET);
        } else if (timeline.getWindow(windowIndex, window, false).isDynamic) {
            seekTo(windowIndex, C.TIME_UNSET);
        }
    }

    private void reset() {

        seekTo(-1, C.TIME_UNSET);


    }

    private void previous() {
        Timeline timeline = player.getCurrentTimeline();
        if (timeline.isEmpty()) {
            return;
        }
        int windowIndex = player.getCurrentWindowIndex();
        timeline.getWindow(windowIndex, window);
        if (windowIndex > 0 && (player.getCurrentPosition() <= MAX_POSITION_FOR_SEEK_TO_PREVIOUS
                || (window.isDynamic && !window.isSeekable))) {
            seekTo(windowIndex - 1, C.TIME_UNSET);
        } else {
            seekTo(0);
        }
    }

    private void seekTo(long positionMs) {
        seekTo(player.getCurrentWindowIndex(), positionMs);
    }

    private void seekTo(int windowIndex, long positionMs) {
        boolean dispatched = controlDispatcher.dispatchSeekTo(player, windowIndex, positionMs);
        if (!dispatched) {
            // The seek wasn't dispatched. If the progress bar was dragged by the user to perform the
            // seek then it'll now be in the wrong position. Trigger a progress update to snap it back.
            //updateProgress();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (ad != null && ad.getLatLng() != null) {
            googleMap.addMarker(new MarkerOptions().position(ad.getLatLng()));
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(ad.getLatLng(),14));

        }

        googleMap.setOnMapClickListener(latLng -> startActivity(AndroidUtil.openMap(getContext(), ad.getLatLng().latitude, ad.getLatLng().longitude)));
    }

    @Override
    public void onVideoEnabled(DecoderCounters counters) {

    }

    @Override
    public void onVideoDecoderInitialized(String decoderName, long initializedTimestampMs, long initializationDurationMs) {

    }

    @Override
    public void onVideoInputFormatChanged(Format format) {

    }

    @Override
    public void onDroppedFrames(int count, long elapsedMs) {

    }

    @Override
    public void onVideoSizeChanged(int width, int height, int unappliedRotationDegrees, float pixelWidthHeightRatio) {

        int viewWidth = videoView.getWidth();
        int viewHeight = videoView.getHeight();

        float pivotX = viewWidth / 2f;
        float pivotY = viewHeight / 2f;

        Matrix transform = new Matrix();
        transform.postRotate(unappliedRotationDegrees, pivotX, pivotY);
        if (unappliedRotationDegrees == 90 || unappliedRotationDegrees == 270) {
            float viewAspectRatio = (float) viewHeight / viewWidth;
            transform.postScale(1 / viewAspectRatio, viewAspectRatio, pivotX, pivotY);
        }
        videoView.setTransform(transform);
    }

    @Override
    public void onRenderedFirstFrame(Surface surface) {

    }

    @Override
    public void onVideoDisabled(DecoderCounters counters) {

    }


    public interface StoryControl{
        void goToNextStory();
    }



    public interface ControlDispatcher {

        /**
         * Dispatches a {@link ExoPlayer#setPlayWhenReady(boolean)} operation.
         *
         * @param player The player to which the operation should be dispatched.
         * @param playWhenReady Whether playback should proceed when ready.
         * @return True if the operation was dispatched. False if suppressed.
         */
        boolean dispatchSetPlayWhenReady(ExoPlayer player, boolean playWhenReady);

        /**
         * Dispatches a {@link ExoPlayer#seekTo(int, long)} operation.
         *
         * @param player The player to which the operation should be dispatched.
         * @param windowIndex The index of the window.
         * @param positionMs The seek position in the specified window, or {@link C#TIME_UNSET} to seek
         *     to the window's default position.
         * @return True if the operation was dispatched. False if suppressed.
         */
        boolean dispatchSeekTo(ExoPlayer player, int windowIndex, long positionMs);

    }

    /**
     * Default {@link ControlDispatcher} that dispatches operations to the player without
     * modification.
     */
    public static final ControlDispatcher DEFAULT_CONTROL_DISPATCHER = new ControlDispatcher() {

        @Override
        public boolean dispatchSetPlayWhenReady(ExoPlayer player, boolean playWhenReady) {
            player.setPlayWhenReady(playWhenReady);
            return true;
        }

        @Override
        public boolean dispatchSeekTo(ExoPlayer player, int windowIndex, long positionMs) {
            player.seekTo(windowIndex, positionMs);
            return true;
        }

    };


    class RecyclerAdapter extends RecyclerView.Adapter<ProductViewFragment.RecyclerAdapter.StoryHolder> implements View.OnClickListener
    {

        List<Specification> specificationList;

        public RecyclerAdapter(){
            specificationList = new ArrayList<>();

            specificationList.add(new Specification("المصنع","تويوتا"));
            specificationList.add(new Specification("النوع","كامري"));
            specificationList.add(new Specification("سنة التصنيع","2012"));
            specificationList.add(new Specification("الكيلومترات","199K"));
        }


        @Override
        public ProductViewFragment.RecyclerAdapter.StoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View inflatedView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_specification, parent, false);
            inflatedView.setOnClickListener(this);


            Fonty.setFonts((ViewGroup) inflatedView);

            return new ProductViewFragment.RecyclerAdapter.StoryHolder(inflatedView);
        }

        @Override
        public void onBindViewHolder(ProductViewFragment.RecyclerAdapter.StoryHolder holder, int position) {
            holder.bindItem(specificationList.get(position));

        }

        @Override
        public int getItemCount() {
            return specificationList.size();
        }

        @Override
        public void onClick(View v) {
           //
        }

        class StoryHolder extends RecyclerView.ViewHolder{


            private final TextView title;
            private final TextView value;
            private View view;
            public StoryHolder(View itemView) {
                super(itemView);
                this.view = itemView;
                title = (TextView) itemView.findViewById(R.id.title_text);
                value = (TextView)itemView.findViewById(R.id.value_text);
            }

            protected void bindItem(Specification specification){

                title.setText(specification.getTitle());
                value.setText(specification.getValue());

            }

        }
    }


    /**
     * A simple pager adapter that represents 5 ScreenSlidePageFragment objects, in
     * sequence.
     */
    private  class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        private final List<Media> mediaList;

        ScreenSlidePagerAdapter(FragmentManager fm, List<Media> mediaList) {
            super(fm);
            this.mediaList = mediaList;
        }

        @Override
        public Fragment getItem(int position) {
            return ImageHolderFragment.newInstance(mediaList.get(position));
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            super.destroyItem(container, position, object);
        }

        @Override
        public int getCount() {
            if (mediaList == null) return 0;
            return mediaList.size();
        }
    }


}
