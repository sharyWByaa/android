package com.shb.sa.shb.chat;

import android.content.Context;
import android.support.v7.widget.RecyclerView;

import com.airbnb.epoxy.Typed2EpoxyController;
import com.shb.sa.shb.chat.model.Chat;
import com.shb.sa.shb.user.model.ChatBoxModel_;
import com.shb.sa.shb.user.model.ChatBoxNotModel_;


import java.util.List;

/**
 * Created by mujtaba on 10/08/2017.
 */

public class ChatController extends Typed2EpoxyController<List<Chat>, Boolean> {

    private final RecyclerView.RecycledViewPool recycledViewPool;
    private final Context context;
    private final String meUid;

    public ChatController(RecyclerView.RecycledViewPool recycledViewPool, Context context, String meUid){
        this.recycledViewPool = recycledViewPool;
        this.context = context;
        this.meUid = meUid;

    }
    @Override
    protected void buildModels(List<Chat> data1, Boolean data2) {



        for(Chat chat: data1){
            if(chat.senderUid.equals(meUid)) {
                add(new ChatBoxModel_().id(chat.timestamp).message(chat.message));
            }else {
                add(new ChatBoxNotModel_().id(chat.timestamp).message(chat.message));
            }

        }
    }
}
