package com.shb.sa.shb.user.model;

import android.view.View;

import com.airbnb.epoxy.EpoxyAttribute;
import com.airbnb.epoxy.EpoxyModel;
import com.airbnb.epoxy.EpoxyModelClass;
import com.google.firebase.storage.FirebaseStorage;
import com.shb.sa.shb.Application;
import com.shb.sa.shb.R;
import com.shb.sa.shb.REST.models.Category;

import static com.airbnb.epoxy.EpoxyAttribute.Option.DoNotHash;
import static com.shb.sa.shb.Application.baseStoreageUrl;

/**
 * Created by mujtaba on 18/06/2017.
 */

@EpoxyModelClass(layout = R.layout.model_explore_feature_item)
public abstract class AdItemFeatureModel extends EpoxyModel<AdItemFeatureView> {

    @EpoxyAttribute String title;
    @EpoxyAttribute
    Category key;
    @EpoxyAttribute float subtitle;
    @EpoxyAttribute String username;
    @EpoxyAttribute String date;
    @EpoxyAttribute String screenshotPath;
    @EpoxyAttribute(DoNotHash)
    View.OnClickListener onClickListener;

    private final FirebaseStorage storage = Application.getStorage();

    @Override
    public void bind(AdItemFeatureView adHolder) {
        adHolder.title.setText(title);
        adHolder.caption.setAmount(subtitle);
        adHolder.username.setText(username);
        adHolder.date.setText(date);
        adHolder.setOnClickListener(onClickListener);

        //StorageReference imgUrl = storage.getReferenceFromUrl(("https://firebasestorage.googleapis.com"+ screenshotPath));

        adHolder.screenshot.setImageURI((baseStoreageUrl + screenshotPath));
        //imgUrl.getDownloadUrl().addOnSuccessListener( uri -> adHolder.screenshot.setImageURI(uri));
        //adHolder.screenshot.setImageURI("https://firebasestorage.googleapis.com"+ screenshotPath.replace("images/", "images%2F") +"?alt=media");

    }

    @Override
    public int getSpanSize(int totalSpanCount, int position, int itemCount) {
        // We want the header to take up all spans so it fills the screen width
        return totalSpanCount/2;
    }

}
