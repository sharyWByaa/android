package com.shb.sa.shb.user;


import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.marcinorlowski.fonty.Fonty;

import com.shb.sa.shb.FeatureFragment;
import com.shb.sa.shb.HomePageActivity;
import com.shb.sa.shb.LocationSelectionActivity;
import com.shb.sa.shb.R;
import com.shb.sa.shb.REST.models.Category;
import com.shb.sa.shb.UploadService;
import com.shb.sa.shb.event.UploadServiceEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ExploreFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ExploreFragment extends Fragment implements ServiceConnection {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String CATEGORY_LIST = "CatList";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private View view;
    private DemoCollectionPagerAdapter mDemoCollectionPagerAdapter;
    private ViewPager mViewPager;
    private String[] cats;
    private List<Category> catsList = new ArrayList<>();

    @BindView(R.id.avatar)
    SimpleDraweeView avatar;

    @BindView(R.id.location_selection_btn)
    Button locationSelection;

    private List<Fragment> fragmentList = new ArrayList<>();

    boolean mBound = false;
    private UploadService service;
    private ShimmerFrameLayout uploadBtn;


    public ExploreFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ExploreFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ExploreFragment newInstance(String param1, String param2) {
        ExploreFragment fragment = new ExploreFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }


        //initalise Cat
        String[] catid = getResources().getStringArray(R.array.cat_id);
        String[] cats = getResources().getStringArray(R.array.cat);


        List<Category>  newCatList = new ArrayList<>();
        newCatList.add(new Category("", "Explore"));
        for (int i = 0; catid.length > i; i++){
            newCatList.add(new Category(catid[i], cats[i]));
        }
        catsList = newCatList;

        fragmentList.add(FeatureFragment.newInstance("", ""));

        for (Category category : catsList) {

            if (category.getUid().equals("nearme")){

                fragmentList.add(ExploreItemFragment.newInstance(category, null));
            }else {
                fragmentList.add(ExploreItemFragment.newInstance(category, null));
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        cats = getActivity().getResources().getStringArray(R.array.cat);


        view = inflater.inflate(R.layout.fragment_explore, container, false);
        ButterKnife.bind(this, view);

        mViewPager = view.findViewById(R.id.pager);
        ((HomePageActivity)getActivity()).setSupportActionBar((android.support.v7.widget.Toolbar) view.findViewById(R.id.main_toolbar));

        ((HomePageActivity)getActivity()).getSupportActionBar().setTitle("");
        (view.findViewById(R.id.coordinator)).setPadding(0, getStatusBarHeight(), 0, 0);



        mDemoCollectionPagerAdapter =
                new DemoCollectionPagerAdapter(
                        getChildFragmentManager(), fragmentList);

        mViewPager.setAdapter(mDemoCollectionPagerAdapter);
        mDemoCollectionPagerAdapter.setFragmentList(fragmentList);
        mDemoCollectionPagerAdapter.notifyDataSetChanged();

        ((TabLayout)view.findViewById(R.id.pager_title)).setupWithViewPager(mViewPager);


        /*
        if (savedInstanceState != null) {
            catsList = savedInstanceState.getParcelableArrayList(CATEGORY_LIST);

            mDemoCollectionPagerAdapter =
                    new DemoCollectionPagerAdapter(
                            getChildFragmentManager());

            mDemoCollectionPagerAdapter.notifyDataSetChanged();
            mViewPager = (ViewPager) view.findViewById(R.id.pager);
            ((TabLayout)view.findViewById(R.id.pager_title)).setupWithViewPager(mViewPager);
            mViewPager.setAdapter(mDemoCollectionPagerAdapter);

        }

        ShbService.getInstence().getRetrofit().create(UserService.class).getCats().enqueue(new Callback<List<Category>>() {
            @Override
            public void onResponse(Call<List<Category>> call, Response<List<Category>> response) {
                if (!compareList(catsList, response.body())) {
                    if (isDetached()) return;
                    if (!isAdded()) return;
                    catsList = response.body();

                    mDemoCollectionPagerAdapter =
                            new DemoCollectionPagerAdapter(
                                    getChildFragmentManager());

                    mDemoCollectionPagerAdapter.notifyDataSetChanged();
                    mViewPager = (ViewPager) view.findViewById(R.id.pager);
                    ((TabLayout)view.findViewById(R.id.pager_title)).setupWithViewPager(mViewPager);
                    mViewPager.setAdapter(mDemoCollectionPagerAdapter);
                }
            }

            @Override
            public void onFailure(Call<List<Category>> call, Throwable t) {

            }
        });

        */

        UserUtil.getUserAvatar().getDownloadUrl().addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if (task.isSuccessful()){
                    avatar.setImageURI(task.getResult());
                }
            }
        });


        locationSelection.setOnClickListener((view) -> {
            Intent intent = new Intent(getActivity(), LocationSelectionActivity.class );
            presentActivity(view, intent);
        });

        Fonty.setFonts((ViewGroup) view);

        uploadBtn =
                (ShimmerFrameLayout) view.findViewById(R.id.shimmer_view_container);
        uploadBtn.startShimmerAnimation();




        return view;
    }

    boolean compareList(List<Category> cat1, List<Category> cat2){
        if (cat1 == null || cat2 == null) return false;
        if (cat1.size() != cat2.size()) return false;
        for (int i =0; i < cat1.size(); i++){
            if(cat1.get(i).getUid().equals(cat2.get(i).getUid())){
            }else {
                return false;
            }
        }
        return true;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);

    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();



        Intent intent= new Intent(getContext(), UploadService.class);
        getActivity().bindService(intent, this, Context.BIND_AUTO_CREATE);

        Fonty.setFonts((ViewGroup) view);
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unbindService(this);
        mBound = false;
    }

    @Override
    public void onSaveInstanceState(Bundle state) {
        super.onSaveInstanceState(state);
        //state.putParcelableArrayList(CATEGORY_LIST, (ArrayList<? extends Parcelable>) catsList);
    }


    public static boolean isRTL(Locale locale) {
        final int directionality = Character.getDirectionality(locale.getDisplayName().charAt(0));
        return directionality == Character.DIRECTIONALITY_RIGHT_TO_LEFT ||
                directionality == Character.DIRECTIONALITY_RIGHT_TO_LEFT_ARABIC;
    }

    public static boolean isRTL() {
        return isRTL(Locale.getDefault());
    }

    @Override
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {

        //Toast.makeText(getContext(), "IT has been Bind", Toast.LENGTH_LONG).show();

        service = ((UploadService.LocalBinder)iBinder).getService();
        mBound = true;

        if (service.isRunning()){
            //uploadBtn.setVisibility(View.VISIBLE);
            Log.d("mushy", "uploading");
        } else {
           // uploadBtn.setVisibility(View.GONE);
            Log.d("mushy", "Not uploading");
        }
        Log.d("mushy", "Binded");


    }

    @Override
    public void onServiceDisconnected(ComponentName componentName) {

        //Toast.makeText(getContext(), "IT has been Bind Disconnected", Toast.LENGTH_LONG).show();

        uploadBtn.setVisibility(View.GONE);
        Log.d("mushy", "Binded");


    }


    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(UploadServiceEvent event) {

        Toast.makeText(getContext(), "Stickey", Toast.LENGTH_LONG).show();
        switch (event.getStatus()){
            case UploadServiceEvent.RUNNING:

                uploadBtn.setVisibility(View.VISIBLE);
                Log.d("mushy", "uploading");
                break;
            case UploadServiceEvent.FINISHED:

                uploadBtn.setVisibility(View.GONE);
                Log.d("mushy", "Not uploading");

                break;
        }
    }


    // Since this is an object collection, use a FragmentStatePagerAdapter,
// and NOT a FragmentPagerAdapter.
    public class DemoCollectionPagerAdapter extends FragmentStatePagerAdapter {
        private  List<Fragment> fragmentList;

        public DemoCollectionPagerAdapter(FragmentManager fm, List<Fragment> fragmentList) {
            super(fm);
            this.fragmentList = fragmentList;


        }

        public void setFragmentList(List<Fragment> fragmentList) {
            this.fragmentList = fragmentList;
        }

        public List<Fragment> getFragmentList() {
            return fragmentList;
        }

        @Override
        public Fragment getItem(int i) {
            //Fragment fragment =  ExploreItemFragment.newInstance("","");
            //Bundle args = new Bundle();
            // Our object is just an integer :-P
            //args.putInt(DemoObjectFragment.ARG_OBJECT, i + 1);
            //fragment.setArguments(args);
            return fragmentList.get(i);
        }

        @Override
        public int getCount() {
            return catsList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {

            return catsList.get(position).getTitle();
            /*
            if (isRTL()) {
                Log.d("mushy", "arabic");
                return catsList.get(catsList.size() - (position + 1)).getTitle();
            } else {
                Log.d("mushy", "english");
                return catsList.get(position).getTitle();

            }
            */
        }
    }


    // A method to find height of the status bar
    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }


    public void presentActivity(View view, Intent intent) {
        ActivityOptionsCompat options = ActivityOptionsCompat.
                makeSceneTransitionAnimation(getActivity(), view, "transition");
        int revealX = (int) (view.getX() + view.getWidth() / 2);
        int revealY = (int) (view.getY() + view.getHeight() / 2);

        intent.putExtra(LocationSelectionActivity.EXTRA_CIRCULAR_REVEAL_X, revealX);
        intent.putExtra(LocationSelectionActivity.EXTRA_CIRCULAR_REVEAL_Y, revealY);

        ActivityCompat.startActivity(getContext(), intent, options.toBundle());
    }


}
