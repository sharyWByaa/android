package com.shb.sa.shb.dialog;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.instagram.igdiskcache.OptionalStream;
import com.instagram.igdiskcache.SnapshotInputStream;
import com.marcinorlowski.fonty.Fonty;
import com.shb.sa.shb.Application;
import com.shb.sa.shb.R;
import com.shb.sa.shb.ViewerActivity;
import com.shb.sa.shb.event.UploadServiceEvent;
import com.shb.sa.shb.model.Media;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class PreviewMediaActivity extends AppCompatActivity {

    private RecyclerView list;
    private RecyclerAdapter mAdapter;

    @BindView(R.id.img_preview)
    SimpleDraweeView imagePreview;

    @BindView(R.id.post_title)
    TextView postTitle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_preview_media);
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
        //getSupportActionBar().hide();

        ButterKnife.bind(this);

        List<Media> medias = getIntent().getParcelableArrayListExtra("mediaList");


        list = (RecyclerView) findViewById(R.id.list);

        //list.setLayoutManager( new GridLayoutManager(this, 3));

        //mAdapter = new RecyclerAdapter(medias);
        //list.setAdapter(mAdapter);


        /*
        mAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                if( mAdapter.getItemCount() < MAX_NUM_VID){

                    capture_btn.setEnabled(true);
                } else {
                    capture_btn.setEnabled(false);
                }
            }
        });
        */
    }

    class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.StoryHolder>
    {

        private final List<Media> mediaList;
        private Media media;

        public RecyclerAdapter(List<Media> mediaList){

            this.mediaList = mediaList;
        }


        @Override
        public RecyclerAdapter.StoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View inflatedView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_media_item, parent, false);

            Fonty.setFonts((ViewGroup) inflatedView);
            return new RecyclerAdapter.StoryHolder(inflatedView);
        }

        @Override
        public void onBindViewHolder(RecyclerAdapter.StoryHolder holder, int position) {
            holder.bindItem(mediaList.get(position));
            media = mediaList.get(position);

        }



        @Override
        public int getItemCount() {
            return mediaList.size();
        }



        class StoryHolder extends RecyclerView.ViewHolder{

            private final ImageView screenshot;
            private View view;
            private Media mediaItem;

            public StoryHolder(View itemView) {
                super(itemView);
                screenshot = (ImageView)itemView.findViewById(R.id.media_screenshot);

                this.view = itemView;

            }


            protected void bindItem(final Media media){

                mediaItem = media;

                Observable.defer(()->{
                    OptionalStream<SnapshotInputStream> thumbnailInput = Application.getInstance(PreviewMediaActivity.this).get(media.getThumnailKey());
                    return Observable.just(thumbnailInput);
                })
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                .subscribe((OptionalStream<SnapshotInputStream> thumbnailInput) ->{

                    if (thumbnailInput.isPresent()){
                        screenshot.setImageBitmap(BitmapFactory.decodeStream(thumbnailInput.get()));
                    }
                    screenshot.setOnClickListener(v -> {
                        Intent intent = new Intent(PreviewMediaActivity.this, ViewerActivity.class);
                        intent.putExtra("media", Application.getInstance(PreviewMediaActivity.this).get(media.getMediaKey()).get().getPath());
                        startActivityForResult(intent, 111);
                    });
                });

            }

        }
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(UploadServiceEvent event) {

        switch (event.getStatus()){
            case UploadServiceEvent.RUNNING:

                if (event.getMediaList() != null && !event.getMediaList().isEmpty()){

                    postTitle.setText(event.getAd().getTitle());

                    Observable.defer(()->{
                        OptionalStream<SnapshotInputStream> thumbnailInput = Application.getInstance(PreviewMediaActivity.this).get(event.getMediaList().get(0).getThumnailKey());
                        return Observable.just(thumbnailInput);
                    })
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe((OptionalStream<SnapshotInputStream> thumbnailInput) ->{




                                if (thumbnailInput.isPresent()){

                                    imagePreview.setImageURI(Uri.fromFile(new File(thumbnailInput.get().getPath())));

                                }
//                                screenshot.setOnClickListener(v -> {
//                                    Intent intent = new Intent(PreviewMediaActivity.this, ViewerActivity.class);
//                                    intent.putExtra("media", Application.getInstance(PreviewMediaActivity.this).get(media.getMediaKey()).get().getPath());
//                                    startActivityForResult(intent, 111);
//                                });
                            });



                }
                break;
            case UploadServiceEvent.FINISHED:
                Toast.makeText(this, R.string.msg_uploading_succ, Toast.LENGTH_SHORT).show();
                finish();


                break;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }



    }
