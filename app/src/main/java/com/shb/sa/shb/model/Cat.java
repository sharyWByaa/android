package com.shb.sa.shb.model;

/**
 * Created by mujtaba on 26/06/2017.
 */

public class Cat {


    public Cat(String title, boolean selected) {
        this.title = title;
        this.selected = selected;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    private String title;


    private boolean selected;
}
