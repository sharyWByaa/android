package com.shb.sa.shb.util;

import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.TextView;

/**
 * Created by mujtaba on 02/07/2017.
 */

public class SnackBar {

    public static Snackbar make(View view, CharSequence charSequence, int lengh){
        Snackbar snack = Snackbar.make(view, charSequence, Snackbar.LENGTH_SHORT);
        ((TextView)snack.getView().findViewById(android.support.design.R.id.snackbar_text)).setTextColor(Color.parseColor("#ffffffff"));

        return snack;
    }
}
