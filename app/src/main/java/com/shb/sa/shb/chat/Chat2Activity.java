package com.shb.sa.shb.chat;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.shb.sa.shb.Application;
import com.shb.sa.shb.BaseActivity;
import com.shb.sa.shb.R;
import com.shb.sa.shb.chat.logic.ChatInteractor;
import com.shb.sa.shb.user.UserProfileActivity;

public class Chat2Activity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat2);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setTitle("");
        getSupportActionBar().setTitle("");
        SimpleDraweeView avatar = toolbar.findViewById(R.id.toolbar_avatar);

        TextView name = toolbar.findViewById(R.id.toolbar_name_text);

        avatar.setImageURI(Application.baseStoreageUrl + getIntent().getStringExtra(ChatInteractor.ARG_RECEIVER_UID) + "/profile_avatar/avatar.jpeg");

        FirebaseDatabase.getInstance().getReference().child("users").child(getIntent().getStringExtra(ChatInteractor.ARG_RECEIVER_UID)).child("username").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                name.setText(dataSnapshot.getValue().toString());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        avatar.setOnClickListener(v -> {

            Intent intent = new Intent(Chat2Activity.this, UserProfileActivity.class);
            intent.putExtra("uid", getIntent().getStringExtra(ChatInteractor.ARG_RECEIVER_UID));
            startActivity(intent);
        });


        findViewById(R.id.frame_layout);



        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.frame_layout, ChatSDKFragment.newInstance(getIntent().getStringExtra("thread")))
                .commit();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
