package com.shb.sa.shb.Registration;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.gson.JsonObject;
import com.marcinorlowski.fonty.Fonty;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;
import com.shb.sa.shb.R;
import com.shb.sa.shb.REST.ShbService;
import com.shb.sa.shb.REST.UserService;
import com.shb.sa.shb.REST.models.User;
import com.stepstone.stepper.BlockingStep;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FirebaseSignupFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FirebaseSignupFragment extends Fragment  implements BlockingStep, Validator.ValidationListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String TAG = "FirebaseSignup";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private View view;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private OnFragmentInteractionListener mListener;


    @NotEmpty
    @Password(min = 6, scheme = Password.Scheme.ANY)
    @BindView(R.id.password_editText)
    EditText password;

    @NotEmpty
    @Email
    @BindView(R.id.user_editText)
    EditText email;

    @NotEmpty
    @BindView(R.id.name_editText)
    EditText username;

    private Validator validator;
    private StepperLayout.OnCompleteClickedCallback callback;


    public FirebaseSignupFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FirebaseLoginFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FirebaseSignupFragment newInstance(String param1, String param2) {
        FirebaseSignupFragment fragment = new FirebaseSignupFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam1 = mParam1.replace("+", "");
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        validator = new Validator(this);
        validator.setValidationListener(this);

        mAuth = FirebaseAuth.getInstance();

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    //Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                } else {
                    // User is signed out
                    //Log.d(TAG, "onAuthStateChanged:signed_out");
                }
                // ...
            }
        };
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_firebase_signup, container, false);
        ButterKnife.bind(this, view);

        view.findViewById(R.id.fire_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String phoneNumber = mParam1 + "@users.shb.sa";
                String password = ((EditText) view.findViewById(R.id.password_editText)).getText().toString();
                final String username = ((EditText) view.findViewById(R.id.user_editText)).getText().toString();

                Task<AuthResult> authResultTask = mAuth.createUserWithEmailAndPassword(phoneNumber, password)
                        .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {


                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {

                                if (!task.isSuccessful()) {
                                    // SOmething went wrong
                                    return;
                                }
                                Log.d(TAG, "signInWithEmail:onComplete:" + task.isSuccessful());

                                // If sign in fails, display a message to the user. If sign in succeeds
                                // the auth state listener will be notified and logic to handle the
                                // signed in user can be handled in the listener.
                                if (!task.isSuccessful()) {
                                    Log.w(TAG, "signInWithEmail:failed", task.getException());


                                    //getActivity().finish();
                                }


                                Toast.makeText(getContext(), "",
                                        Toast.LENGTH_SHORT).show();

                                final User user = new User();
                                user.setUid(task.getResult().getUser().getUid());
                                user.setPhoneNumber(mParam1);
                                user.setUsername(username);
                                user.fireBaseUsername = phoneNumber;


                                HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
                                logging.setLevel(HttpLoggingInterceptor.Level.BODY);

                                OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
                                httpClient.addInterceptor(logging);

                                Retrofit retrofit = ShbService.getInstence().getRetrofit();

                                UserService service = retrofit.create(UserService.class);

                                Call<JsonObject> userCall = service.createUser(user);

                                Toast.makeText(getContext(), userCall.request().toString(),
                                        Toast.LENGTH_SHORT).show();
                                //this does not work
                                userCall.enqueue(new Callback<JsonObject>() {
                                                     @Override
                                                     public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                                                         Toast.makeText(getContext(), response.toString(),
                                                                 Toast.LENGTH_SHORT).show();

                                                         getActivity().finish();
                                                     }

                                                     @Override
                                                     public void onFailure(Call<JsonObject> call, Throwable t) {

                                                     }
                                                 }
                                );

                                // ...
                            }

                        });

            }

        });


        Fonty.setFonts((ViewGroup) view);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {


    }

    @Override
    public void onNextClicked(StepperLayout.OnNextClickedCallback callback) {



        final String username = ((EditText) view.findViewById(R.id.user_editText)).getText().toString();
        //mListener.onEmailSubmited(username, password, callback);
    }

    @Override
    public void onCompleteClicked(StepperLayout.OnCompleteClickedCallback callback) {

        this.callback = callback;

        validator.validate();

    }

    @Override
    public void onBackClicked(StepperLayout.OnBackClickedCallback callback) {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onValidationSucceeded() {

        String password = this.password.getText().toString().trim();
        String email = this.email.getText().toString().toLowerCase().trim();
        final String username = this.username.getText().toString().toLowerCase().trim();

        mListener.onEmailSubmited(email, password, username, callback);
    }

    @Override
    public void onValidationFailed(List<ValidationError> list) {

        for (ValidationError error : list) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getContext());

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
            }
        }
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onEmailSubmited(String email, String password,String name,  StepperLayout.OnCompleteClickedCallback callback);

    }
}
