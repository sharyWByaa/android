package com.shb.sa.shb.user.model;

import android.content.Context;

import com.airbnb.epoxy.Carousel;
import com.airbnb.epoxy.ModelView;

@ModelView(autoLayout = ModelView.Size.WRAP_WIDTH_MATCH_HEIGHT)
     public class SnappingCarousel extends Carousel {


        public SnappingCarousel(Context context) {
            super(context);


            setDefaultGlobalSnapHelperFactory(null);
        }




    }