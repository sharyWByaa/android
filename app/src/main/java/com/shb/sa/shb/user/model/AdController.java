package com.shb.sa.shb.user.model;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.airbnb.epoxy.OnModelClickListener;
import com.airbnb.epoxy.Typed2EpoxyController;
import com.shb.sa.shb.LocationSelectionActivity;
import com.shb.sa.shb.REST.models.Ad;
import com.shb.sa.shb.user.ViewStoryActivity;

import org.ocpsoft.prettytime.PrettyTime;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by mujtaba on 18/06/2017.
 */

public class AdController extends Typed2EpoxyController<List<Ad>, Boolean> {

    private final RecyclerView.RecycledViewPool recycledViewPool;
    private final Context context;
    final PrettyTime p = new PrettyTime();


    public AdController(RecyclerView.RecycledViewPool recycledViewPool, Context context){
        this.recycledViewPool = recycledViewPool;
        this.context = context;

    }
    @Override
    protected void buildModels(List<Ad> data1, Boolean isLoading) {
        //headerModel.title("he").subtitle("Hello World");


        //add(new BigAdModel_().id("header"));



            for (Ad ad : data1) {
                add(new AdItemModel_()
                        .id(ad.getId())
                        .title(ad.getTitle())
                        .subtitle(String.valueOf(ad.getPrice().doubleValue()))
                        .username(ad.getUser().getUsername())
                        .date(p.format(new Date(ad.getCreated())))
                        .screenshotPath(ad.getMedias().get(0).getThumbnail())
                        .onClickListener(new OnModelClickListener<AdItemModel_, AdItemView>() {
                            @Override
                            public void onClick(AdItemModel_ model, AdItemView parentView, View clickedView, int position) {
                                Intent intent = new Intent(context, ViewStoryActivity.class);
                                intent.putExtra("itemNo", position);
                                intent.putExtra("adsList", (ArrayList<Ad>)data1);
                                presentActivity(clickedView, intent);
                                //context.startActivity(intent);
                            }
                        })

                );
            }

        new LoadingModel_().id("loading").addIf(isLoading, this);





    }



    @Override
    protected void onExceptionSwallowed(RuntimeException exception) {
        // Best practice is to throw in debug so you are aware of any issues that Epoxy notices.
        // Otherwise Epoxy does its best to swallow these exceptions and continue gracefully
        throw exception;
    }


    public void presentActivity(View view, Intent intent) {
        ActivityOptionsCompat options = ActivityOptionsCompat.
                makeSceneTransitionAnimation((Activity) context, view, "transition");
        int revealX = (int) (view.getX() + view.getWidth() / 2);
        int revealY = (int) (view.getY() + view.getHeight() / 2);

        intent.putExtra(LocationSelectionActivity.EXTRA_CIRCULAR_REVEAL_X, revealX);
        intent.putExtra(LocationSelectionActivity.EXTRA_CIRCULAR_REVEAL_Y, revealY);

        ActivityCompat.startActivity(context, intent, options.toBundle());
    }
}
