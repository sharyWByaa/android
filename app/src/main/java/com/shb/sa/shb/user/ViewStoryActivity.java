package com.shb.sa.shb.user;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.LinearInterpolator;


import com.marcinorlowski.fonty.Fonty;
import com.mikepenz.iconics.context.IconicsContextWrapper;

import com.shb.sa.shb.ProductViewFragment;
import com.shb.sa.shb.R;
import com.shb.sa.shb.REST.ShbService;
import com.shb.sa.shb.REST.UserService;
import com.shb.sa.shb.REST.models.Ad;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewStoryActivity extends AppCompatActivity implements ProductViewFragment.StoryControl, Animator.AnimatorListener {



    public static final String EXTRA_CIRCULAR_REVEAL_X = "EXTRA_CIRCULAR_REVEAL_X";
    public static final String EXTRA_CIRCULAR_REVEAL_Y = "EXTRA_CIRCULAR_REVEAL_Y";

    View rootLayout;

    private int revealX;
    private int revealY;


    private int itemNo;
    private List<Ad> adList;
    private ViewPager mViewPager;
    private DemoCollectionPagerAdapter mDemoCollectionPagerAdapter;
    private boolean mIsInAnimation;
    private float mLastMotionX;
    private long mMotionBeginTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

        setContentView(R.layout.activity_view_story);


        // cericlar reveal

        final Intent intent = getIntent();

        rootLayout = findViewById(R.id.root_layout);

        if (savedInstanceState == null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP &&
                intent.hasExtra(EXTRA_CIRCULAR_REVEAL_X) &&
                intent.hasExtra(EXTRA_CIRCULAR_REVEAL_Y)) {
            rootLayout.setVisibility(View.INVISIBLE);

            revealX = intent.getIntExtra(EXTRA_CIRCULAR_REVEAL_X, 0);
            revealY = intent.getIntExtra(EXTRA_CIRCULAR_REVEAL_Y, 0);


            ViewTreeObserver viewTreeObserver = rootLayout.getViewTreeObserver();
            if (viewTreeObserver.isAlive()) {
                viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        revealActivity(revealX, revealY);
                        rootLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }
                });
            }
        } else {
            rootLayout.setVisibility(View.VISIBLE);
        }

        findViewById(R.id.fragment_container);

        itemNo = getIntent().getIntExtra("itemNo",0);
        adList = getIntent().getParcelableArrayListExtra("adsList");

        if (adList != null){
            initPager(adList);
        }else {
            ShbService.getInstence().getRetrofit().create(UserService.class).getAds().enqueue(new Callback<List<Ad>>() {
                @Override
                public void onResponse(Call<List<Ad>> call, Response<List<Ad>> response) {

                    adList = response.body();
                    initPager(adList);
                }

                @Override
                public void onFailure(Call<List<Ad>> call, Throwable t) {

                }
            });
        }



        //getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, ProductViewFragment.newInstance("https://s3-us-west-2.amazonaws.com/etlaq-shb/Snapchat-1734828977.mp4","")).commit();


        /*
        slideUp = new SlideUp.Builder(slideView)
                .withStartState(SlideUp.State.HIDDEN)
                .withStartGravity(Gravity.BOTTOM)
                .build();
*/





        Fonty.setFonts(this);
    }

    void initPager(List<Ad> adList){

        mDemoCollectionPagerAdapter = new DemoCollectionPagerAdapter(getSupportFragmentManager(), adList);

        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mDemoCollectionPagerAdapter);

        if (mDemoCollectionPagerAdapter.getCount() > itemNo) mViewPager.setCurrentItem(itemNo);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(IconicsContextWrapper.wrap(newBase));
    }

    @Override
    public void goToNextStory() {

        if (mIsInAnimation) return;
        ObjectAnimator anim;

        if (mViewPager != null
                &&  mDemoCollectionPagerAdapter != null
                && mViewPager.getCurrentItem() < mDemoCollectionPagerAdapter.getCount()) {
            //mViewPager.setCurrentItem(mViewPager.getCurrentItem() + 1, true);

            anim = ObjectAnimator.ofFloat(this, "motionX", 0, -mViewPager.getWidth());

            anim.setInterpolator(new LinearInterpolator());
            anim.addListener(this);
            anim.setDuration(300);
            anim.start();

        } else {
            finish();
        }
    }
    public void setMotionX(float motionX) {
        if (!mIsInAnimation) return;
        mLastMotionX = motionX;
        final long time = SystemClock.uptimeMillis();
        simulate(MotionEvent.ACTION_MOVE, mMotionBeginTime, time);
    }

    @Override
    public void onAnimationStart(Animator animator) {
        mLastMotionX = 0;
        mIsInAnimation = true;
        final long time = SystemClock.uptimeMillis();
        simulate(MotionEvent.ACTION_DOWN, time, time);
        mMotionBeginTime = time;
    }

    @Override
    public void onAnimationEnd(Animator animator) {

        mIsInAnimation = false;
        final long time = SystemClock.uptimeMillis();
        simulate(MotionEvent.ACTION_UP, mMotionBeginTime, time);
    }

    @Override
    public void onAnimationCancel(Animator animator) {

    }

    @Override
    public void onAnimationRepeat(Animator animator) {

    }

    private void simulate(int action, long startTime, long endTime) {
        // specify the property for the two touch points
        MotionEvent.PointerProperties[] properties = new MotionEvent.PointerProperties[1];
        MotionEvent.PointerProperties pp = new MotionEvent.PointerProperties();
        pp.id = 0;
        pp.toolType = MotionEvent.TOOL_TYPE_FINGER;

        properties[0] = pp;

        // specify the coordinations of the two touch points
        // NOTE: you MUST set the pressure and size value, or it doesn't work
        MotionEvent.PointerCoords[] pointerCoords = new MotionEvent.PointerCoords[1];
        MotionEvent.PointerCoords pc = new MotionEvent.PointerCoords();
        pc.x = mLastMotionX;
        pc.pressure = 1;
        pc.size = 1;
        pointerCoords[0] = pc;

        final MotionEvent ev = MotionEvent.obtain(
                startTime, endTime, action, 1, properties,
                pointerCoords, 0,  0, 1, 1, 0, 0, 0, 0);

        mViewPager.dispatchTouchEvent(ev);
    }


    private class DemoCollectionPagerAdapter extends FragmentStatePagerAdapter {

        private final List<Ad> adList;

        DemoCollectionPagerAdapter(FragmentManager fm, List<Ad> adList) {
            super(fm);
            this.adList = adList;
        }

        @Override
        public Fragment getItem(int i) {
            Fragment fragment = ProductViewFragment.newInstance(adList.get(i));
            Bundle args = new Bundle();
            // Our object is just an integer :-P
            //args.putInt(DemoObjectFragment.ARG_OBJECT, i + 1);
            //fragment.setArguments(args);
            return fragment;
        }

        @Override
        public int getCount() {
            return adList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return "";
        }
    }


    protected void revealActivity(int x, int y) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            float finalRadius = (float) (Math.max(rootLayout.getWidth(), rootLayout.getHeight()) * 1.1);

            // create the animator for this view (the start radius is zero)
            Animator circularReveal = ViewAnimationUtils.createCircularReveal(rootLayout, x, y, 0, finalRadius);
            circularReveal.setDuration(400);
            circularReveal.setInterpolator(new AccelerateInterpolator());

            // make the view visible and start the animation
            rootLayout.setVisibility(View.VISIBLE);
            circularReveal.start();
        } else {
            finish();
        }
    }



}
