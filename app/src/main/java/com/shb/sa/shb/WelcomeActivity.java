package com.shb.sa.shb;

import android.content.pm.ActivityInfo;
import android.os.Bundle;

import com.stephentuso.welcome.BasicPage;
import com.stephentuso.welcome.WelcomeConfiguration;

public class WelcomeActivity extends com.stephentuso.welcome.WelcomeActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);


        //setContentView(R.layout.activity_welcome);
        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);



    }

    @Override
    protected WelcomeConfiguration configuration() {
        return new WelcomeConfiguration.Builder(this)
                .defaultTitleTypefacePath("fonts/BlueOcean.ttf")
                .defaultHeaderTypefacePath("fonts/BlueOcean.ttf")
                .defaultDescriptionTypefacePath("fonts/JF-Flat-regular.ttf")
                .page(new BasicPage(R.drawable.ic_logo_white,
                        getResources().getString(R.string.welcome_text),
                        getResources().getString(R.string.info_text))
                        .background(R.color.colorPrimary)
                )

                .page(new BasicPage(R.drawable.ic_perm_identity_black_24dp,
                        "Simple to use",
                        "Add a welcome screen to your app with only a few lines of code.")
                        .background(R.color.colorPrimaryDark)
                )


                .page(new BasicPage(R.drawable.ic_action_camera,
                        "Customizable",
                        "All elements of the welcome screen can be customized easily.")
                        .background(R.color.colorPrimaryDark)
                )

                .swipeToDismiss(true)
                .exitAnimation(android.R.anim.fade_out)
                .build();
    }

    public static String welcomeKey() {
        return "WelcomeScreen";
    }

}
