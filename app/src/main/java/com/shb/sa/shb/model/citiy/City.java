package com.shb.sa.shb.model.citiy;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.Locale;

/**
 * Created by mujtaba on 22/01/2018.
 */

public class City implements Parcelable {

    String uid;

    @SerializedName("id")
    long saudiId;
    @SerializedName("arabic_name")
    String arabicName;
    @SerializedName("english_name")
    String englishName;

    @SerializedName("g")
    GooglePlaceInfo g;


    public long getSaudiId() {

        return saudiId;
    }

    public void setSaudiId(long saudiId) {
        this.saudiId = saudiId;
    }

    public String getArabicName() {
        return arabicName;
    }

    public void setArabicName(String arabicName) {
        this.arabicName = arabicName;
    }

    public String getEnglishName() {
        return englishName;
    }

    public void setEnglishName(String englishName) {
        this.englishName = englishName;
    }

    public String getName(){
        if (Locale.getDefault().getLanguage().trim().equals("ar")) {
            return this.arabicName;
        }else {
            return this.englishName;
        }
    }


    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public GooglePlaceInfo getG() {
        return g;
    }

    public void setG(GooglePlaceInfo g) {
        this.g = g;
    }

    public City(){}



    protected City(Parcel in) {
        uid = in.readString();
        saudiId = in.readLong();
        arabicName = in.readString();
        englishName = in.readString();
        g = (GooglePlaceInfo) in.readValue(GooglePlaceInfo.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(uid);
        dest.writeLong(saudiId);
        dest.writeString(arabicName);
        dest.writeString(englishName);
        dest.writeValue(g);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<City> CREATOR = new Parcelable.Creator<City>() {
        @Override
        public City createFromParcel(Parcel in) {
            return new City(in);
        }

        @Override
        public City[] newArray(int size) {
            return new City[size];
        }
    };
}