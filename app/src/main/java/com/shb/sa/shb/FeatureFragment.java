package com.shb.sa.shb;


import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.nytimes.android.external.store3.base.impl.Store;
import com.shb.sa.shb.REST.models.Ad;
import com.shb.sa.shb.REST.models.AdResult;
import com.shb.sa.shb.REST.models.Category;
import com.shb.sa.shb.model.citiy.City;
import com.shb.sa.shb.user.ViewStoryActivity;
import com.shb.sa.shb.user.model.BarCodeWithCursor;
import com.shb.sa.shb.user.model.FeatureAdsController;
import com.shb.sa.shb.util.SharedPrefUtil;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.disposables.Disposable;
import it.sephiroth.android.library.tooltip.Tooltip;

import static android.app.Activity.RESULT_OK;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FeatureFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FeatureFragment extends Fragment implements FeatureAdsController.Callback{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String TOOLTIPS_LOCATION = "location";

    private final RecyclerView.RecycledViewPool recycledViewPool = new RecyclerView.RecycledViewPool();
    private final FeatureAdsController controller = new FeatureAdsController(this);

    private static final String DATA_KEY = "feature_data_key";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private View view;
    @BindView(R.id.list)
    RecyclerView list;

    @BindView(R.id.swiperefresh)
    SwipeRefreshLayout swipeRefreshLayout;

    private Map<String, List<Ad>> dataMap;

    private Map<Category, AdResult> dataset = new LinkedHashMap<>();

    @BindView(R.id.location_selection_btn)
    Button locationSelection;


    private City currentCity;
    private boolean created = true;

    List<Disposable> disposableList = new ArrayList<>();

    private int modeLocation = 0;

    private int loading;

    public static final int LOADED = FeatureAdsController.LOADED;
    public static final int LOADING = FeatureAdsController.LOADING;
    public static final int LOADING_BACKGROUND = FeatureAdsController.LOADING_BACKGROUND;
    public static final int EROOR = FeatureAdsController.EROOR;
/*
    PathResolver pathResolver = new PathResolver<BarCodeWithCursor>() {
        @Nonnull
        @Override
        public String resolve(@Nonnull BarCodeWithCursor barCodeWithCursor) {
            return barCodeWithCursor.getType() + "/" + barCodeWithCursor.getKey() +"/" + barCodeWithCursor.getCity() + "/" + barCodeWithCursor.getCursor();
        }
    };

    final Store<Map<String,AdResult>, BarCodeWithCursor> articleStore = StoreBuilder.<BarCodeWithCursor, Map<String,AdResult>>key()
            .fetcher((barCode) -> {
                if (barCode.getCity() != null) {
                    return ShbService.getRetrofitRx().create(UserService.class).getAdsByListofCatAndCityRx(barCode.getType(), barCode.getCity());
                } else {
                    return ShbService.getRetrofitRx().create(UserService.class).getAdsByListofCatRx(barCode.getType());
                }
            }).persister(FileSystemPersister.create(FileSystemFactory.create(getActivity().getFilesDir()),pathResolver))
            .parser(GsonParserFactory.createSourceParser(new GsonBuilder().create(), new TypeToken<Map<String,AdResult>>(){}.getType()))
            .open();


*/

    private  Store<Map<String,AdResult>, BarCodeWithCursor> articleStore;
    private Map<String,Category> newCatList;
    private String catsAsString;

    private List<AdResult> adResults;


    public FeatureFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FeatureFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FeatureFragment newInstance(String param1, String param2) {
        FeatureFragment fragment = null;

        fragment = new FeatureFragment();

        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);

        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

            articleStore = ((Application)this.getContext().getApplicationContext()).getFeatureStore();
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);


            //initalise Cat
            String[] catid = getResources().getStringArray(R.array.cat_id);
            String[] cats = getResources().getStringArray(R.array.cat);


            newCatList = ((Application)this.getContext().getApplicationContext()).getCatMap();

            catsAsString = ((Application)this.getContext().getApplicationContext()).getCatsAsString();


        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_feature, container, false);
        (view.findViewById(R.id.coordinator)).setPadding(0, getStatusBarHeight(), 0, 0);

        ButterKnife.bind(this, view);


        ( (Toolbar) view.findViewById(R.id.main_toolbar)).showOverflowMenu();




        /*
        list.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                list.getViewTreeObserver().removeOnGlobalLayoutListener(this);

                final int appBarHeight = view.findViewById(R.id.main_appbar).getHeight();
                list.setTranslationY(-appBarHeight);
                list.getLayoutParams().height = list.getHeight() + appBarHeight;
            }
        });
*/

        return view;
    }



    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);



        GridLayoutManager mLinearLayoutManager = new GridLayoutManager(getActivity(), 2);
        controller.setSpanCount(2);
        mLinearLayoutManager.setSpanSizeLookup(controller.getSpanSizeLookup());
        list.setLayoutManager(mLinearLayoutManager);
        list.setRecycledViewPool(recycledViewPool);
        list.setHasFixedSize(true);
        list.setAdapter(controller.getAdapter());

        controller.onRestoreInstanceState(savedInstanceState);







        getData(true);

        if (currentCity != null) locationSelection.setText(currentCity.getName());
        /*

        if (savedInstanceState != null) {
            LinkedHashMap<Category, AdResult> data = (LinkedHashMap<Category, AdResult>) savedInstanceState.getSerializable(DATA_KEY);;
            if (data != null) {
                dataset = data;
                controller.setData(dataset, false);
            }

            if (data == null) {



                    //mIsLoading = true;
                getData();


            }
        } else {

               getData();



        }


*/
        TypedValue tv = new TypedValue();
        if (getActivity() != null && getActivity().getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true))
        {
            int actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, getResources().getDisplayMetrics());
            swipeRefreshLayout.setProgressViewOffset(false, actionBarHeight, (int)Math.ceil(actionBarHeight*1.8));
        }



        locationSelection.setOnClickListener((v) -> {
            Intent intent = new Intent(getActivity(), LocationSelectionActivity.class );
            presentActivity(v, intent);
        });

        swipeRefreshLayout.setOnRefreshListener(() -> {

            //articleStore.clear();
            fetch(false);


        });

        if (currentCity == null) {
            locationSelection.setText(R.string.allcity);
        }else {
            locationSelection.setText(currentCity.getName());
        }
        showLocationTooltips();
    }

    private void getData(boolean withLoading){

        if (withLoading) controller.setData(dataset, modeLocation, LOADING);
        else controller.setData(dataset, modeLocation, LOADING_BACKGROUND);
        BarCodeWithCursor barcode = new BarCodeWithCursor("feature", catsAsString, "");
        if (currentCity != null) barcode.setCity(currentCity.getG().getAddressComponents().get(0).getLongName().toLowerCase().trim());

        Log.d("mushy", barcode.toString());
        disposableList.add(
        articleStore.get(barcode)
                .subscribeOn(io.reactivex.schedulers.Schedulers.newThread())
                .observeOn(io.reactivex.android.schedulers.AndroidSchedulers.mainThread())
                .subscribe(this::showPosts, this::onError)
        );



    }

    private void fetch(boolean withLoading){

        if (withLoading) controller.setData(dataset, modeLocation, LOADING);
        else controller.setData(dataset, modeLocation, LOADING_BACKGROUND);
        BarCodeWithCursor barcode = new BarCodeWithCursor("feature", catsAsString, "");
        if (currentCity != null) barcode.setCity(currentCity.getG().getAddressComponents().get(0).getLongName().toLowerCase().trim());
        articleStore.clear(barcode);
        articleStore.clear();

        disposableList.add(
                articleStore.fetch(barcode)
                        .subscribeOn(io.reactivex.schedulers.Schedulers.newThread())
                        .observeOn(io.reactivex.android.schedulers.AndroidSchedulers.mainThread())
                        .subscribe(this::showPosts, this::onError)
        );
    }

    private void showPosts(Map<String,AdResult> adResult) {

        if (created) {
            /*
            BarCodeWithCursor barcode = new BarCodeWithCursor("feature", catsAsString.toString(), "");
            if (currentCity != null)
                barcode.setCity(currentCity.getG().getAddressComponents().get(0).getLongName().toLowerCase().trim());

            articleStore.clear(barcode);
            articleStore.clear();
            */
            fetch(false);
            created = false;
        }


        dataset = new LinkedHashMap<>();
        for (Map.Entry<String, AdResult> entry :adResult.entrySet()){

           dataset.put( newCatList.get(entry.getKey()), entry.getValue());
        }

       if (swipeRefreshLayout != null) swipeRefreshLayout.setRefreshing(false);
        //dataset.put(category, adResult);

        controller.setData(dataset, modeLocation, LOADED);

    }

    private void onError(Throwable throwable){
        if (swipeRefreshLayout != null) swipeRefreshLayout.setRefreshing(false);

        controller.setData(dataset, modeLocation, EROOR);
        Log.w("mush", throwable.getLocalizedMessage());
    }

    // A method to find height of the status bar
    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public void presentActivity(View view, Intent intent) {
        ActivityOptionsCompat options = ActivityOptionsCompat.
                makeSceneTransitionAnimation(getActivity(), view, "transition");
        int revealX = (int) (view.getX() + view.getWidth() / 2);
        int revealY = (int) (view.getY() + view.getHeight() / 2);

        intent.putExtra(LocationSelectionActivity.EXTRA_CIRCULAR_REVEAL_X, revealX);
        intent.putExtra(LocationSelectionActivity.EXTRA_CIRCULAR_REVEAL_Y, revealY);

        startActivityForResult( intent, 100 ,options.toBundle());
        //ActivityCompat.startActivityForResult(getActivity(), intent, 100 ,options.toBundle());
    }


    @Override
    public void onAdClick(View v,Category key, int pos) {


        Intent intent = new Intent(getActivity(), ViewStoryActivity.class);
        intent.putExtra("itemNo", pos);
        intent.putExtra("adsList", (ArrayList<Ad>)dataset.get(key).getResults());

        presentActivity(v, intent);
    }

    @Override
    public void onSectionClicked(Category category) {
        Intent intent = new Intent(getActivity(),AdBrowserActivity.class);
        intent.putExtra("cat", (Parcelable) category);
        if (currentCity != null)  intent.putExtra("city",currentCity);
        startActivity(intent);
    }

    @Override
    public void expandSearch() {
        locationSelection.setText(R.string.allcity);
        currentCity = null;
        modeLocation = 0;
        getData(false);
    }


    @Override
    public void onSaveInstanceState(Bundle state) {
        super.onSaveInstanceState(state);


        controller.onSaveInstanceState(state);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        for (Disposable disposable: disposableList){
            if (disposable != null) disposable.dispose();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK){
            //Toast.makeText(getActivity(), "onResult", Toast.LENGTH_SHORT).show();
            City city = data.getParcelableExtra("city");
            if (city != null) locationSelection.setText(city.getName());
            else locationSelection.setText(R.string.allcity);
            currentCity = city;

            if (city == null) modeLocation = 0;
            else modeLocation = 1;

            dataset = new LinkedHashMap<>();

            fetch(true);

        }
    }


    private void showLocationTooltips() {

        if (getActivity() != null && !new SharedPrefUtil(getActivity()).readSharedPref(TOOLTIPS_LOCATION)) {
            Tooltip.make(getActivity(),
                    new Tooltip.Builder(TOOLTIPS_LOCATION.hashCode())
                            .anchor(view.findViewById(R.id.main_toolbar), Tooltip.Gravity.BOTTOM)
                            .closePolicy(new Tooltip.ClosePolicy()
                                    .insidePolicy(true, false)
                                    .outsidePolicy(true, false), 5000)
                            .activateDelay(800)
                            .showDelay(300)
                            .text(getResources(), R.string.tip_click_to_change_city)
                            .maxWidth(500)
                            .withArrow(true)
                            .withOverlay(true)
                            .floatingAnimation(Tooltip.AnimationBuilder.DEFAULT)
                            .build()
            ).show();

            new SharedPrefUtil(getActivity()).writeSharedPref(TOOLTIPS_LOCATION, true);
        }
    }

}
