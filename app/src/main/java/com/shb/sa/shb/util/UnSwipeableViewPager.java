package com.shb.sa.shb.util;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class UnSwipeableViewPager extends com.duolingo.open.rtlviewpager.RtlViewPager {
    private boolean pagingEnabled = true;

    public UnSwipeableViewPager(Context context) {
        super(context);
    }

    public UnSwipeableViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setPagingEnabled(boolean pagingEnabled) {
        this.pagingEnabled = pagingEnabled;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        return pagingEnabled && super.onInterceptTouchEvent(event);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return pagingEnabled && super.onTouchEvent(event);
    }

}