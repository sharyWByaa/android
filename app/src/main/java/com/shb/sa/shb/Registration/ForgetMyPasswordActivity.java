package com.shb.sa.shb.Registration;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Patterns;
import android.widget.EditText;

import com.google.firebase.auth.FirebaseAuth;
import com.shb.sa.shb.R;

import br.com.simplepass.loading_button_lib.customViews.CircularProgressButton;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ForgetMyPasswordActivity extends AppCompatActivity {


    @BindView(R.id.email_editText)
    EditText emailText;

    @BindView(R.id.submit_btn)
    CircularProgressButton submitBtn;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_my_password);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        submitBtn.setOnClickListener(view -> {


            if (!Patterns.EMAIL_ADDRESS.matcher(emailText.getText().toString().trim()).matches()) {
                emailText.setError("please input an email in the correct format");
                return;
            }

            submitBtn.startAnimation();

            FirebaseAuth.getInstance().sendPasswordResetEmail(emailText.getText().toString().trim().toLowerCase())
                .addOnCompleteListener(task -> {

                    if (task.isSuccessful()){
                        submitBtn.revertAnimation();
                        new AlertDialog.Builder(ForgetMyPasswordActivity.this).setMessage("Email has been sent").setPositiveButton("okay", (dialogInterface, i) -> {

                            finish();
                        }).create().show();
                    }else {
                        emailText.setError("Email is not registered");
                        submitBtn.revertAnimation();
                    }
                });
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        submitBtn.dispose();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;

    }
}
