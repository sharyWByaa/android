package com.shb.sa.shb.user.model;

import android.net.Uri;
import android.view.View;

import com.airbnb.epoxy.OnModelClickListener;
import com.airbnb.epoxy.Typed2EpoxyController;
import com.google.firebase.auth.FirebaseAuth;
import com.shb.sa.shb.Application;
import com.shb.sa.shb.R;
import com.shb.sa.shb.REST.models.Ad;
import com.shb.sa.shb.REST.models.User;

import java.util.List;

/**
 * Created by mujtaba on 20/06/2017.
 */

public class ProfileController extends Typed2EpoxyController<List<Ad>, User> {
    //@AutoModel ProfileHeaderModel_ profileHeaderModel_;

    private final ProfileCallback profileCallback;

    public ProfileController(ProfileCallback profileCallback){
        this.profileCallback = profileCallback;
    }

    @Override
    protected void buildModels(List<Ad> data1, User data2) {

        //profileHeaderModel_.addTo(this);

        if (data2 != null) {
            add(new ProfileHeaderModel_()
                    .id("header")
                    .username(data2.getUsername())
                    .aboutme(data2.getAbout())
            .imageUri(Uri.parse(Application.baseStoreageUrl + data2.getUid() +"/profile_avatar/avatar.jpeg")));
        } else {
            add(new ProfileHeaderModel_().id("header"));
        }


        if (data2 != null && FirebaseAuth.getInstance().getCurrentUser() != null) {
            if (!FirebaseAuth.getInstance().getCurrentUser().getUid().equals(data2.getUid())) {
                new ButtonModel_().id("button").button_title(R.string.app_contact_user)
                        .clickListener(v -> profileCallback.callUser()).addIf(data2.isShowPhoneNo(), this);

                ButtonModel_ contact = new ButtonModel_().id("btn").button_title(R.string.app_message_user)
                        .clickListener(v -> profileCallback.messageUser());
                if (!data2.isShowPhoneNo()) contact.spanSizeOverride((totalSpanCount, position, itemCount) -> totalSpanCount);

                add(contact);
            }
        }


        if (data1 != null) {

            for (int i = 0; i < data1.size(); i++) {
                int finalI = i;
                add(new AdItemModel_()
                        .id(data1.get(i).getId())
                        .title(data1.get(i).getTitle())
                        .subtitle(String.valueOf(data1.get(i).getPrice().doubleValue()))
                        .screenshotPath(data1.get(i).getMedias().get(0).getThumbnail()).onClickListener(new OnModelClickListener<AdItemModel_, AdItemView>() {
                            @Override
                            public void onClick(AdItemModel_ model, AdItemView parentView, View clickedView, int position) {
                                int adPos = finalI;
                                profileCallback.onAdClick(adPos);
                            }
                        })

                );
            }
        }


    }

    @Override
    protected void onExceptionSwallowed(RuntimeException exception) {
        // Best practice is to throw in debug so you are aware of any issues that Epoxy notices.
        // Otherwise Epoxy does its best to swallow these exceptions and continue gracefully
        throw exception;
    }

    public interface ProfileCallback{
        public void callUser();
        public void messageUser();
        public void onAdClick(int pos);
    }
}
