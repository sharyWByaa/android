package com.shb.sa.shb.user;


import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.marcinorlowski.fonty.Fonty;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.iconics.IconicsDrawable;

import java.util.ArrayList;
import java.util.List;

import com.shb.sa.shb.BaseFragment;
import com.shb.sa.shb.R;
import com.shb.sa.shb.Registration.LoginActivity;
import com.shb.sa.shb.model.ProfileMenu;
import com.zendesk.sdk.model.access.AnonymousIdentity;
import com.zendesk.sdk.model.access.Identity;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import com.zendesk.sdk.support.SupportActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.chatsdk.core.session.NM;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProfileFragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private View view;
    private RecyclerView listview;
    private LinearLayoutManager mLinearLayoutManager;
    private RecyclerAdapter mAdapter;

    @BindView(R.id.profile_fragment_username_text) TextView usernameText;
    @BindView(R.id.profile_fragment_avatar)
    SimpleDraweeView avatar;

    public ProfileFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ProfileFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ProfileFragment newInstance(String param1, String param2) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_profile, container, false);

        view.findViewById(R.id.edit_profile_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                //startActivity(new Intent(getActivity(), ViewProfileActivity.class));

                Intent intent = new Intent(getActivity(), UserProfileActivity.class);
                intent.putExtra("uid",mAuth.getCurrentUser().getUid());
                startActivity(intent);
            }
        });

        listview = (RecyclerView) view.findViewById(R.id.profile_list_view);
        mLinearLayoutManager = new LinearLayoutManager(getActivity());
        listview.setLayoutManager(mLinearLayoutManager);
        List<ProfileMenu> profileMenus = new ArrayList<>();

        profileMenus = getMenu(profileMenus);
        mAdapter = new RecyclerAdapter(profileMenus.toArray(new ProfileMenu[profileMenus.size()]));
        listview.setAdapter(mAdapter);

        Fonty.setFonts((ViewGroup) view);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (mAuth.getCurrentUser() != null && mAuth.getCurrentUser().getDisplayName() != null) usernameText.setText(mAuth.getCurrentUser().getDisplayName());
        UserUtil.getUserAvatar().getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                avatar.setImageURI(uri);
            }
        });


    }

    private List<ProfileMenu> getMenu(List<ProfileMenu> profileMenuList){

        ProfileMenu profileMenu1 =  new ProfileMenu();
        profileMenu1.setTitle(getResources().getString(R.string.my_ads_profile));
        profileMenu1.setDrawable(new IconicsDrawable(getActivity()).icon(FontAwesome.Icon.faw_list).color(Color.GRAY).sizeDp(24));
        profileMenu1.setListener(new ProfileMenu.MyListener() {
            @Override
            public void callback() {
                startActivity(new Intent(getActivity(), MyAdActivity.class));
            }
        });
        profileMenuList.add(profileMenu1);

        ProfileMenu menu2 = new ProfileMenu();
        menu2.setTitle(getResources().getString(R.string.feedback_profile));
        menu2.setDrawable(new IconicsDrawable(getActivity()).icon(FontAwesome.Icon.faw_commenting_o).color(Color.GRAY).sizeDp(24));

        profileMenuList.add(menu2);

        ProfileMenu menu3 = new ProfileMenu();
        menu3.setTitle(getResources().getString(R.string.contact_us_profile));
        menu3.setDrawable(new IconicsDrawable(getActivity()).icon(FontAwesome.Icon.faw_envelope_o).color(Color.GRAY).sizeDp(24));
        menu3.setListener(new ProfileMenu.MyListener() {
            @Override
            public void callback() {
                ZendeskConfig.INSTANCE.init(getContext(), "https://etlaq.zendesk.com", "75f26ddecdfcb7d6341cd229d85147e61fb43008e6bd9ef0", "mobile_sdk_client_7dbb0179b80c39be54cf");
                Identity identity = new AnonymousIdentity.Builder().build();
                ZendeskConfig.INSTANCE.setIdentity(identity);
                new SupportActivity.Builder().show(getActivity());
            }
        });
        profileMenuList.add(menu3);


        ProfileMenu menu4 = new ProfileMenu();
        menu4.setTitle(getResources().getString(R.string.logout_profile));
        menu4.setDrawable(new IconicsDrawable(getActivity()).icon(FontAwesome.Icon.faw_sign_out).color(Color.GRAY).sizeDp(24));
        menu4.setListener(() -> {

            FirebaseAuth.getInstance().signOut();




            startActivity(
            // Get an instance of AuthUI based on the default app
                    new Intent(getContext(), LoginActivity.class)
            );

            getActivity().finish();
        });
        profileMenuList.add(menu4);

        return profileMenuList;
    }

    public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.PhotoHolder> {

        private final ProfileMenu[] menuItem;

        public RecyclerAdapter (ProfileMenu[] strings){
            this.menuItem = strings;
        }
        @Override
        public PhotoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View inflatedView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_layout_profile, parent, false);
            Fonty.setFonts(getActivity());
            return new PhotoHolder(inflatedView);
        }

        @Override
        public void onBindViewHolder(PhotoHolder holder, int position) {
            holder.bindItem(menuItem[position]);
        }



        @Override
        public int getItemCount() {
            return menuItem.length;
        }

        class PhotoHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

            private final ImageView iconImageView;
            private TextView titleTextview;
            private ProfileMenu item;


            public PhotoHolder(View v) {
                super(v);

                titleTextview = (TextView) v.findViewById(R.id.menu_item_text);
                iconImageView = (ImageView) v.findViewById(R.id.menu_icon_profile);

                Fonty.setFonts((ViewGroup) titleTextview.getRootView());

                v.setOnClickListener(this);
            }

            public void bindItem(ProfileMenu menuItem){

                this.item = menuItem;
                titleTextview.setText(menuItem.getTitle());
                iconImageView.setImageDrawable(menuItem.getDrawable());
            }

            //5
            @Override
            public void onClick(View v) {
                Log.d("RecyclerView", "CLICK!");

                if(item.getListener() != null) item.getListener().callback();
            }
        }

    }

}
