package com.shb.sa.shb.user.model;

import android.support.annotation.StringRes;
import android.view.View;

import com.airbnb.epoxy.DataBindingEpoxyModel;
import com.airbnb.epoxy.EpoxyAttribute;
import com.airbnb.epoxy.EpoxyModelClass;
import com.shb.sa.shb.R;

/**
 * Created by mujtaba on 20/06/2017.
 */
@EpoxyModelClass(layout = R.layout.model_text)
public abstract class TextModel extends DataBindingEpoxyModel {
    @EpoxyAttribute @StringRes
    int title;
    @EpoxyAttribute
    int title_alignment = View.TEXT_ALIGNMENT_TEXT_START;

    @Override
    public int getSpanSize(int totalSpanCount, int position, int itemCount) {
        return totalSpanCount;
    }
}
