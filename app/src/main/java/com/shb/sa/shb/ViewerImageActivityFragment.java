package com.shb.sa.shb;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.shb.sa.shb.model.Media;

/**
 * A placeholder fragment containing a simple view.
 */
public class ViewerImageActivityFragment extends Fragment {

    private View view;
    private SimpleExoPlayerView simpleExoPlayerView;

    private Media media;


    private static final String ARG_PARAM1 = "media";
    private static final String ARG_PARAM2 = "param2";
    private SimpleDraweeView image;

    public ViewerImageActivityFragment() {
    }

    public static ViewerImageActivityFragment newInstance(Media media) {
        ViewerImageActivityFragment fragment = new ViewerImageActivityFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM1, media);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            media =  getArguments().getParcelable(ARG_PARAM1);

        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_image_viewer, container, false);


        image = view.findViewById(R.id.image_view);

        if (media != null) image.setImageURI(media.getThumnailKeyUri());
        return view;
    }



}
