package com.shb.sa.shb;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.anupcowkur.reservoir.Reservoir;
import com.anupcowkur.reservoir.ReservoirGetCallback;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.facebook.common.internal.ByteStreams;
import com.facebook.shimmer.ShimmerFrameLayout;

import com.google.gson.reflect.TypeToken;
import com.instagram.igdiskcache.EditorOutputStream;
import com.instagram.igdiskcache.IgDiskCache;
import com.instagram.igdiskcache.OptionalStream;

import com.otaliastudios.cameraview.CameraListener;
import com.otaliastudios.cameraview.CameraOptions;
import com.otaliastudios.cameraview.CameraUtils;
import com.otaliastudios.cameraview.CameraView;
import com.otaliastudios.cameraview.Facing;
import com.otaliastudios.cameraview.Gesture;
import com.otaliastudios.cameraview.GestureAction;
import com.otaliastudios.cameraview.SessionType;
import com.shb.sa.shb.dialog.PreviewMediaActivity;
import com.shb.sa.shb.event.MediaEditingMessage;
import com.shb.sa.shb.event.UploadServiceEvent;
import com.shb.sa.shb.model.Media;
import com.shb.sa.shb.user.AdInfoActivity;
import com.shb.sa.shb.util.SharedPrefUtil;
import com.shb.sa.shb.util.VideoUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import it.sephiroth.android.library.tooltip.Tooltip;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment  implement the

 * to handle interaction events.
 * Use the {@link CameraViewFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CameraViewFragment extends Fragment implements MainActivity.CameraControl, MediaController.Callback {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    public static final String CACHE_MEDIA = "cachemedia";
    public static final String CACHE_VIDEO_PICTURES = "cacheallmedia";
    public static final String TOOLTIPS_CAMERA = "tooltips_camera_btn";
    private static final int SPAN_COUNT = 1;
    private static final String TOOLTIPS_UPLOAD = "tooltips_upload_btn";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private CameraView cameraView;
    private VideoCaptureTask videoTask;
    private Button capture_btn;
    private ProgressBar progressbar;

    private final static int MAX_NUM_VID = 3;
    private List<Media> mediaList = new ArrayList<>();
    private List<Media> mediaArrayListList = new ArrayList<>();
    private List<Media> pictureArrayListList = new ArrayList<>();
    private HashSet<Media> mediaHashSet = new LinkedHashSet<>();

    @BindView(R.id.video_limit_indicator)
    ProgressBar limitIndicator;

    private ProgressDialog progrssDialog;
    private Disposable sub;

    @BindView(R.id.flip_camera_btn)
    TextView flipCamera;

    @BindView(R.id.flash_btn)
    TextView flash;

    @BindView(R.id.list)
    RecyclerView list;

    @BindView(R.id.shimmer_view_container)
    ShimmerFrameLayout uploadingContainer;

    private Disposable retriveMedia;
    private ScreenLock mListener;

    private boolean isWaitingForResult = false;

    private final RecyclerView.RecycledViewPool recycledViewPool = new RecyclerView.RecycledViewPool();
    private MediaRecyclerViewAdapter adapter;
    //private final MediaController controller = new MediaController(this);

    public CameraViewFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CameraViewFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CameraViewFragment newInstance(String param1, String param2) {
        CameraViewFragment fragment = new CameraViewFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        mediaList = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_camera_view, container, false);

        ButterKnife.bind(this,view);
        ((HomePageActivity)getActivity()).setSupportActionBar((Toolbar) view.findViewById(R.id.my_toolbar));
        cameraView = (CameraView) view.findViewById(R.id.camera);

        cameraView.setSessionType(SessionType.VIDEO);

        cameraView.mapGesture(Gesture.PINCH, GestureAction.ZOOM); // Pinch to zoom!
        cameraView.mapGesture(Gesture.TAP, GestureAction.FOCUS_WITH_MARKER); // Tap to focus!

        progressbar = (ProgressBar) view.findViewById(R.id.progress_bar);


        progrssDialog = new ProgressDialog(getActivity());





        cameraView.addCameraListener(new CameraListener() {

            @Override
            public void onCameraOpened(CameraOptions options) {
                super.onCameraOpened(options);
                cameraHasOpen();
            }

            @Override
            public void onPictureTaken(byte[] jpeg) {
                super.onPictureTaken(jpeg);

                CameraUtils.decodeBitmap(jpeg, 720, 1280, bitmap -> {

                    String mediaKey = UUID.randomUUID().toString().replaceAll("-", "");

                    Observable.defer(()->{

                        IgDiskCache igCahce = Application.getInstance(getContext());
                        OptionalStream<EditorOutputStream> imageOutput = igCahce.edit(mediaKey);

                        Bitmap img = bitmap;


                        img.compress(Bitmap.CompressFormat.JPEG, 60, imageOutput.get());

                        imageOutput.get().commit();


                        Media media = new Media(Media.TYPE_IMAGE, 0L, mediaKey, mediaKey);
                        media.setThumnailKeyUri(Uri.fromFile(Application.getInstance(getContext()).getDirectory()).buildUpon().appendEncodedPath(mediaKey+".clean").build());


                        return Observable.just(media);
                    })
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                    .subscribe((media -> {
                        Intent intent = new Intent(getActivity(), ViewerActivity.class);
                        intent.putExtra("media", "");
                        intent.putExtra("mediaObj", media);
                        intent.putExtra("type", Media.TYPE_IMAGE);
                        startActivityForResult(intent, 112);
                    }));


                    /*

                    Observable.defer(()->
                    {
                        IgDiskCache igCahce = Application.getInstance(getContext());
                        OptionalStream<EditorOutputStream> imageOutput = igCahce.edit(mediaKey);

                        Bitmap img = bitmap;


                        img.compress(Bitmap.CompressFormat.JPEG, 60, imageOutput.get());

                        imageOutput.get().commit();

                        Media media = new Media(Media.TYPE_IMAGE, 0L, mediaKey, mediaKey);
                        media.setThumnailKeyUri(Uri.fromFile(Application.getInstance(getContext()).getDirectory()).buildUpon().appendEncodedPath(mediaKey+".clean").build());

                        mediaArrayListList.add(media);
                        pictureArrayListList.add(media);
                        cacheMediaList();
                        return Observable.just(media);
                    })
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe((media)-> {


                                //adapter = new MediaRecyclerViewAdapter(getContext(),mediaArrayListList);
                                //list.setAdapter(adapter);
                                adapter.notifyDataSetChanged();
                            });
                            */

                });


                //Bitmap img = BitmapFactory.decodeByteArray(jpeg, 0, jpeg.length);

                //MediaStore.Images.Media.insertImage(getActivity().getContentResolver(), img, "" , "");

                //Media imgMedia = new Media(Media.TYPE_IMAGE, null, 0L, jpeg);
                //if(mediaList.size() < MAX_NUM_VID){

                 //   mediaList.add(imgMedia);

               // }
            }



            @Override
            public void onVideoTaken(final File video) {
                super.onVideoTaken(video);

                long duration = new VideoUtil(video.getPath()).getDuration();

                if (duration < 1000L) return; // if the video is too short ignore it
                Log.d("mushy", "dur: " + duration);
                Intent intent = new Intent(getActivity(), ViewerActivity.class);
                intent.putExtra("media", video.getAbsolutePath());
                intent.putExtra("type", Media.TYPE_VIDEO);
                startActivityForResult(intent, 111);
                isWaitingForResult = true;

            }
        });

        capture_btn = ((Button)view.findViewById(R.id.capture_button));
        capture_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                cameraView.capturePicture();
            }
        });



        capture_btn.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (mediaList.size() >= 3) return true; // max video Number
                cameraView.setSessionType(SessionType.VIDEO);
                cameraView.setSessionType(SessionType.VIDEO);
                videoTask = new VideoCaptureTask();
                if(videoTask.getStatus() == AsyncTask.Status.PENDING) {
                    videoTask.execute(cameraView);

                }
                return true;
            }
        });

        capture_btn.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()){
                    case MotionEvent.ACTION_DOWN:


                        break;
                    case MotionEvent.ACTION_UP:


                        if(videoTask != null && videoTask.getStatus() == AsyncTask.Status.RUNNING) {
                            videoTask.cancel(true);
                        }


                        //cameraView.stopRecordingVideo();
                        //Toast.makeText(getActivity(),"Cancel", Toast.LENGTH_LONG).show();
                        break;
                }
                return false;
            }
        });


        view.findViewById(R.id.view_media).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent = new Intent(getActivity(), PreviewMediaActivity.class);
                intent.putParcelableArrayListExtra("mediaList", (ArrayList<Media>) mediaList);
                startActivity(intent);
            }
        });




        view.findViewById(R.id.btn_upload).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mediaArrayListList == null || mediaArrayListList.isEmpty()) {
                    Toast.makeText(getContext(), R.string.err_no_video_item, Toast.LENGTH_SHORT).show();
                    return;
                }
                Intent intent = new Intent(getActivity(), AdInfoActivity.class);
                //intent.putParcelableArrayListExtra("media", (ArrayList<Media>) mediaList);
                //intent.putParcelableArrayListExtra("picture", (ArrayList<Media>) pictureArrayListList);
                intent.putParcelableArrayListExtra("allMedia", (ArrayList<Media>) mediaArrayListList);
                startActivityForResult(intent, 100);

                /*

                new AsyncTask<String, String, List<InputStream>>() {

                    @Override
                    protected void onPreExecute() {
                        super.onPreExecute();
                        progrssDialog.show();
                    }

                    @Override
                    protected List<InputStream> doInBackground(String... params) {


                        List<InputStream> inputStreams = new ArrayList<InputStream>();
                        for(Media media: mediaList) {
                            SnapshotInputStream item = Application.getInstance(getContext()).get(media.getMediaKey()).get();
                            inputStreams.add(item);
                        }

                        return inputStreams;
                    }

                    @Override
                    protected void onPostExecute(List<InputStream> s) {
                        super.onPostExecute(s);


                        FirebaseStorage storage = FirebaseStorage.getInstance();

                        // Create a storage reference from our app
                        StorageReference storageRef = storage.getReference();


                        for (InputStream inputStream: s){
                            String uuid = UUID.randomUUID().toString().replaceAll("-", "");
                            StorageReference mountainsRef = storageRef.child(uuid + ".mp4");
                            mountainsRef.putStream(inputStream);

                        }



                    }
                }.execute("");


*/
            }
        });

        /*
         list = (RecyclerView) view.findViewById(R.id.list);

        LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(getActivity());
        list.setLayoutManager(mLinearLayoutManager);

        mAdapter = new RecyclerAdapter(mediaList);
        list.setAdapter(mAdapter);

        mAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
               if( mAdapter.getItemCount() < MAX_NUM_VID){

                   capture_btn.setEnabled(true);
               } else {
                   capture_btn.setEnabled(false);
               }
            }
        });
        */

        adapter = new MediaRecyclerViewAdapter(getContext(), mediaArrayListList);

        flipCamera.setOnClickListener(view1 -> cameraView.toggleFacing());

        flash.setOnClickListener( view1 -> cameraView.toggleFlash());

        list.setRecycledViewPool(recycledViewPool);
        list.setHasFixedSize(true);

        LinearLayoutManager LinearLayoutManager = new LinearLayoutManager(getContext(), android.support.v7.widget.LinearLayoutManager.HORIZONTAL, false);
        list.setLayoutManager(LinearLayoutManager);

        list.setAdapter(adapter);


        return view;
    }

    private void cameraHasOpen() {
        showTooltip();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        uploadingContainer.startShimmerAnimation();






    }

    private void increaseProgress(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            limitIndicator.setProgress(limitIndicator.getProgress() + 1,true);
        } else {
            limitIndicator.setProgress(limitIndicator.getProgress() + 1);
        }
    }



    @Override
    public void onResume() {
        super.onResume();

        if (isVisible()  && cameraView != null && getUserVisibleHint()) {
            cameraView.start();
            // show tour guide


        }

        Log.d("mushy", "onResume");
        restoreMediaList();




        //cameraView.start();
    }

    private void showTooltip() {

        if (getActivity() != null && !new SharedPrefUtil(getActivity()).readSharedPref(TOOLTIPS_CAMERA)) {
            Tooltip.make(getActivity(),
                    new Tooltip.Builder(101)
                            .anchor(capture_btn, Tooltip.Gravity.TOP)
                            .closePolicy(new Tooltip.ClosePolicy()
                                    .insidePolicy(true, false)
                                    .outsidePolicy(true, false), 5000)
                            .activateDelay(800)
                            .showDelay(300)
                            .text(getResources(),R.string.tip_recored_video)
                            .maxWidth(500)
                            .withArrow(true)
                            .withOverlay(true)
                            .floatingAnimation(Tooltip.AnimationBuilder.DEFAULT)
                            .build()
            ).show();
            new SharedPrefUtil(getActivity()).writeSharedPref(TOOLTIPS_CAMERA, true);
        }
    }

    private void showTooltipsForUploading(){

        if (getActivity() != null
                && getView() != null
                && !new SharedPrefUtil(getActivity()).readSharedPref(TOOLTIPS_UPLOAD)) {
            Tooltip.make(getActivity(),
                    new Tooltip.Builder(101)
                            .anchor(getView().findViewById(R.id.btn_upload), Tooltip.Gravity.TOP)
                            .closePolicy(new Tooltip.ClosePolicy()
                                    .insidePolicy(true, false)
                                    .outsidePolicy(true, false), 5000)
                            .activateDelay(800)
                            .showDelay(300)
                            .text(getResources(), R.string.tip_upload_post)
                            .maxWidth(500)
                            .withArrow(true)
                            .withOverlay(true)
                            .floatingAnimation(Tooltip.AnimationBuilder.DEFAULT)
                            .build()
            ).show();
            new SharedPrefUtil(getActivity()).writeSharedPref(TOOLTIPS_UPLOAD, true);
        }

    }
    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        cameraView.stop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        cameraView.destroy();
        if (retriveMedia != null )retriveMedia.dispose();
        cameraView.destroy();
        if (sub != null && !sub.isDisposed()) {
            sub.dispose();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MediaEditingMessage event) {
        //
        mediaList = event.mediaList;
        limitIndicator.setProgress(mediaList.size());


    }

    @Override
    public Facing switchCamera() {
        return cameraView.toggleFacing();
    }

    @Override
    public void addMedia(String media) {
        if(mediaList.size() < MAX_NUM_VID){

            //mediaList.add(media);

            prepareMedia(media);


        }
    }


    private void prepareMedia(final String media){

        sub =  Observable.defer(() -> {
            String mediaKey = UUID.randomUUID().toString().replaceAll("-", "");
            String thumbnailKey = UUID.randomUUID().toString().replaceAll("-", "");
            IgDiskCache igCahce = Application.getInstance(getContext());
            OptionalStream<EditorOutputStream> outputStream = igCahce.edit(mediaKey);

            if (outputStream.isPresent()) {
                Map<String, String> map = new HashMap<>();
                map.put("media", mediaKey);
                map.put("thumbnail", thumbnailKey);
                map.put("duration", String.valueOf(new VideoUtil(media).getDuration()));
                try {
                    outputStream.get().write(ByteStreams.toByteArray(new FileInputStream(new File(media))));
                    outputStream.get().commit();

                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    isWaitingForResult = false;
                    outputStream.get().abortUnlessCommitted();
                }

            OptionalStream<EditorOutputStream> outputStreamThumnail = igCahce.edit(thumbnailKey);



                if (outputStreamThumnail.isPresent()) {
                    new VideoUtil(media).getVideoFrame().compress(Bitmap.CompressFormat.PNG, 100, outputStreamThumnail.get());
                    outputStreamThumnail.get().commit();
                }

                Media m1 = new Media(Media.TYPE_VIDEO, Long.valueOf(map.get("duration")),map.get("media") , map.get("thumbnail"));
                m1.setThumnailKeyUri(Uri.fromFile(Application.getInstance(getContext()).getDirectory()).buildUpon().appendEncodedPath(map.get("thumbnail") + ".clean").build());
                mediaList.add(m1);
                mediaArrayListList.add(m1);
                mediaHashSet.add(m1);

                cacheMediaList();
                isWaitingForResult = false;



                return Observable.just(map);

            } else {

                isWaitingForResult = false;
                return null;
            }

        })

                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::finishPrepare);


/*        final AsyncTask<String, String, Map<String,String>> task = new AsyncTask<String, String, Map<String, String>>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progrssDialog.show();
            }

            @Override
            protected Map<String, String> doInBackground(String... params) {
                String mediaKey = UUID.randomUUID().toString().replaceAll("-", "");
                String thumbnailKey = UUID.randomUUID().toString().replaceAll("-", "");
                IgDiskCache igCahce = Application.getInstance(getContext());
                OptionalStream<EditorOutputStream> outputStream = igCahce.edit(mediaKey);

                Map<String, String> map = new HashMap<>();
                map.put("media", mediaKey);
                map.put("thumbnail", thumbnailKey);
                map.put("duration", String.valueOf(new VideoUtil(params[0]).getDuration()));
                try {
                    outputStream.get().write(org.apache.commons.io.IOUtils.toByteArray(new FileInputStream(new File(params[0]))));
                    outputStream.get().commit();

                } catch (IOException e) {
                    e.printStackTrace();
                }

                OptionalStream<EditorOutputStream> outputStreamThumnail = igCahce.edit(thumbnailKey);

                new VideoUtil(params[0]).getVideoFrame().compress(Bitmap.CompressFormat.PNG, 100,outputStreamThumnail.get());
                outputStreamThumnail.get().commit();


                return map;
            }

            @Override
            protected void onPostExecute(Map<String, String> s) {
                super.onPostExecute(s);
                progrssDialog.hide();
                Media media = new Media(Media.TYPE_VIDEO, Long.valueOf(s.get("duration")),s.get("media") , s.get("thumbnail"));
                mediaList.add(media);

                increaseProgress();
                //cacheMediaList();
            }
        };

        task.execute(media);*/

    }

    private void finishPrepare(Map<String,String> s){
        //Media media = new Media(Media.TYPE_VIDEO, Long.valueOf(s.get("duration")),s.get("media") , s.get("thumbnail"));
        //media.setThumnailKeyUri(Uri.fromFile(Application.getInstance(getContext()).getDirectory()).buildUpon().appendEncodedPath(s.get("thumbnail") + ".clean").build());
        //mediaList.add(media);
        //mediaArrayListList.add(media);

        //adapter = new MediaRecyclerViewAdapter(getContext(), mediaArrayListList);
        //list.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        increaseProgress();
        restoreMediaList();
    }


    private void restoreMediaList(){

        Type resultType = new TypeToken<List<Media>>() {}.getType();
        Reservoir.getAsync(CACHE_MEDIA, resultType, new ReservoirGetCallback<List<Media>>() {
            @Override
            public void onSuccess(List<Media> myObject) {
                //success

                mediaList = myObject;
                limitIndicator.setProgress(mediaList.size());

                /*
               retriveMedia = Observable.defer(()->{

                   Iterator<Media> iter = mediaList.iterator();
                   while (iter.hasNext()) {
                       Media item = iter.next();

                       if (!Application.getInstance(getContext()).get(item.getMediaKey()).isPresent())
                           iter.remove();
                   }


                    return Observable.just(mediaList);
                })
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                .subscribe(medias -> {
                    mediaList = medias;
                    limitIndicator.setProgress(mediaList.size());
                }, Throwable::printStackTrace);


*/

            }

            @Override
            public void onFailure(Exception e) {
                //error
                if (mediaList != null && limitIndicator != null) limitIndicator.setProgress(mediaList.size());
            }
        });





        Reservoir.getAsync(CACHE_VIDEO_PICTURES, resultType, new ReservoirGetCallback<List<Media>>() {
            @Override
            public void onSuccess(List<Media> media) {

                //if (mediaArrayListList != null && !mediaArrayListList.isEmpty()) return; // don't restore

                mediaArrayListList = media;
                adapter = new MediaRecyclerViewAdapter(getContext(), mediaArrayListList);
                list.setAdapter(adapter);
                adapter.notifyDataSetChanged();

            }

            @Override
            public void onFailure(Exception e) {

            }
        });



    }
    private void cacheMediaList(){
        try {
            Reservoir.put(CACHE_MEDIA, mediaList);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            Reservoir.put(CACHE_VIDEO_PICTURES, mediaArrayListList);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private class VideoCaptureTask extends AsyncTask<CameraView,Integer, Integer>{


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mListener.lockSceen(true); // stop viewpager from swiping while recoreding
            File outputDir =  getActivity().getExternalCacheDir() != null ? getActivity().getExternalCacheDir() : getActivity().getCacheDir();
            try {

                File outputFile = File.createTempFile("video", "mp4", outputDir);

                cameraView.startCapturingVideo(outputFile);
                //Toast.makeText(getActivity(),"StartRecoreding", Toast.LENGTH_LONG).show();
            } catch (IOException e) {
                e.printStackTrace();
            }


        }


        @Override
        protected Integer doInBackground(CameraView... params) {



            int i;
            for(i = 0; i <= 1000 && !isCancelled(); i++){

                publishProgress(i);
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return i;
        }



        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);


            progressbar.setProgress(values[values.length - 1]);

        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            mListener.lockSceen(false);
            if (!isCancelled()) cameraView.stopCapturingVideo();
            //Toast.makeText(getActivity(), " StopRecording", Toast.LENGTH_SHORT).show();
            capture_btn.clearFocus();
            progressbar.setProgress(0);
        }


        @Override
        protected void onCancelled(Integer integer) {
            super.onCancelled(integer);
            mListener.lockSceen(false); // stop viewpager from swiping while recoreding
            try {
                if (isCancelled()) cameraView.stopCapturingVideo();
            } catch (java.lang.RuntimeException e){
                e.printStackTrace();
            }
            capture_btn.clearFocus();
            progressbar.setProgress(0);
            //Toast.makeText(getActivity(), " StopRecording", Toast.LENGTH_SHORT).show();
        }



    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);


        if (isVisibleToUser) {
           onResume();
        } else {
           if(isResumed()) onPause();

        }




    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("mushy", "activity for Result" + resultCode);
        if (resultCode == 111){

            Log.d("mushy", "activity for Result");
            String media = data.getStringExtra("media");
            addMedia(media);


        }else if (resultCode == 112){

            Media media = data.getParcelableExtra("mediaObj");
            mediaArrayListList.add(media);
            pictureArrayListList.add(media);
            mediaHashSet.add(media);

            cacheMediaList();
            adapter.notifyDataSetChanged();
            isWaitingForResult = false;

        }

        if (resultCode == 100){
            mediaList = new ArrayList<>();
            limitIndicator.setProgress(mediaList.size());
        }

        showTooltipsForUploading();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ScreenLock) {
            mListener = (ScreenLock) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface ScreenLock{
        void lockSceen(boolean isLock);
    }



    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(UploadServiceEvent event) {


        switch (event.getStatus()){
            case UploadServiceEvent.RUNNING:

                uploadingContainer.setVisibility(View.VISIBLE);
                if (getView() != null)
                    getView().findViewById(R.id.view_media).setOnClickListener(v -> {

                        Intent intent = new Intent(getActivity(), PreviewMediaActivity.class);
                        intent.putParcelableArrayListExtra("mediaList", (ArrayList<Media>) mediaList);
                        startActivity(intent);
                    });
                Log.d("mushy", "uploading");
                break;
            case UploadServiceEvent.FINISHED:

                uploadingContainer.setVisibility(View.GONE);
                Log.d("mushy", "Not uploading");

                break;
        }
    }
}
