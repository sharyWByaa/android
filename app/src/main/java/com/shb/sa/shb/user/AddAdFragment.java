package com.shb.sa.shb.user;


import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GetTokenResult;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.gson.JsonObject;
import com.instagram.igdiskcache.SnapshotInputStream;
import com.marcinorlowski.fonty.Fonty;
import com.shb.sa.shb.Application;
import com.shb.sa.shb.R;
import com.shb.sa.shb.REST.ShbService;
import com.shb.sa.shb.REST.UserService;
import com.shb.sa.shb.REST.models.Ad;
import com.shb.sa.shb.model.Media;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AddAdFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddAdFragment extends Fragment implements Step {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private ArrayList<Media> mParam1;
    private String mParam2;
    private View view;
    private Detail mListener;

    @BindView(R.id.title_editText)
    EditText title;
    @BindView(R.id.product_editText)
    EditText desc;
    @BindView(R.id.price_editText)
    EditText price;



    public AddAdFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment AddAdFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AddAdFragment newInstance(ArrayList<Media> param1) {
        AddAdFragment fragment = new AddAdFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(ARG_PARAM1, param1);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getParcelableArrayList(ARG_PARAM1);

        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_add_ad, container, false);

        ButterKnife.bind(this, view);

        view.findViewById(R.id.add_ad_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                upload(mParam1);
            }
        });

        Fonty.setFonts((ViewGroup) view);
        return view;
    }

    private Ad createAd(){

        String title = this.title.getText().toString().trim();
        String desc = this.desc.getText().toString().trim();
        String price = this.price.getText().toString().trim();

        Ad ad = new Ad();
        ad.setTitle(title);
        ad.setDesc(desc);
        ad.setPrice(new BigDecimal(price));

        return ad;
    }

    private void postAd(List<com.shb.sa.shb.REST.models.Media> mediaList){

        final Ad ad = createAd();
        ad.setMedias(mediaList);


        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        if (mAuth.getCurrentUser() != null){
            mAuth.getCurrentUser().getToken(true).addOnSuccessListener(new OnSuccessListener<GetTokenResult>() {
                @Override
                public void onSuccess(GetTokenResult getTokenResult) {
                    ShbService.getInstence().getRetrofit().create(UserService.class).createAd(getTokenResult.getToken(), ad).enqueue(new Callback<JsonObject>() {
                        @Override
                        public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                            //Toast.makeText(getContext(), "Post the ad", Toast.LENGTH_LONG).show();
                        }

                        @Override
                        public void onFailure(Call<JsonObject> call, Throwable t) {

                        }
                    });
                }
            });
        }





    }

    private void upload(ArrayList<Media> medias){

        AsyncTask<Media, String, List<Media>> task = new AsyncTask<Media, String, List<Media>>() {


            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                //progrssDialog.show();
            }

            @Override
            protected List<Media> doInBackground(Media... params) {


                List<Media> mediaArrayList = new ArrayList<Media>();
                for (Media media : params) {
                    SnapshotInputStream mediaStreamitem = Application.getInstance(getContext()).get(media.getMediaKey()).get();
                    SnapshotInputStream thumnailStreamitem = Application.getInstance(getContext()).get(media.getThumnailKey()).get();

                    media.mediaStream = mediaStreamitem;
                    media.thumnailStream = thumnailStreamitem;
                    mediaArrayList.add(media);
                }

                return mediaArrayList;
            }

            @Override
            protected void onPostExecute(List<Media> s) {
                super.onPostExecute(s);


                FirebaseStorage storage = Application.getStorage();


                // Create a storage reference from our app
                final StorageReference storageRef = storage.getReference();



                final List<com.shb.sa.shb.REST.models.Media> medias1 = new ArrayList<>();
                for (Media inputStream : s) {
                    String uuid = UUID.randomUUID().toString().replaceAll("-", "");
                    final com.shb.sa.shb.REST.models.Media media = new com.shb.sa.shb.REST.models.Media();
                    media.setUid(uuid);

                    StorageReference mountainsRef = storageRef.child(uuid + ".mp4");


                    StorageReference mountainsThumnailRef = storageRef.child("images/" + uuid + ".png");


                    UploadTask uploadtask = mountainsRef.putStream(inputStream.mediaStream);
                    UploadTask uploadImagetask = mountainsThumnailRef.putStream(inputStream.thumnailStream);


                    uploadtask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            if (storageRef.getActiveUploadTasks().size() == 0){

                                media.setUrl(taskSnapshot.getDownloadUrl().getPath());



                                postAd(medias1);


                            }else {

                                media.setUrl(taskSnapshot.getDownloadUrl().getPath());

                            }
                        }
                    });

                    uploadImagetask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            if (storageRef.getActiveUploadTasks().size() == 0){
                                media.setThumbnail(taskSnapshot.getDownloadUrl().getPath());
                                postAd(medias1);

                            }else {

                                media.setThumbnail(taskSnapshot.getDownloadUrl().getPath());

                            }
                        }
                    });

                    medias1.add(media);





                }

                getActivity().setResult(222, new Intent());
                getActivity().finish();




            }
        };

        task.execute(medias.toArray(new Media[medias.size()]));
    }

    @Override
    public VerificationError verifyStep() {



        String title = this.title.getText().toString().trim();
        String desc = this.desc.getText().toString().trim();
        String price = this.price.getText().toString().trim();

        if (title.isEmpty()){
            return new VerificationError("title cannot be empty");
        }

        Ad ad = new Ad();
        ad.setTitle(title);
        ad.setDesc(desc);
        ad.setPrice(new BigDecimal(price));
        mListener.informationSubmited(ad);


        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {
        title.setError(error.getErrorMessage());
    }



    public interface Detail {
        void informationSubmited(Ad ad);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Detail) {
            mListener = (Detail) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
}
