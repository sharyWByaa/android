package com.shb.sa.shb.user.model;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.shb.sa.shb.R;
import com.shb.sa.shb.REST.models.Ad;

import java.util.Collections;
import java.util.List;

public  class FeatureAdAdapter extends RecyclerView.Adapter<FeatureAdAdapter.ViewHolder> {

        private List<Ad> mData = Collections.emptyList();
        private LayoutInflater mInflater;
        private ItemClickListener mClickListener;

        // data is passed into the constructor
        public FeatureAdAdapter(Context context, List<Ad> data) {
            this.mInflater = LayoutInflater.from(context);
            this.mData = data;
        }

        // inflates the row layout from xml when needed
        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = mInflater.inflate(R.layout.list_explore_item_feature, parent, false);
            ViewHolder viewHolder = new ViewHolder(view);
            return viewHolder;
        }

        // binds the data to the textview in each row
        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            Ad animal = mData.get(position);
            holder.myTextView.setText(animal.getTitle());
            holder.screenshot.setImageURI("https://firebasestorage.googleapis.com"+ animal.getMedias().get(0).getThumbnail());
        }

        // total number of rows
        @Override
        public int getItemCount() {
            return mData.size();
        }


        // stores and recycles views as they are scrolled off screen
        public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            public SimpleDraweeView screenshot;
            public TextView myTextView;

            public ViewHolder(View itemView) {
                super(itemView);
                myTextView = (TextView) itemView.findViewById(R.id.title_text);
                screenshot = itemView.findViewById(R.id.screenshot_image_view);
                itemView.setOnClickListener(this);
            }

            @Override
            public void onClick(View view) {
                if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
            }
        }

        // convenience method for getting data at click position
        public Ad getItem(int id) {
            return mData.get(id);
        }

        // allows clicks events to be caught
        public void setClickListener(ItemClickListener itemClickListener) {
            this.mClickListener = itemClickListener;
        }

        // parent activity will implement this method to respond to click events
        public interface ItemClickListener {
            void onItemClick(View view, int position);
        }
    }