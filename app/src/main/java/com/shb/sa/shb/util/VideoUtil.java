package com.shb.sa.shb.util;

import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;

/**
 * Created by mujtaba on 26/05/2017.
 */

public class VideoUtil {

    private final String path;
    private MediaMetadataRetriever retriever;

    public VideoUtil(String path){
        this.path = path;
        retriever = new MediaMetadataRetriever();
        retriever.setDataSource(path);
    }

    public Bitmap getVideoFrame() {
        try {
            return retriever.getFrameAtTime();
        } catch (IllegalArgumentException ex) {
            ex.printStackTrace();
        } catch (RuntimeException ex) {
            ex.printStackTrace();
        } finally {
            try {
                retriever.release();
            } catch (RuntimeException ex) {
            }
        }
        return null;
    }

    public Long getDuration(){
        String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        long timeInMillisec = Long.parseLong(time);
        return timeInMillisec;
    }

    public Boolean hasVideo(){
        String hasVideo = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_HAS_VIDEO);
        boolean isVideo = "yes".equals(hasVideo);
        return isVideo;
    }

    public String getMediaType(){
        String mediaType = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_MIMETYPE);
        return mediaType;
    }

    public int getWidth(){
        String width = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH);
        return Integer.valueOf(width);
    }

    public int getHight(){
        String hight = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT);
        return Integer.valueOf(hight);
    }


}
