package com.shb.sa.shb.user;

import android.content.Context;
import android.support.v7.widget.RecyclerView;

import com.airbnb.epoxy.Typed2EpoxyController;
import com.shb.sa.shb.REST.models.Favorite;
import com.shb.sa.shb.user.model.AdItemModel_;

import java.util.List;

/**
 * Created by mujtaba on 15/08/2017.
 */

public class FavController extends Typed2EpoxyController<List<Favorite>, Boolean> {

    private final RecyclerView.RecycledViewPool recycledViewPool;
    private final Context context;
    private final AdapterCallbacks adapterCallbacks;

    public FavController(RecyclerView.RecycledViewPool recycledViewPool, Context context, AdapterCallbacks adapterCallbacks){
        this.recycledViewPool = recycledViewPool;
        this.context = context;
        this.adapterCallbacks = adapterCallbacks;

    }



    @Override
    protected void buildModels(List<Favorite> favoriteList, Boolean data2) {

        for(Favorite favorite: favoriteList){
            add(new AdItemModel_().id(favorite.getId())
                    .title(favorite.getAd().getTitle())
                    .subtitle(String.valueOf(favorite.getAd().getPrice().doubleValue()))
                    .username(favorite.getAd().getUser().getUsername())
                    .screenshotPath(favorite.getAd().getMedias().get(0).getThumbnail())
                    .onClickListener((model, parentView, clickedView, position) -> adapterCallbacks.itemClicked(position, model)));
        }
    }

    public interface AdapterCallbacks {
        void itemClicked(int pos, AdItemModel_ model_);
    }
}
