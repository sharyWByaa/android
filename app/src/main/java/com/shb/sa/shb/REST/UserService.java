package com.shb.sa.shb.REST;

import com.google.gson.JsonObject;


import io.reactivex.Single;
import okhttp3.ResponseBody;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;

import com.shb.sa.shb.REST.models.Ad;
import com.shb.sa.shb.REST.models.AdResult;
import com.shb.sa.shb.REST.models.Category;
import com.shb.sa.shb.REST.models.Favorite;
import com.shb.sa.shb.REST.models.User;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by mujtaba on 29/03/2017.
 */

public interface UserService {

    @POST("users/new")
    Call<JsonObject> createUser(@Body User user);

    @POST("users/signin")
    Call<JsonObject> login(@Header("shb-token") String token);

    @POST("/users/username-signin")
    Call<JsonObject> loginWusername(@Body HashMap<String, String> credentials);

    @POST("me/ads/new")
    Call<JsonObject> createAd(@Header("shb-token") String token, @Body Ad ad);

    @GET("ads")
    Call<List<Ad>> getAds();

    @GET("ads")
    io.reactivex.Observable<List<Ad>> getAdsRx();

    @GET("ads/{catUid}")
    Single<AdResult> getAdsByCatRx(@Path("catUid") String catId, @Query("cursor") String endCursor);

    @GET("ads/{catUid}")
    Single<ResponseBody> getAdsByCatRxStore(@Path("catUid") String catId, @Query("cursor") String endCursor);

    @GET("ads/feature")
    Single<ResponseBody> getAdsByListofCatRx(@Query("catList") String catIds);

    @GET("ads/feature/{city}")
    Single<ResponseBody> getAdsByListofCatAndCityRx(@Path("city") String city, @Query("catList") String catIds);


    @GET("ads/{catUid}/{city}")
    Single<ResponseBody> getAdsByCatAndCityRx(@Path("catUid") String catId, @Path("city") String city, @Query("cursor") String endCursor);

    @GET("ads/near/{city}")
    Single<AdResult> getAdsByCityRx(@Path("city") String city, @Query("cursor") String endCursor);

    @GET("/me/ads")
    Call<List<Ad>> getMyAds(@Header("shb-token") String token);



    @DELETE("/me/ads/{id}")
    Call<JsonObject> deleteAd(@Header("shb-token") String token, @Path("id") String id);

    @PUT("/me/ads/{id}")
    Call<Ad> updateAd(@Header("shb-token") String token, @Path("id") String id, @Body Ad ad);



    @GET("user/{uid}")
    Call<User> getUserInfo(@Header("shb-token") String token, @Path("uid") String uid);

    @GET("/user/{uid}/ads")
    Call<List<Ad>> getUserAds(@Header("shb-token") String token, @Path("uid") String uid);

    @GET("users")
    Call<List<User>> getUsers(@Header("shb-token") String token);

    @GET("users/{phone}")
    Call<ResponseBody> canThisPhoneNumberRegistere(@Path("phone") String phoneNo);

    @GET("/me")
    Call<User> getMyInfo(@Header("shb-token") String token);


    @POST("/me/{field}")
    Call<User> updateField(@Header("shb-token") String token, @Path("field") String field, @Body String value);


    @POST("/fav/{userId}/{adId}")
    Call<JsonObject> createFav(@Header("shb-token") String token, @Path("adId") String adId, @Path("userId") String userId);

    @GET("/fav")
    Call<List<Favorite>> getMyFav(@Header("shb-token") String token);

    @GET("/fav")
    Single<ResponseBody> getMyFavRx(@Header("shb-token") String token);

    @DELETE("/me/fav/{adId}")
    Call<JsonObject> deleteAdFromFav(@Header("shb-token") String token, @Path("adId") String adId);

    @GET("/me/fav/{adId}")
    Call<JsonObject> findFav(@Header("shb-token") String token, @Path("adId") String adId);

    @GET("/ads/cat")
    Call<List<Category>> getCats();

}
