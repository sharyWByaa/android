package com.shb.sa.shb.util;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by mujtaba on 16/02/2018.
 */

public class SharedPrefUtil {

    private final Activity contex;
    private final String packageName;
    private String key;

    public SharedPrefUtil(Activity context){
        this.contex = context;
        this.packageName = contex.getApplicationContext().getPackageName() + ".";
    }

    public void writeSharedPref(String key, Boolean value){
        SharedPreferences sharedPref = contex.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(packageName + key, value);
        editor.apply();
    }

    public Boolean readSharedPref(String key){
        SharedPreferences sharedPref = contex.getPreferences(Context.MODE_PRIVATE);
        return sharedPref.getBoolean(packageName + key, false);
    }
}
