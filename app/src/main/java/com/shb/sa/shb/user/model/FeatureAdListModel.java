package com.shb.sa.shb.user.model;

import android.content.Context;
import android.support.v7.widget.RecyclerView;

import com.airbnb.epoxy.EpoxyAttribute;
import com.airbnb.epoxy.EpoxyModelClass;
import com.airbnb.epoxy.EpoxyModelWithHolder;
import com.shb.sa.shb.R;

import butterknife.BindView;

/**
 * Created by mujtaba on 07/12/2017.
 */

@EpoxyModelClass(layout = R.layout.model_horizental_list)
public abstract class FeatureAdListModel extends EpoxyModelWithHolder<FeatureAdListModel.AdListHolder> {

    @EpoxyAttribute
    RecyclerView.Adapter adapter;

    @EpoxyAttribute
    public Context context;


    @Override
    public void bind(AdListHolder holder) {
        super.bind(holder);
        holder.list.setAdapter(adapter);

    }

    @Override
    public void unbind(AdListHolder holder) {
        super.unbind(holder);
        holder.list.setAdapter(null);
    }

    public static class AdListHolder extends BaseEpoxyHolder {

        @BindView(R.id.list)
        RecyclerView list;



    }


    @Override
    public int getSpanSize(int totalSpanCount, int position, int itemCount) {
        return totalSpanCount;
    }
}
