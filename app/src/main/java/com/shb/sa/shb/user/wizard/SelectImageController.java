package com.shb.sa.shb.user.wizard;

import android.net.Uri;

import com.airbnb.epoxy.Typed2EpoxyController;
import com.shb.sa.shb.R;
import com.shb.sa.shb.user.model.AddPicButtonModel_;
import com.shb.sa.shb.user.model.ImageModel_;
import com.shb.sa.shb.user.model.TextModel_;

import java.util.HashSet;
import java.util.List;

/**
 * Created by mujtaba on 22/06/2017.
 */

public class SelectImageController extends Typed2EpoxyController<HashSet<Uri>, List<Uri>> {

    private final AdapterCallbacks callbacks;

    public SelectImageController(AdapterCallbacks callbacks){

        this.callbacks = callbacks;
    }
    @Override
    protected void buildModels(HashSet<Uri> data, List<Uri> mediaList) {

        add(new TextModel_().id("titleVideo").title(R.string.captured_video));

        for (Uri media: mediaList){
            add(new ImageModel_().id(media.getPath()).imageRes(media)
                    .clickListener((model, parentView, clickedView, position) -> callbacks.deleteVideo(model.imageRes())));
        }

        add(new TextModel_().id("title").title(R.string.captured_pictures));

        for (Uri uri: data) {
            add(new ImageModel_()
                    .id(uri.getPath())
                    .imageRes(uri)
                    .clickListener((model, parentView, clickedView, position) -> callbacks.deleteImage(model.imageRes())));
        }

        new AddPicButtonModel_().id("addImage").button_title(R.string.app_message_user)
                .clickListener((model, parentView, clickedView, position) -> callbacks.addImages())
                .addIf(data.size() < 9,this);
    }

    public interface AdapterCallbacks {
        void addImages();
        void deleteImage(Uri url);
        void deleteVideo(Uri url);
    }
}
