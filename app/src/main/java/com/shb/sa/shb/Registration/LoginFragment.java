package com.shb.sa.shb.Registration;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatDelegate;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;


import com.anupcowkur.reservoir.Reservoir;
import com.anupcowkur.reservoir.ReservoirGetCallback;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GetTokenResult;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.JsonObject;
import com.marcinorlowski.fonty.Fonty;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.HashMap;
import java.util.List;

import com.shb.sa.shb.BaseFragment;
import com.shb.sa.shb.HomePageActivity;
import com.shb.sa.shb.LocationSelectionActivity;
import com.shb.sa.shb.R;
import com.shb.sa.shb.REST.ShbService;
import com.shb.sa.shb.REST.UserService;
import com.shb.sa.shb.chat.fcm.MyFirebaseInstanceIDService;
import com.shb.sa.shb.chat.logic.ChatInteractor;

import br.com.simplepass.loading_button_lib.customViews.CircularProgressButton;
import co.chatsdk.core.dao.User;
import co.chatsdk.core.session.NM;
import co.chatsdk.core.session.NetworkManager;
import co.chatsdk.core.session.StorageManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link LoginFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link LoginFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LoginFragment extends BaseFragment implements Validator.ValidationListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

    }

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    private View view;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    @NotEmpty
    private EditText password;
    @NotEmpty
    //@Pattern(regex = "^0[0-9]{9}")
    private EditText phoneNumberView;
    private Validator validator;
    private CircularProgressButton loginBtn;

    public LoginFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment LoginFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static LoginFragment newInstance(String param1, String param2) {
        LoginFragment fragment = new LoginFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_login, container, false);




        mAuth = FirebaseAuth.getInstance();

        if (mAuth.getCurrentUser() != null && mAuth.getCurrentUser().getProviders().size() > 1) {
            // already signed in
            // get token

            Reservoir.getAsync(ChatInteractor.ARG_FIREBASE_TOKEN, String.class, new ReservoirGetCallback<String>() {
                @Override
                public void onSuccess(String token) {
                    FirebaseDatabase.getInstance()
                            .getReference()
                            .child(ChatInteractor.ARG_USERS)
                            .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                            .child(ChatInteractor.ARG_FIREBASE_TOKEN)
                            .setValue(token);
                }

                @Override
                public void onFailure(Exception e) {

                }
            });

            Intent intent = new Intent(getContext(), HomePageActivity.class);
            startActivity(intent);
            getActivity().finish();

            /*
            mAuth.getCurrentUser().getToken(true).addOnCompleteListener(this, new OnCompleteListener<GetTokenResult>() {
                @Override
                public void onComplete(@NonNull Task<GetTokenResult> task) {

                    token = task.getResult().getToken();
                    Toast.makeText(getApplicationContext(),"Signed in" + token, Toast.LENGTH_LONG).show();
                }
            });

*/

            //Toast.makeText(this,"Signed in", Toast.LENGTH_LONG).show();
        }

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {



                    // User is signed in
                    //Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                } else {
                    // User is signed out
                    //Log.d(TAG, "onAuthStateChanged:signed_out");
                }
                // ...
            }
        };



        phoneNumberView = (EditText) view.findViewById(R.id.email_editText);
        password = (EditText) view.findViewById(R.id.password_editText);



        loginBtn = view.findViewById(R.id.login_btn);
        loginBtn.findViewById(R.id.login_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                validator.validate();

            }
        });

        view.findViewById(R.id.signin_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), SignupFlowActivity.class);
                startActivity(intent);
            }
        });

        view.findViewById(R.id.forget_password_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(),ForgetMyPasswordActivity.class);

                startActivity(intent);
            }
        });

        validator = new Validator(this);
        validator.setValidationListener(this);
        Fonty.setFonts((ViewGroup) view);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        loginBtn.dispose();
    }

    @Override
    public void onValidationSucceeded() {


        Toast.makeText(getContext(), "Got it", Toast.LENGTH_SHORT).show();

        loginBtn.startAnimation();
        password.setEnabled(false);
        phoneNumberView.setEnabled(false);


        if (android.util.Patterns.EMAIL_ADDRESS.matcher(phoneNumberView.getText().toString().trim()).matches())
        {

            Task<AuthResult> authResultTask = mAuth.signInWithEmailAndPassword(phoneNumberView.getText().toString().trim(), password.getText().toString())
                    .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {


                            if (!task.isSuccessful()) {

                                phoneNumberView.setEnabled(true);
                                password.setEnabled(true);
                                loginBtn.revertAnimation();


                                return;
                            }

                            //String token = task.getResult().getUser().getToken(true).getResult().getToken();


                            task.getResult().getUser().getIdToken(true).addOnCompleteListener(getActivity(), new OnCompleteListener<GetTokenResult>() {
                                @Override
                                public void onComplete(@NonNull Task<GetTokenResult> task) {


                                    ShbService retrofit = ShbService.getInstence();
                                    UserService api = retrofit.getRetrofit().create(UserService.class);
                                    api.login(task.getResult().getToken()).enqueue(new Callback<JsonObject>() {
                                        @Override
                                        public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                                            onSuccLogin();

                                            Intent intent = new Intent(getContext(), HomePageActivity.class);
                                            //startActivity(intent);
                                            presentActivity(loginBtn,intent);
                                            //getActivity().finish();
                                            //loginBtn.revertAnimation();

                                        }

                                        @Override
                                        public void onFailure(Call<JsonObject> call, Throwable t) {

                                            phoneNumberView.setEnabled(true);
                                            password.setEnabled(true);

                                            loginBtn.revertAnimation();
                                            Toast.makeText(getContext(), t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();

                                        }
                                    });


                                }
                            });


                        }
                    });


        } else {
            //user name login

            HashMap<String, String> map = new HashMap<>();
            map.put("username",phoneNumberView.getText().toString().trim());
            map.put("password", password.getText().toString());

            ShbService.getInstence().getRetrofit().create(UserService.class).loginWusername(map).enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {




                    if (response.isSuccessful()) {
                        String token = response.body().get("token").getAsString();


                        FirebaseAuth.getInstance().signInWithCustomToken(token).addOnCompleteListener(task -> {


                            //loginBtn.revertAnimation();

                            onSuccLogin();

                            task.getResult().getUser();
                            Intent intent = new Intent(getContext(), HomePageActivity.class);
                            presentActivity(loginBtn,intent);
                            //getActivity().finish();

                        });
                    }else {

                        phoneNumberView.setEnabled(true);
                        password.setEnabled(true);

                        loginBtn.revertAnimation();
                        phoneNumberView.setError("Please double check your login info ");
                        password.setError("please check double check your login info");
                        //Toast.makeText(getContext(), "" + response.code(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable throwable) {

                    phoneNumberView.setEnabled(true);
                    password.setEnabled(true);
                    loginBtn.revertAnimation();
                }
            });
        }

    }

    private void onSuccLogin() {
        //setup the chatSDK client
        NetworkManager.shared().a.auth.authenticateWithCachedToken().subscribe(()->{


            User user = StorageManager.shared().fetchOrCreateEntityWithEntityID(User.class, NM.currentUser().getEntityID());

        }, (error)-> Log.d("mush", error.getMessage()));

        new MyFirebaseInstanceIDService().onTokenRefresh();
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {

        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getActivity());

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
                
            } else {
                Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
            }
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    public void presentActivity(View view, Intent intent) {
        ActivityOptionsCompat options = ActivityOptionsCompat.
                makeSceneTransitionAnimation(getActivity(), view, "transition");
        int revealX = (int) (view.getX() + view.getWidth() / 2);
        int revealY = (int) (view.getY() + view.getHeight() / 2);

        intent.putExtra(LocationSelectionActivity.EXTRA_CIRCULAR_REVEAL_X, revealX);
        intent.putExtra(LocationSelectionActivity.EXTRA_CIRCULAR_REVEAL_Y, revealY);

        ActivityCompat.startActivity(getContext(), intent, options.toBundle());
    }
}
