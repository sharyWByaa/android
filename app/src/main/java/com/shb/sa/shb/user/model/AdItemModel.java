package com.shb.sa.shb.user.model;

import android.view.View;

import com.airbnb.epoxy.EpoxyAttribute;
import com.airbnb.epoxy.EpoxyModel;
import com.airbnb.epoxy.EpoxyModelClass;
import com.google.firebase.storage.FirebaseStorage;
import com.shb.sa.shb.Application;
import com.shb.sa.shb.R;

import static com.airbnb.epoxy.EpoxyAttribute.Option.DoNotHash;

/**
 * Created by mujtaba on 18/06/2017.
 */

@EpoxyModelClass(layout = R.layout.model_explore_item)
public abstract class AdItemModel extends EpoxyModel<AdItemView> {

    @EpoxyAttribute String title;
    @EpoxyAttribute String subtitle;
    @EpoxyAttribute String username;
    @EpoxyAttribute String screenshotPath;
    @EpoxyAttribute String date;
    @EpoxyAttribute(DoNotHash)
    View.OnClickListener onClickListener;

    private final FirebaseStorage storage = Application.getStorage();

    @Override
    public void bind(AdItemView adHolder) {
        adHolder.title.setText(title);
        adHolder.caption.setText(subtitle);
        adHolder.username.setText(username);
        adHolder.setOnClickListener(onClickListener);
        adHolder.date.setText(date);

        //StorageReference imgUrl = storage.getReferenceFromUrl(("https://firebasestorage.googleapis.com"+ screenshotPath));

        adHolder.screenshot.setImageURI((Application.baseStoreageUrl + screenshotPath));
        //imgUrl.getDownloadUrl().addOnSuccessListener( uri -> adHolder.screenshot.setImageURI(uri));
        //adHolder.screenshot.setImageURI("https://firebasestorage.googleapis.com"+ screenshotPath.replace("images/", "images%2F") +"?alt=media");

    }

    @Override
    public int getSpanSize(int totalSpanCount, int position, int itemCount) {
        // We want the header to take up all spans so it fills the screen width
        return totalSpanCount/2;
    }

}
