package com.shb.sa.shb.welcome;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.shb.sa.shb.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class MainWelcomeFragment extends Fragment {


    public MainWelcomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main_welcome, container, false);
    }

}
