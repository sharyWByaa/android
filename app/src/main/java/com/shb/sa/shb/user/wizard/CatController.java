package com.shb.sa.shb.user.wizard;

import com.airbnb.epoxy.TypedEpoxyController;
import com.shb.sa.shb.R;
import com.shb.sa.shb.REST.models.Category;
import com.shb.sa.shb.user.model.RadioButtonModel_;
import com.shb.sa.shb.user.model.TextModel_;

import java.util.List;

/**
 * Created by mujtaba on 21/06/2017.
 */

public class CatController extends TypedEpoxyController<List<Category>> {

    private final AdapterCallbacks adapterCallbacks;

    public CatController(AdapterCallbacks adapterCallbacks){
        this.adapterCallbacks = adapterCallbacks;
    }
    @Override
    protected void buildModels(List<Category> data) {

        add(new TextModel_().id("title").title(R.string.select_cat));

        if (data != null) {
            for (int i = 0; i < data.size(); i++) {
                add(new RadioButtonModel_().id(i).title(data.get(i).getTitle()).selected(data.get(i).isSelected())
                        .clickListener((model, parentView, clickedView, position) ->
                                adapterCallbacks.catSelected(position, model)));
            }
        }
    }

    public interface AdapterCallbacks {
        void catSelected(int pos, RadioButtonModel_ model_);
    }
}
