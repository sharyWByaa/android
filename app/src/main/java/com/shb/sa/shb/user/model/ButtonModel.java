package com.shb.sa.shb.user.model;

import android.support.annotation.StringRes;
import android.view.View;

import com.airbnb.epoxy.DataBindingEpoxyModel;
import com.airbnb.epoxy.EpoxyAttribute;
import com.airbnb.epoxy.EpoxyModelClass;
import com.shb.sa.shb.R;

import static com.airbnb.epoxy.EpoxyAttribute.Option.DoNotHash;

/**
 * Created by mujtaba on 20/06/2017.
 */
@EpoxyModelClass(layout = R.layout.model_button)
public abstract class ButtonModel extends DataBindingEpoxyModel {
    @EpoxyAttribute
    @StringRes
    int button_title;

    @EpoxyAttribute(DoNotHash)
    View.OnClickListener clickListener;


}
