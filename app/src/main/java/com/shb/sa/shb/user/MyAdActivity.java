package com.shb.sa.shb.user;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.shb.sa.shb.R;
import com.shb.sa.shb.REST.models.Ad;

import java.io.Serializable;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MyAdActivity extends AppCompatActivity implements MyAdFragment.OnListFragmentInteractionListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_ad);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, MyAdFragment.newInstance(0)).commit();
    }

    @Override
    public void onListFragmentInteraction(Ad item) {

        Intent intent = new Intent(this, EditAdActivity.class);
        intent.putExtra("ad", (Serializable) item);
        startActivity(intent);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
