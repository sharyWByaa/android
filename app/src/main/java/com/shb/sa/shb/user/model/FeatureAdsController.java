package com.shb.sa.shb.user.model;

import android.view.View;

import com.airbnb.epoxy.CarouselModel_;
import com.airbnb.epoxy.Typed3EpoxyController;
import com.shb.sa.shb.R;
import com.shb.sa.shb.REST.models.Ad;
import com.shb.sa.shb.REST.models.AdResult;
import com.shb.sa.shb.REST.models.Category;


import org.ocpsoft.prettytime.PrettyTime;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by mujtaba on 07/12/2017.
 */

public class FeatureAdsController extends Typed3EpoxyController<Map< Category,AdResult>, Integer, Integer > {

    public static final int LOADED = 0;
    public static final int LOADING = 1;
    public static final int LOADING_BACKGROUND = 2; // don't show it loading
    public static final int EROOR = -1;

    private final Callback callback;
    final PrettyTime p = new PrettyTime();

    public FeatureAdsController(Callback callback){

        this.callback = callback;

    }
    @Override
    protected void buildModels(Map< Category,AdResult> data1, Integer mode, Integer loadingStatus) {



        new EmptyViewModel_().id("space").addTo(this);

        if (mode.equals(1) && loadingStatus.equals(LOADED)  &&  data1.isEmpty()) {

            new TextModel_().id("no_items").title(R.string.err_there_is_no_ads).title_alignment(View.TEXT_ALIGNMENT_CENTER).addTo(this);
            new ButtonModel_().id("show_all_ads").button_title(R.string.show_ads_from_all)
                    .spanSizeOverride((totalSpanCount, position, itemCount) -> totalSpanCount)
                    .clickListener(view -> callback.expandSearch())
                    .addTo(this);

        }





        for (Map.Entry<Category, AdResult> adSection: data1.entrySet()){

            if (adSection.getValue().getResults() != null && adSection.getValue().getResults().size() > 0) {
                new TextShowMoreModel_().id(adSection.getKey().getUid()).title(adSection.getKey().getTitle())
                        .category(adSection.getKey())
                        .clickListener((model, parentView, clickedView, position) -> callback.onSectionClicked(model.category()))
                        .addTo(this);
                //FeatureAdAdapter adapter = new FeatureAdAdapter(context, adSection.getValue());
                //new FeatureAdListModel_().id(adSection.getKey()).adapter(adapter).addTo(this);

                List<AdItemFeatureModel_> models = new ArrayList<>();

                for (Ad ad : adSection.getValue().getResults()) {
                    models.add(
                            new AdItemFeatureModel_()
                                    .id(ad.getId())
                                    .title(ad.getTitle())
                                    .date(p.format(new Date(ad.getCreated())))
                                    .subtitle(ad.getPrice().floatValue())
                                    .username(ad.getUser().getUsername())
                                    .screenshotPath(ad.getMedias().get(0).getThumbnail())
                                    .key(adSection.getKey())
                                    .onClickListener((model, parentView, clickedView, position) ->
                                            callback.onAdClick(clickedView, model.key(), position))
                    );
                }

                new CarouselModel_().id(adSection.getKey().getUid()+"carousel")
                        .models(models)
                        .hasFixedSize(true)
                        .paddingDp(5)
                        .numViewsToShowOnScreen(3.05f)
                        .addTo(this);
            }

        }

        new LoadingModel_().id("loading").addIf(loadingStatus.equals(LOADING), this);

    }



    public interface Callback {

        void onAdClick(View view, Category key, int pos);
        void onSectionClicked(Category category);
        void expandSearch();

    }

}
