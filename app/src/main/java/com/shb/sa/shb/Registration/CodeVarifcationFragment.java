package com.shb.sa.shb.Registration;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Length;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.shb.sa.shb.R;
import com.stepstone.stepper.BlockingStep;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CodeVarifcationFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CodeVarifcationFragment extends Fragment implements BlockingStep, Validator.ValidationListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String TAG = "Code_verification";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private View view;

    @NotEmpty
    @Length(max = 6, min = 6)
    @BindView(R.id.code_verification)
    PinEntryEditText codeVerifcation;

    @BindView(R.id.resend_btn)
    Button resend;

    String verificationId;
    private OnFragmentInteractionListener mListener;
    private Validator validator;
    private StepperLayout.OnNextClickedCallback callback;


    public CodeVarifcationFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CodeVarifcationFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CodeVarifcationFragment newInstance(String param1, String param2) {
        CodeVarifcationFragment fragment = new CodeVarifcationFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        validator = new Validator(this);
        validator.setValidationListener(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_code_varifcation, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (codeVerifcation != null){
            codeVerifcation.setOnPinEnteredListener(charSequence -> {

                if (codeVerifcation.getText().toString().trim().isEmpty()) {
                    codeVerifcation.setError("empty");
                    return;
                }

                //Log.d("mushy", "code " + codeVerifcation.getText().toString() + "  code2 " + charSequence);
                validator.validate();
            });
        }


        resend.setOnClickListener(view1 -> mListener.resendPinNo());
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(SignupFlowActivity.CodeExpUpdate event) {
        if (event.getToExp() != 0) {
            resend.setText("resend (" + event.getToExp() / 1000 + ")");
            resend.setEnabled(false);
        }else {
            resend.setEnabled(true);
            resend.setText("resend");
        }
    }



    @Override
    public void onNextClicked(StepperLayout.OnNextClickedCallback callback) {




        if (codeVerifcation.getText().toString().trim().isEmpty()) {
            codeVerifcation.setError("empty");
            return;
        }
        this.callback = callback;
        validator.validate();
        //mListener.onCodeSubmitted(codeVerifcation.getText().toString().trim(), codeVerifcation);
    }



    @Override
    public void onCompleteClicked(StepperLayout.OnCompleteClickedCallback callback) {

    }

    @Override
    public void onBackClicked(StepperLayout.OnBackClickedCallback callback) {

    }

    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof MobileVerificationFragment.OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onValidationSucceeded() {

        mListener.onCodeSubmitted(codeVerifcation.getText().toString().trim(), codeVerifcation);
    }

    @Override
    public void onValidationFailed(List<ValidationError> list) {

        for (ValidationError error : list) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getContext());

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {

            }
        }
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onCodeSubmitted(String code, EditText codeEditText);
        void resendPinNo();


    }
}
