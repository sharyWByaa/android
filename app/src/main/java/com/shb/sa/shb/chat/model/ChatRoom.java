package com.shb.sa.shb.chat.model;

import android.net.Uri;

/**
 * Created by mujtaba on 13/08/2017.
 */

public class ChatRoom {


    String id;
    String otherUserId;
    String meId;
    String lastMessage;
    long timeStamp;

    Uri avatar;


    Boolean readByMe;

    public ChatRoom(String id, String otherUserId, String meId, String lastMessage, Long timeStamp) {
        this.id = id;
        this.otherUserId = otherUserId;
        this.meId = meId;
        this.lastMessage = lastMessage;
        this.timeStamp = timeStamp;
    }



    public Boolean getReadByMe() {
        return readByMe;
    }

    public void setReadByMe(Boolean readByMe) {
        this.readByMe = readByMe;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOtherUserId() {
        return otherUserId;
    }

    public void setOtherUserId(String otherUserId) {
        this.otherUserId = otherUserId;
    }

    public String getMeId() {
        return meId;
    }

    public void setMeId(String meId) {
        this.meId = meId;
    }


    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    public Uri getAvatar() {
        return avatar;
    }

    public void setAvatar(Uri avatar) {
        this.avatar = avatar;
    }


    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }


    @Override
    public String toString() {
        return "ChatRoom{" +
                "id='" + id + '\'' +
                ", otherUserId='" + otherUserId + '\'' +
                ", meId='" + meId + '\'' +
                ", lastMessage='" + lastMessage + '\'' +
                ", timeStamp=" + timeStamp +
                ", avatar=" + avatar +
                '}';
    }

}
