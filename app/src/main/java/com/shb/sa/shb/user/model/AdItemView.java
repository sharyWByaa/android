package com.shb.sa.shb.user.model;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.StringRes;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.shb.sa.shb.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdItemView extends RelativeLayout {

  @BindView(R.id.title_text)
  TextView title;
  @BindView(R.id.price_text) TextView caption;


  @BindView(R.id.username_text) TextView username;

  @BindView(R.id.date_text) TextView date;



  @BindView(R.id.screenshot_image_view)
  SimpleDraweeView screenshot;

  public AdItemView(Context context, AttributeSet attrs) {
    super(context, attrs);
    init();
  }

  private void init() {

    inflate(getContext(), R.layout.list_explore_item, this);
    ButterKnife.bind(this);
  }

  public void setTitle(@StringRes int title) {
    this.title.setText(title);
  }

  public void setCaption(@StringRes int caption) {
    this.caption.setText(caption);
  }

    public void setScreenshot(String path) {
        this.screenshot.setImageURI(Uri.parse(path));
    }
}