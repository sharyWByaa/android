package com.shb.sa.shb;

import android.net.Uri;

import com.airbnb.epoxy.Typed2EpoxyController;
import com.shb.sa.shb.model.Media;

import com.shb.sa.shb.user.model.PreviewImageModel_;

import java.util.List;

/**
 * Created by mujtaba on 14/11/2017.
 */

public class MediaController extends Typed2EpoxyController<List<Media>, List<Uri>> {

    private final Callback callback;

    public MediaController(Callback callback){
        this.callback = callback;
    }


    @Override
    protected void buildModels(List<Media> media, List<Uri> uris) {

        for (Media media1 : media){


            new PreviewImageModel_().id(media1.getThumnailKey())
                    .imageRes(Uri.parse("https://images.pexels.com/photos/207962/pexels-photo-207962.jpeg"))
                    .addTo(this);
        }
    }


    interface Callback{

    }
}
