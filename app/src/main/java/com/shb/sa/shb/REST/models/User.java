package com.shb.sa.shb.REST.models;

import android.os.Parcel;
import android.os.Parcelable;

public class User implements Parcelable {





    public String uid;
    private String idpToken;

    private String idpSecret;
    private String providerTypeAuth;
    private String username;
    private String phoneNumber;
    private String name;
    private String email;
    private String password;

    public String fireBaseUsername;

    private boolean validated;
    private boolean banned;
    private String about;


    private boolean showPhoneNo;


    private String ip;

    public String getIdpToken() {
        return idpToken;
    }

    public void setIdpToken(String idpToken) {
        this.idpToken = idpToken;
    }

    public String getIdpSecret() {
        return idpSecret;
    }

    public void setIdpSecret(String idpSecret) {
        this.idpSecret = idpSecret;
    }

    public String getProviderTypeAuth() {
        return providerTypeAuth;
    }

    public void setProviderTypeAuth(String providerTypeAuth) {
        this.providerTypeAuth = providerTypeAuth;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public boolean isShowPhoneNo() {
        return showPhoneNo;
    }

    public void setShowPhoneNo(boolean showPhoneNo) {
        this.showPhoneNo = showPhoneNo;
    }



    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    @Override
    public String toString() {
        return "User{" +
                "uid='" + uid + '\'' +
                ", idpToken='" + idpToken + '\'' +
                ", idpSecret='" + idpSecret + '\'' +
                ", providerTypeAuth='" + providerTypeAuth + '\'' +
                ", username='" + username + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                '}';
    }


    public User(){}



    protected User(Parcel in) {
        uid = in.readString();
        idpToken = in.readString();
        idpSecret = in.readString();
        providerTypeAuth = in.readString();
        username = in.readString();
        phoneNumber = in.readString();
        name = in.readString();
        email = in.readString();
        fireBaseUsername = in.readString();
        validated = in.readByte() != 0x00;
        banned = in.readByte() != 0x00;
        about = in.readString();
        showPhoneNo = in.readByte() != 0x00;
        ip = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(uid);
        dest.writeString(idpToken);
        dest.writeString(idpSecret);
        dest.writeString(providerTypeAuth);
        dest.writeString(username);
        dest.writeString(phoneNumber);
        dest.writeString(name);
        dest.writeString(email);
        dest.writeString(fireBaseUsername);
        dest.writeByte((byte) (validated ? 0x01 : 0x00));
        dest.writeByte((byte) (banned ? 0x01 : 0x00));
        dest.writeString(about);
        dest.writeByte((byte) (showPhoneNo ? 0x01 : 0x00));
        dest.writeString(ip);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}