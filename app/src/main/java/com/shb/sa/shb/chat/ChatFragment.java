package com.shb.sa.shb.chat;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.shb.sa.shb.BaseFragment;
import com.shb.sa.shb.R;
import com.shb.sa.shb.chat.event.NotificationEvent;
import com.shb.sa.shb.chat.logic.ChatContract;
import com.shb.sa.shb.chat.logic.ChatInteractor;
import com.shb.sa.shb.chat.logic.ChatPresenter;
import com.shb.sa.shb.chat.model.Chat;
import com.vanniktech.emoji.EmojiEditText;
import com.vanniktech.emoji.EmojiPopup;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.chatsdk.core.dao.Thread;
import co.chatsdk.core.session.NM;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ChatFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ChatFragment extends BaseFragment implements ChatContract.View, TextView.OnEditorActionListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    EmojiPopup emojiPopup;
    //firebase token
    //firebase reciverUID
    // firebase reciver

    private final RecyclerView.RecycledViewPool recycledViewPool = new RecyclerView.RecycledViewPool();

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private ChatPresenter mChatPresenter;
    private String receiverUid;
    private View view;
    @BindView(R.id.list)
    RecyclerView list;
    @BindView(R.id.fragment_chat_message)
    EditText messageEditText;
    @BindView(R.id.fragment_smiley_face_btn)
    Button smilyFaceBtn;

    private ArrayAdapter<String> adapter;
    private ChatController controller;
    private LinearLayoutManager mLinearLayoutManager;
    private ArrayList<Chat> chats;
    private Thread thread;
    private ChatSDKController controller2;


    public ChatFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *

     * @return A new instance of fragment ChatFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ChatFragment newInstance(String receiver, String receiverUid, String firebaseToken) {
        ChatFragment fragment = new ChatFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, receiver);
        args.putString(ARG_PARAM2, receiverUid);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            receiverUid = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_chat, container, false);
        ButterKnife.bind(this, view);
        emojiPopup = EmojiPopup.Builder.fromRootView(view).build((EmojiEditText) messageEditText);
        smilyFaceBtn.setText("{faw-smile-o}");

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);



        controller =  new ChatController(recycledViewPool, getActivity(), FirebaseAuth.getInstance().getCurrentUser().getUid());

        mLinearLayoutManager = new LinearLayoutManager(getActivity());
        controller.setSpanCount(2);
        //mLinearLayoutManager.setSpanSizeLookup(controller.getSpanSizeLookup());

        mLinearLayoutManager.setStackFromEnd(true);
        mLinearLayoutManager.setReverseLayout(true);
        //mLinearLayoutManager.setStackFromEnd(true);
        list.setLayoutManager(mLinearLayoutManager);

        list.setRecycledViewPool(recycledViewPool);
        list.setHasFixedSize(true);
        list.setAdapter(controller2.getAdapter());


        chats = new ArrayList<>();

        controller.setData(chats, false);


        mChatPresenter = new ChatPresenter(this);
        view.findViewById(R.id.btn_send).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMessage(messageEditText.getText().toString());
                messageEditText.getText().clear();
            }
        });
        mChatPresenter.getMessage(FirebaseAuth.getInstance().getCurrentUser().getUid(),receiverUid);

        smilyFaceBtn.setOnClickListener((v) -> {

            if (emojiPopup.isShowing()){
                emojiPopup.dismiss();
                smilyFaceBtn.setText("{faw-smile-o}");

            } else {
                emojiPopup.toggle();
                smilyFaceBtn.setText("{faw-keyboard-o}");

            }
        });



    }

    void sendMessage(String message){
        NM.thread().sendMessageWithText(message,thread).subscribe((ms)->{

        });
        FirebaseDatabase.getInstance()
                .getReference()
                .child(ChatInteractor.ARG_USERS)
                .child(receiverUid)
                .child(ChatInteractor.ARG_FIREBASE_TOKEN)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        String fcmToken = dataSnapshot.getValue(String.class);

                        Log.d("mushy", fcmToken);

                        String senderuid = FirebaseAuth.getInstance().getCurrentUser().getUid();
                        Chat chat = new Chat(mAuth.getCurrentUser().getDisplayName(), senderuid, "Mujy", receiverUid, message, new Date().getTime());
                        mChatPresenter.sendMessage(getActivity().getApplicationContext(),
                                chat,
                                fcmToken);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });


    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        return false;
    }

    @Override
    public void onSendMessageSuccess() {


    }

    @Override
    public void onSendMessageFailure(String message) {


    }

    @Override
    public void onGetMessagesSuccess(Chat chat) {

        chats.add(chat);
        controller.setData(chats, true);
        list.smoothScrollToPosition(chats.size() - 1);


        //list.smoothScrollToPosition(chats.size() - 1);


    }

    @Override
    public void onGetMessagesFailure(String message) {


    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void onPushNotificationEvent(NotificationEvent pushNotificationEvent) {
            mChatPresenter.getMessage(FirebaseAuth.getInstance().getCurrentUser().getUid(),
                    pushNotificationEvent.getUid());
            Toast.makeText(getContext(), "Got push", Toast.LENGTH_SHORT).show();

    }


    @Override
    public void onDestroy() {
        super.onDestroy();

    }
}
