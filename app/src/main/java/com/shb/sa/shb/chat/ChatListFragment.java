package com.shb.sa.shb.chat;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.shb.sa.shb.BaseFragment;
import com.shb.sa.shb.R;
import com.shb.sa.shb.chat.logic.ChatInteractor;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.chatsdk.core.dao.Thread;
import co.chatsdk.core.dao.User;
import co.chatsdk.core.events.NetworkEvent;
import co.chatsdk.core.interfaces.ThreadType;
import co.chatsdk.core.session.NM;
import io.reactivex.disposables.Disposable;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ChatListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ChatListFragment extends BaseFragment  {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private View view;
    @BindView(R.id.list)
    RecyclerView recyclerView;
    private String[] values;

    @BindView(R.id.empty_list)
    View emptyList;

    private List<Disposable> disposableList = new ArrayList<>();
    private List<Thread> threads = new ArrayList<>();
    private ThreadAdapter threadsList;


    public ChatListFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ChatListFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ChatListFragment newInstance(String param1, String param2) {
        ChatListFragment fragment = new ChatListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        view = inflater.inflate(R.layout.fragment_chat_list, container, false);

        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));





        /*
        FirebaseDatabase.getInstance().getReference().child("user_in_rooms").child(mAuth.getCurrentUser().getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {



                Toast.makeText(getContext(), String.valueOf(dataSnapshot.getChildrenCount() + "  " ), Toast.LENGTH_LONG).show();

                ArrayList<String> rooms = new ArrayList<>();

                dataSnapshot.getChildren().forEach(dataSnapshot1 -> rooms.add(dataSnapshot1.getKey()));

                for(int i = 0; i < rooms.size(); i++) {
                    int finalI = i;
                    FirebaseDatabase.getInstance().getReference().child("members").child(rooms.get(i)).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {

                            dataSnapshot.getChildren().forEach(dataSnapshot1 -> {

                                if (!dataSnapshot1.getKey().equals(mAuth.getCurrentUser().getUid())) {

                                    //chatRoomList.add(new ChatRoom(rooms.get(finalI), dataSnapshot1.getKey(), mAuth.getCurrentUser().getUid()));
                                    //adapter.notifyDataSetChanged();
                                    FirebaseDatabase.getInstance().getReference().child("chats").child(rooms.get(finalI)).addValueEventListener(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {

                                            String lastMesg = (String) dataSnapshot.child("lastMessage").getValue();
                                            Long timestamp = (Long) dataSnapshot.child("timestamp").getValue();
                                            ChatRoom chatRoom = new ChatRoom(rooms.get(finalI), dataSnapshot1.getKey(), mAuth.getCurrentUser().getUid(), lastMesg, timestamp);
                                            chatRoomList.add(chatRoom);

                                            adapter.notifyDataSetChanged();
                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {

                                        }
                                    });

                                }
                            });

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });




                }






            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

*/

        /*
        mAuth.getCurrentUser().getIdToken(true).addOnSuccessListener(new OnSuccessListener<GetTokenResult>() {
            @Override
            public void onSuccess(GetTokenResult getTokenResult) {

                ShbService.getInstence().getRetrofit().create(UserService.class).getUsers(getTokenResult.getToken()).enqueue(new Callback<List<User>>() {
                    @Override
                    public void onResponse(Call<List<User>> call, Response<List<User>> response) {

                        if(response.body() == null) return;

                        values = new String[response.body().size()];

                        for(int i = 0; i < response.body().size(); i++){
                            values[i] = response.body().get(i).getUid();
                        }
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),
                                android.R.layout.simple_list_item_1, android.R.id.text1, values);
                        recyclerView.setAdapter(adapter);

                        recyclerView.setOnItemClickListener(ChatListFragment.this);

                    }

                    @Override
                    public void onFailure(Call<List<User>> call, Throwable t) {

                    }
                });

            }
        });
        */

        //Thread threads = StorageManager.shared().fetchThreadWithUsers(list);

        //new Verson todo


        threads = NM.thread().getThreads(ThreadType.Private1to1);
        if (threads == null || threads.isEmpty()){
            emptyList.setVisibility(View.VISIBLE);
        }else {
            emptyList.setVisibility(View.GONE);
        }

        threadsList = new ThreadAdapter(getContext(), threads);
        recyclerView.setAdapter(threadsList);

        threadsList.setClickListener((view1, position) -> {

            for (User user:  threads.get(position).getUsers()){
                if (!user.getEntityID().equals(NM.currentUser().getEntityID())){
                    Intent intent = new Intent(getActivity(), Chat2Activity.class);
                    intent.putExtra(ChatInteractor.ARG_RECEIVER_UID,user.getEntityID());
                    intent.putExtra("thread",threads.get(position).getEntityID());
                    startActivity(intent);
                    break;
                }
            }

        });









/*

        threadsList.setClickListener((view1, position) -> {

            Intent intent = new Intent(getActivity(), Chat2Activity.class);
            intent.putExtra(ChatInteractor.ARG_RECEIVER_UID, chatRoomList.get(position).getOtherUserId());
            intent.putExtra(ChatInteractor.ARG_RECEIVER_UID, chatRoomList.get(position).getOtherUserId());
            intent.putExtra(ChatInteractor.ARG_RECEIVER_UID, chatRoomList.get(position).getOtherUserId());
            startActivity(intent);
        });
*/
    }

    /*
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Intent intent = new Intent(getActivity(), Chat2Activity.class);
        intent.putExtra("receiver", values[position]);
        intent.putExtra("receiverUid", values[position]);
        intent.putExtra("firebaseToken", values[position]);
        startActivity(intent);
    }

    @Override
    public void onItemClick(View view, int position) {

        Intent intent = new Intent(getActivity(), Chat2Activity.class);
        intent.putExtra("receiver", values[position]);
        intent.putExtra("receiverUid", values[position]);
        intent.putExtra("firebaseToken", values[position]);
        startActivity(intent);
    }
    */


    @Override
    public void onDetach() {
        super.onDetach();
/*
        if (listnerData!= null) FirebaseDatabase.getInstance().getReference().removeEventListener(listnerData);
        chatroom = null;
        chatRoomList = null;
        recyclerView.destroyDrawingCache();
        recyclerView = null;

*/
    }

    @Override
    public void onStart() {
        super.onStart();

        if (!NM.auth().userAuthenticated()){
            NM.auth().authenticateWithCachedToken().subscribe(()->{});
        }

        disposableList.add(
                NM.events().sourceOnMain()
                        .filter(NetworkEvent.filterPrivateThreadsUpdated())
                        .subscribe(networkEvent -> {

                            Thread thread = networkEvent.thread;
                            if (thread != null) {

                                threadsList.setData(NM.thread().getThreads(ThreadType.Private1to1));

                                threadsList.notifyDataSetChanged();
                                if (threads != null || !threads.isEmpty()){
                                    emptyList.setVisibility(View.VISIBLE);
                                }

                            }

                        })
        );
    }

    @Override
    public void onResume() {
        super.onResume();

        if (!NM.auth().userAuthenticated()) NM.auth().authenticateWithCachedToken().subscribe(()->{}, err -> Log.e("chatList", err.getMessage()));




    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        for (Disposable disposable:disposableList){
            if (disposable != null) disposable.dispose();
        }
    }
}
