package com.shb.sa.shb.REST;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.GsonBuilder;
import com.shb.sa.shb.Application;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by mujtaba on 24/04/2017.
 */

public class ShbService {

    private Retrofit retrofit;
    private static ShbService service;

    public static ShbService getInstence(){
        if (service == null) service = new ShbService();

        return service;
    }

    private ShbService(){


        //.baseUrl("http://10.0.2.2:9000")
// .baseUrl("http://192.168.0.192:9000")
        retrofit = new Retrofit.Builder()
                .baseUrl("https://api.shb-app.com")
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().addDeserializationExclusionStrategy(new ExclusionStrategy() {
                    @Override
                    public boolean shouldSkipField(FieldAttributes f) {
                        return f.getName().toLowerCase().contains("created");
                    }

                    @Override
                    public boolean shouldSkipClass(Class<?> clazz) {
                        return false;
                    }
                }).create()))
                .client(Application.client)
                .build();
    }

    public static Retrofit getRetrofitRx(){

        //.baseUrl("http://10.0.2.2:9000")

        return new Retrofit.Builder()
                .baseUrl("https://api.shb-app.com")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory.create())
                .client(Application.client)
                .build();
    }

    public Retrofit getRetrofit() {
        return retrofit;
    }

    public void setRetrofit(Retrofit retrofit) {
        this.retrofit = retrofit;
    }
}
