package com.shb.sa.shb;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.shb.sa.shb.REST.models.Category;
import com.shb.sa.shb.model.citiy.City;
import com.shb.sa.shb.user.ExploreItemSectionFragment;
import com.shb.sa.shb.user.model.BarCodeWithCursor;

public class AdBrowserActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ad_browser);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        BarCodeWithCursor v;
        Category category= getIntent().getParcelableExtra("cat");
        getSupportActionBar().setTitle(category.getTitle());

        if (getIntent().getParcelableExtra("city") != null){
            City city = getIntent().getParcelableExtra("city");
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.frame_layout, ExploreItemSectionFragment.newInstance(category,city.getG().getAddressComponents().get(0).getLongName().toLowerCase().trim())).commit();
        } else {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.frame_layout, ExploreItemSectionFragment.newInstance(category,"")).commit();
        }

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
