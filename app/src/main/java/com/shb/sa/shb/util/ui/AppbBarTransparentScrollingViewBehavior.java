package com.shb.sa.shb.util.ui;

import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.view.View;

/**
 * Created by mujtaba on 11/12/2017.
 */

public class AppbBarTransparentScrollingViewBehavior extends AppBarLayout.ScrollingViewBehavior {

    @Override
    public boolean onDependentViewChanged(CoordinatorLayout parent, View child,
                                          View dependency) {
        updateOffset(parent, child, dependency);
        return false;
    }

    private boolean updateOffset(CoordinatorLayout parent, View child,
                                 View dependency) {
        final CoordinatorLayout.Behavior behavior = ((CoordinatorLayout.LayoutParams) dependency
                .getLayoutParams()).getBehavior();
        if (behavior instanceof AppBarLayout.Behavior) {
            // Offset the child so that it is below the app-bar (with any
            // overlap)
            final int offset = 0;   // CHANGED TO 0
            setTopAndBottomOffset(offset);
            return true;
        }
        return false;
    }
}
