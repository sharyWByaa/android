package com.shb.sa.shb.chat.logic;

import com.google.firebase.auth.FirebaseAuth;
import com.shb.sa.shb.REST.ShbService;
import com.shb.sa.shb.REST.UserService;

import org.jetbrains.annotations.NotNull;

import co.chatsdk.core.dao.Thread;
import co.chatsdk.core.dao.User;
import co.chatsdk.core.interfaces.ThreadType;
import co.chatsdk.core.session.NM;
import co.chatsdk.core.session.StorageManager;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mujtaba on 26/12/2017.
 */

public class ChatSDKExtention {


    public Single<Thread> get1to1Thread(@NotNull User mainUser, @NotNull User otherUser){

        String threadName1 = mainUser.getEntityID() + "_" + otherUser.getEntityID();
        String threadName2 =  otherUser.getEntityID() + "_" + mainUser.getEntityID();

        Thread thread1 = StorageManager.shared().fetchThreadWithEntityID(threadName1);
        Thread thread2 = StorageManager.shared().fetchThreadWithEntityID(threadName2);

        if (thread1 != null){
            return Single.just(thread1);
        } else if (thread2 != null){
            return Single.just(thread2);
        } else {
            Thread thread = StorageManager.shared().fetchOrCreateEntityWithEntityID(Thread.class, NM.currentUser().getEntityID() +"_"+otherUser.getEntityID());
            thread.setType(ThreadType.Private1to1);
            thread.addUsers(NM.currentUser(), otherUser);
            thread.setName("thread");
            thread.setCreator(NM.currentUser());
            thread.update();
            return NM.thread().pushThread(thread).subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .andThen(NM.thread().addUsersToThread(thread, mainUser, otherUser)).toSingle(() -> thread);
        }
    }


    //TODO create maybe

    public void getUserOrCreateUser(String uid){
        User user = co.chatsdk.core.session.StorageManager.shared().fetchUserWithEntityID(uid);


        if (user != null){
            return; // return user;
        }else {
            user = co.chatsdk.core.session.StorageManager.shared().fetchOrCreateEntityWithEntityID(User.class ,uid);
            User finalUser1 = user;
            FirebaseAuth.getInstance().getCurrentUser().getIdToken(true).addOnCompleteListener(task -> {

                if (task.isSuccessful()){
                    User finalUser = finalUser1;
                    ShbService.getInstence().getRetrofit().create(UserService.class).getUserInfo(task.getResult().getToken(), uid).enqueue(new Callback<com.shb.sa.shb.REST.models.User>() {
                        @Override
                        public void onResponse(Call<com.shb.sa.shb.REST.models.User> call, Response<com.shb.sa.shb.REST.models.User> response) {
                            if (response.isSuccessful()){

                                finalUser.setName(response.body().getName());
                            }
                        }

                        @Override
                        public void onFailure(Call<com.shb.sa.shb.REST.models.User> call, Throwable throwable) {

                        }
                    });
                }
            });

        }
    }


}
