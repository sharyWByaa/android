package com.shb.sa.shb.user.model;

import android.content.Context;
import android.support.v7.widget.SnapHelper;
import android.view.Gravity;

import com.airbnb.epoxy.Carousel.SnapHelperFactory;
import com.github.rubensousa.gravitysnaphelper.GravitySnapHelper;

/**
 * Created by mujtaba on 15/12/2017.
 */

abstract class SnapFactoryHelper extends SnapHelperFactory{

    public SnapHelper buildSnapHelper(Context context) {
        return new GravitySnapHelper(Gravity.START);
    }
}
