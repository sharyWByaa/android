package com.shb.sa.shb.user;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GetTokenResult;
import com.google.gson.JsonObject;
import com.shb.sa.shb.R;
import com.shb.sa.shb.REST.ShbService;
import com.shb.sa.shb.REST.UserService;
import com.shb.sa.shb.REST.models.Ad;

import java.math.BigDecimal;

import br.com.simplepass.loading_button_lib.customViews.CircularProgressButton;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditAdActivity extends AppCompatActivity {

    private Ad ad;

    @BindView(R.id.edit_ad_title)
    EditText title;

    @BindView(R.id.edit_ad_desc)
    EditText desc;

    @BindView(R.id.edit_ad_price)
    EditText price;

    @BindView(R.id.edit_ad_update_btn)
    CircularProgressButton updateAd;

    @BindView(R.id.delete_btn)
    CircularProgressButton deleteBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_ad);
        setSupportActionBar(findViewById(R.id.toolbar));
        ButterKnife.bind(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        ad = (Ad)getIntent().getSerializableExtra("ad");

        getSupportActionBar().setTitle(ad.getTitle());

        title.setText(ad.getTitle());
        desc.setText(ad.getDesc());
        price.setText(String.valueOf(ad.getPrice()));

        updateAd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                updateAd.startAnimation();
                FirebaseAuth mAuth = FirebaseAuth.getInstance();
                if (mAuth.getCurrentUser() != null) {
                    mAuth.getCurrentUser().getToken(true).addOnSuccessListener(getTokenResult -> {

                        ad.setTitle(title.getText().toString());
                        ad.setDesc(desc.getText().toString());
                        ad.setPrice(BigDecimal.valueOf(Double.valueOf(price.getText().toString())));
                        ShbService.getInstence().getRetrofit().create(UserService.class)
                                .updateAd(getTokenResult.getToken(), ad.getId(), ad).enqueue(new Callback<Ad>() {
                            @Override
                            public void onResponse(Call<Ad> call, Response<Ad> response) {

                                if (response.isSuccessful()){
                                    updateAd.revertAnimation();
                                    ad = response.body();
                                    title.setText(ad.getTitle());
                                    desc.setText(ad.getDesc());
                                    price.setText(String.valueOf(ad.getPrice()));

                                    Toast.makeText(EditAdActivity.this, "it has been updated", Toast.LENGTH_LONG).show();
                                }else {
                                    updateAd.revertAnimation();
                                }
                            }

                            @Override
                            public void onFailure(Call<Ad> call, Throwable throwable) {

                                updateAd.revertAnimation();
                            }
                        });
                    });
                }else {
                    updateAd.revertAnimation();
                }
            }
        });


        findViewById(R.id.delete_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                new AlertDialog.Builder(EditAdActivity.this).setMessage("Are you sure you want delete the ad")
                        .setPositiveButton("yes", (dialogInterface, i) -> {

                            deleteBtn.startAnimation();
                            FirebaseAuth mAuth = FirebaseAuth.getInstance();
                            if (mAuth.getCurrentUser() != null) {
                                mAuth.getCurrentUser().getToken(true).addOnSuccessListener(new OnSuccessListener<GetTokenResult>() {
                                    @Override
                                    public void onSuccess(GetTokenResult getTokenResult) {


                                        //Toast.makeText(EditAdActivity.this, String.valueOf(ad.getId()), Toast.LENGTH_SHORT).show();
                                        ShbService.getInstence().getRetrofit().create(UserService.class).deleteAd(getTokenResult.getToken(), ad.getId())
                                                .enqueue(new Callback<JsonObject>() {
                                                    @Override
                                                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                                                        deleteBtn.revertAnimation();
                                                        finish();
                                                    }

                                                    @Override
                                                    public void onFailure(Call<JsonObject> call, Throwable t) {

                                                        deleteBtn.revertAnimation();
                                                    }
                                                });
                                    }
                                });
                            } else {
                                deleteBtn.revertAnimation();
                            }
                        }).setNegativeButton("no", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {



                            }
                        }).create().show();



            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;

    }
}
