package com.shb.sa.shb.model.citiy;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mujtaba on 23/01/2018.
 */

public class GooglePlaceInfo implements Parcelable {


    @SerializedName("address_components")
    List<AddressComponent> addressComponents;

    @SerializedName("place_id")
    String placeId;

    public List<AddressComponent> getAddressComponents() {
        return addressComponents;
    }

    public void setAddressComponents(List<AddressComponent> addressComponents) {
        this.addressComponents = addressComponents;
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }


    protected GooglePlaceInfo(Parcel in) {
        if (in.readByte() == 0x01) {
            addressComponents = new ArrayList<AddressComponent>();
            in.readList(addressComponents, AddressComponent.class.getClassLoader());
        } else {
            addressComponents = null;
        }
        placeId = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (addressComponents == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(addressComponents);
        }
        dest.writeString(placeId);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<GooglePlaceInfo> CREATOR = new Parcelable.Creator<GooglePlaceInfo>() {
        @Override
        public GooglePlaceInfo createFromParcel(Parcel in) {
            return new GooglePlaceInfo(in);
        }

        @Override
        public GooglePlaceInfo[] newArray(int size) {
            return new GooglePlaceInfo[size];
        }
    };
}