package com.shb.sa.shb.user.model;

import android.net.Uri;

import com.airbnb.epoxy.EpoxyAttribute;
import com.airbnb.epoxy.EpoxyModelClass;
import com.airbnb.epoxy.EpoxyModelWithHolder;
import com.facebook.drawee.view.SimpleDraweeView;
import com.shb.sa.shb.R;

import butterknife.BindView;

/**
 * Created by mujtaba on 20/06/2017.
 */
@EpoxyModelClass(layout = R.layout.model_preview_image)
public abstract class PreviewImageModel extends EpoxyModelWithHolder<PreviewImageModel.ImageHolder> {
    @EpoxyAttribute
    Uri imageRes;

    //@EpoxyAttribute(DoNotHash)
    //View.OnClickListener clickListener;


    @Override
    public void bind(ImageHolder holder) {
        holder.image.setImageURI(imageRes);
        //holder.removeImage.setOnClickListener(clickListener);


    }

    @Override
    public void unbind(ImageHolder holder) {
        super.unbind(holder);
        holder.image.setImageURI((Uri) null);
        //holder.removeImage.setOnClickListener(null);
    }



    public static class ImageHolder extends BaseEpoxyHolder {
        @BindView(R.id.image_img)
        SimpleDraweeView image;

        //@BindView(R.id.remove_image)
        //TextView removeImage;
    }
}
