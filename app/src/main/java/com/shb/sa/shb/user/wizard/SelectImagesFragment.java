package com.shb.sa.shb.user.wizard;


import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.anupcowkur.reservoir.Reservoir;
import com.anupcowkur.reservoir.ReservoirGetCallback;
import com.anupcowkur.reservoir.ReservoirPutCallback;
import com.google.gson.reflect.TypeToken;
import com.shb.sa.shb.Application;
import com.shb.sa.shb.R;
import com.shb.sa.shb.event.MediaEditingMessage;
import com.shb.sa.shb.model.Media;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;
import com.zhihu.matisse.Matisse;
import com.zhihu.matisse.MimeType;
import com.zhihu.matisse.engine.impl.PicassoEngine;
import com.zhihu.matisse.internal.entity.CaptureStrategy;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.RuntimePermissions;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;


import static android.app.Activity.RESULT_OK;
import static com.shb.sa.shb.CameraViewFragment.CACHE_MEDIA;
import static com.shb.sa.shb.CameraViewFragment.CACHE_VIDEO_PICTURES;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SelectImagesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
@RuntimePermissions
public class SelectImagesFragment extends Fragment  implements SelectImageController.AdapterCallbacks, Step {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final int SPAN_COUNT = 3;
    private static final int REQUEST_CODE_CHOOSE = 4312;
    private static final String IMAGE_DATA_KEY = "images" ;
    public static final String CACHE_IMAGES = "cacheimages";

    // TODO: Rename and change types of parameters
    private ArrayList<Media> mParam1;
    private ArrayList<Media> mediaArrayList;
    private View view;
    private RecyclerView list;
    private final RecyclerView.RecycledViewPool recycledViewPool = new RecyclerView.RecycledViewPool();
    private final SelectImageController controller = new SelectImageController(this);
    private List<Uri> mSelected;
    private ArrayList<Uri> images = new ArrayList<>();
    private HashSet<Uri> imagesUri = new HashSet<>();
    private Set<Uri> mediaPath = new LinkedHashSet<>();
    private MediaSelection mListener;


    public SelectImagesFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SelectImagesFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SelectImagesFragment newInstance(ArrayList<Media> param1, ArrayList<Media> param2) {
        SelectImagesFragment fragment = new SelectImagesFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(ARG_PARAM1, param1);
        args.putParcelableArrayList(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getParcelableArrayList(ARG_PARAM1);
            mediaArrayList = getArguments().getParcelableArrayList(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        controller.onRestoreInstanceState(savedInstanceState);

        view = inflater.inflate(R.layout.fragment_select_images, container, false);


        Reservoir.getAsync("helloWorld", String.class, new ReservoirGetCallback<String>() {
            @Override
            public void onSuccess(String s) {

            }

            @Override
            public void onFailure(Exception e) {

            }
        });

        if (view instanceof RecyclerView){
           list = (RecyclerView) view;

            list.setRecycledViewPool(recycledViewPool);
            controller.setSpanCount(SPAN_COUNT);
            list.setHasFixedSize(true);
            GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), SPAN_COUNT);
            gridLayoutManager.setSpanSizeLookup(controller.getSpanSizeLookup());
            list.setLayoutManager(gridLayoutManager);
            list.setAdapter(controller.getAdapter());
            prepareMedia();
            // get saved images if there is no images
            if (savedInstanceState != null) {
                imagesUri = new HashSet<>(Arrays.asList((Uri[])savedInstanceState.getParcelableArray(IMAGE_DATA_KEY)));
                addImage();
                controller.setData(imagesUri, null);
            } else {
                Type resultType = new TypeToken<Uri[]>() {}.getType();
                Reservoir.getAsync(CACHE_IMAGES, resultType, new ReservoirGetCallback<Uri[]>() {
                    @Override
                    public void onSuccess(Uri[] myObject) {
                        HashSet<Uri> uriHashSet = new HashSet<>();

                        for (Uri uri: myObject){

                            uriHashSet.add(uri);
                        }

                        imagesUri = uriHashSet;
                        addImage();
                        controller.setData(imagesUri, Arrays.asList(mediaPath.toArray(new Uri[mediaPath.size()])));
                        //success
                        //imagesUri = myObject;
                        //controller.setData(imagesUri);
                       //Toast.makeText(getContext(), " "+ myObject.length, Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Exception e) {
                        //error
                    }
                });

                controller.setData(imagesUri, Arrays.asList(mediaPath.toArray(new Uri[mediaPath.size()])));
                addImage();
            }



        }

        return view;
    }

    private void addImage(){

        if (imagesUri == null) return;

        for (Media media: mParam1){

            imagesUri.add(media.getThumnailKeyUri());
        }

    }
    private void prepareMedia(){

        Observable.defer(()->{

            com.annimon.stream.Stream.of(mediaArrayList)
                    .filter(media -> Application.getInstance(getContext()).get(media.getThumnailKey()).isPresent())
                    .forEach(media -> mediaPath.add(Uri.parse("file://" + Application.getInstance(getContext()).get(media.getThumnailKey()).get().getPath())));
            return Observable.just(mediaPath);
        }).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
        .subscribe(list ->{
            mediaPath = list;
            controller.setData(imagesUri, Arrays.asList(mediaPath.toArray(new Uri[mediaPath.size()])));

        });
    }

    @NeedsPermission({Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE})
    void showGallery(){


        Matisse.from(this)
                .choose(MimeType.allOf())
                .countable(true)
                .capture(true)
                .captureStrategy(
                new CaptureStrategy(true, "com.shb.sa.shwb.fileprovider"))
                .maxSelectable(9 - imagesUri.size())
                .restrictOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
                .thumbnailScale(0.85f)
                .imageEngine(new PicassoEngine()).forResult(REQUEST_CODE_CHOOSE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Toast.makeText(getContext(), "Got Images", Toast.LENGTH_LONG).show();
        if (requestCode == REQUEST_CODE_CHOOSE && resultCode == RESULT_OK) {
            mSelected = Matisse.obtainResult(data);
            imagesUri.addAll(mSelected);
            controller.setData(imagesUri, Arrays.asList(mediaPath.toArray(new Uri[mediaPath.size()])));
            cacheImages();

        }


        Reservoir.putAsync("helloWorld", "Hello World", new ReservoirPutCallback() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onFailure(Exception e) {

            }
        });
    }

    @Override
    public void addImages() {
        SelectImagesFragmentPermissionsDispatcher.showGalleryWithCheck(this);
    }

    @Override
    public void deleteImage(Uri uri) {
        imagesUri.remove(uri);
        controller.setData(imagesUri, Arrays.asList(mediaPath.toArray(new Uri[mediaPath.size()])));
        deleteMediabyUri(uri);



    }

    @Override
    public void deleteVideo(Uri uri) {

        mediaPath.remove(uri);
        for(int i = 0; i < mediaArrayList.size(); i++){

            if (Application.getInstance(getContext()).get(mediaArrayList.get(i).getThumnailKey()).isPresent()) {
                Uri toCompare = Uri.parse("file://" + Application.getInstance(getContext()).get(mediaArrayList.get(i).getThumnailKey()).get().getPath());
                if (uri.equals(toCompare)) {
                    deleteMediabyUri(mediaArrayList.get(i).getThumnailKeyUri());
                    mediaArrayList.remove(i);
                    mediaPath.remove(uri);
                }
            }
        }
        controller.setData(imagesUri, Arrays.asList(mediaPath.toArray(new Uri[mediaPath.size()])));

        // send messages
        MediaEditingMessage mediaEditingMessage = new MediaEditingMessage();
        mediaEditingMessage.mediaList = mediaArrayList;
        EventBus.getDefault().post(mediaEditingMessage);

        Reservoir.putAsync(CACHE_MEDIA, mediaArrayList, new ReservoirPutCallback() {
            @Override
            public void onSuccess() {
                //success
            }

            @Override
            public void onFailure(Exception e) {
                //error
            }
        });
    }


    void deleteMediabyUri(Uri uri){

        Observable.defer(() -> {

            Type resultType = new TypeToken<List<Media>>() {}.getType();
            try {
                List<Media> medias = Reservoir.get(CACHE_VIDEO_PICTURES, resultType);
                for (int i = 0; i < medias.size(); i++){

                    if (medias.get(i).getThumnailKeyUri().equals(uri)){

                        medias.remove(i);
                        Reservoir.put(CACHE_VIDEO_PICTURES, medias);
                        Log.d("mushy", "equalss");
                        break;
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return Observable.just("");
        })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
        .subscribe((result) -> {
            Log.d("mushy", "HHHHH" + uri.toString());
        });



    }


    @Override
    public VerificationError verifyStep() {

        if (mediaArrayList != null && mediaArrayList.isEmpty()) return new VerificationError("NO VIDEO");
        mListener.selectedImages(imagesUri);
        mListener.selectedMedia(mediaArrayList);
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

        Toast.makeText(getContext(), R.string.err_no_video, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onSaveInstanceState(Bundle state) {
        super.onSaveInstanceState(state);
        state.putParcelableArray(IMAGE_DATA_KEY, imagesUri.toArray(new Uri[imagesUri.size()]));
        controller.onSaveInstanceState(state);



    }

    private void cacheImages(){

        Reservoir.putAsync(CACHE_IMAGES, imagesUri.toArray(new Uri[imagesUri.size()]), new ReservoirPutCallback() {
            @Override
            public void onSuccess() {
                //success
                Log.w("Mushy", "nice");
            }

            @Override
            public void onFailure(Exception e) {
                //error
                Log.w("Mushy", e.getLocalizedMessage());
            }
        });

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof MediaSelection) {
            mListener = (MediaSelection) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface MediaSelection{
        void selectedImages(HashSet<Uri> images);
        void selectedMedia(ArrayList<Media> mediaArrayList);
    }
}
