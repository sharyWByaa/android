package com.shb.sa.shb.model;

import android.graphics.drawable.Drawable;

/**
 * Created by mujtaba on 30/04/2017.
 */

public class ProfileMenu {

    private String title;
    private Drawable drawable;
    private MyListener listener;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Drawable getDrawable() {
        return drawable;
    }

    public void setDrawable(Drawable drawable) {
        this.drawable = drawable;
    }

    public void setListener(MyListener listener){
        this.listener = listener;
    }

    public MyListener getListener(){
        return this.listener;
    }

    public interface MyListener {

        void callback();
    }


}
