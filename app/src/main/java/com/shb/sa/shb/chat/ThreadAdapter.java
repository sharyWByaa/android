package com.shb.sa.shb.chat;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.shb.sa.shb.R;

import org.ocpsoft.prettytime.PrettyTime;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.chatsdk.core.dao.Thread;
import co.chatsdk.core.dao.User;
import co.chatsdk.core.session.NM;

public class ThreadAdapter extends RecyclerView.Adapter<ThreadAdapter.ViewHolder> {

        private List<Thread> mData = Collections.emptyList();
        private LayoutInflater mInflater;
        private ItemClickListener mClickListener;
        final PrettyTime p =new PrettyTime();

        // data is passed into the constructor
        public ThreadAdapter(Context context, List<Thread> data) {
            this.mInflater = LayoutInflater.from(context);
            this.mData = data;


        }

        public void setData(List<Thread> mData){
            this.mData = mData;
            notifyDataSetChanged();
        }

        // inflates the row layout from xml when needed
        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = mInflater.inflate(R.layout.item_chat_list, parent, false);
            ViewHolder viewHolder = new ViewHolder(view);
            return viewHolder;
        }



    // binds the data to the textview in each row
        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            Thread animal = mData.get(position);



            for (User user: animal.getUsers()){


                if (!user.getEntityID().equals(NM.currentUser().getEntityID())) {
                    holder.myTextView.setText(user.getName());

                }
            }

                if (animal.getLastMessage() != null) holder.lastMessageTextView.setText(animal.getLastMessage().getTextString());
                //holder.avatar.setImageURI(Application.baseStoreageUrl + animal.getOtherUserId() + "/profile_avatar/avatar.jpeg");
            if (animal.getLastMessageAddedDate() != null) holder.timeStampTextView.setText(p.format(animal.getLastMessageAddedDate()));
                if (animal.isLastMessageWasRead()) holder.newMessage.setVisibility(View.INVISIBLE);
                else holder.newMessage.setVisibility(View.VISIBLE);



        }

        // total number of rows
        @Override
        public int getItemCount() {
            return mData.size();
        }


        // stores and recycles views as they are scrolled off screen
        public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            @BindView(R.id.item_chat_list_text)
            public TextView myTextView;
            @BindView(R.id.item_chat_list_last_message_text)
            public TextView lastMessageTextView;
            @BindView(R.id.item_chat_list_timestamp)
            public TextView timeStampTextView;

            @BindView(R.id.item_chat_list_avatar)
            public SimpleDraweeView avatar;

            @BindView(R.id.item_chat_notification)
            TextView newMessage;

            public ViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);

                itemView.setOnClickListener(this);


            }

            @Override
            public void onClick(View view) {
                if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
            }
        }

        // convenience method for getting data at click position
        public Thread getItem(int id) {
            return mData.get(id);
        }

        // allows clicks events to be caught
        public void setClickListener(ItemClickListener itemClickListener) {
            this.mClickListener = itemClickListener;
        }

        // parent activity will implement this method to respond to click events
        public interface ItemClickListener {
            void onItemClick(View view, int position);
        }
    }