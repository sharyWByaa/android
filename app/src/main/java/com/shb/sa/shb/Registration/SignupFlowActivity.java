package com.shb.sa.shb.Registration;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;


import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.gson.JsonObject;
import com.marcinorlowski.fonty.Fonty;
import com.shb.sa.shb.R;
import com.shb.sa.shb.REST.ShbService;
import com.shb.sa.shb.REST.UserService;
import com.shb.sa.shb.REST.models.User;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.adapter.AbstractFragmentStepAdapter;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SignupFlowActivity extends AppCompatActivity implements  MobileVerificationFragment.OnFragmentInteractionListener
        ,PhoneLoginFragment.OnFragmentInteractionListener, CodeVarifcationFragment.OnFragmentInteractionListener, FirebaseSignupFragment.OnFragmentInteractionListener{

  private static final String TAG = "SignupFlow";
  private StepperLayout mPager;

  private String mobilePhoneNo;


  private ScreenSlidePagerAdapter mPagerAdapter;

  private List<Step> pages;
  private String mVerificationId;
  private PhoneAuthProvider.ForceResendingToken mResendToken;
  private FirebaseUser user;
  private FirebaseUser phoneUser;
  @BindView(R.id.signup_progressbar) ProgressBar progressBar;
    private boolean isPosting;
  private boolean isSubmitingVerificationCode = false;
  private String phoneNo;


  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    pages = new ArrayList<>();
    setContentView(R.layout.activity_signup_flow);
    ButterKnife.bind(this);

    Toolbar toolbar = findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    getSupportActionBar().setDisplayShowHomeEnabled(true);

    toolbar.setNavigationIcon(R.drawable.ic_clear_black_24dp);

    mPager = (StepperLayout) findViewById(R.id.stepperLayout);

        /*
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);

        Digits.Builder digitsBuilder = new Digits.Builder().withTheme(R.style.CustomDigitsTheme);
        fabric = Fabric.with(this, new TwitterCore(authConfig), digitsBuilder.build());







        mPager = (ViewPager) findViewById(R.id.pager);

        mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(mPagerAdapter);



/*

        final DigitsAuthButton digitBtn = (DigitsAuthButton) findViewById(R.id.auth_button);

        digitBtn.setCallback(new AuthCallback() {
            @Override
            public void success(DigitsSession session, String phoneNumber) {
                // TODO: associate the session userID with your user model
                Toast.makeText(getApplicationContext(), "Authentication successful for "
                        + phoneNumber, Toast.LENGTH_LONG).show();
                //Intent intent = new Intent(getContext(), SignupFlowActivity.class);
                //intent.putExtra("phoneNumber", phoneNumber);
                //startActivity(intent);

                mobilePhoneNo = phoneNumber;


            }

            @Override
            public void failure(DigitsException exception) {
                Log.d("Digits", "Sign in with Digits failure", exception);
            }
        });
        */

    //digitBtn.performClick();






    //mobilePhoneNo = getIntent().getStringExtra("phoneNumber");



    //if(mobilePhoneNo == null){

    // }




    //mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
    //mPager.setAdapter(mPagerAdapter);


    pages.add(new PhoneLoginFragment());
    pages.add(new CodeVarifcationFragment());


    mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager(), this);
    mPager.setAdapter(mPagerAdapter);

    pages.add(FirebaseSignupFragment.newInstance("234324", "2342342342"));
    mPagerAdapter.notifyDataSetChanged();



    Fonty.setFonts(this);
  }

    @Override
    public boolean onSupportNavigateUp() {

        onBackPressed();
        startActivity(new Intent(this, LoginActivity.class));
        return true;

    }

    @Override
  public void onBackPressed() {
    if (mPager.getCurrentStepPosition() == 0) {
      // If the user is currently looking at the first step, allow the system to handle the
      // Back button. This calls finish() on this activity and pops the back stack.
      super.onBackPressed();
    } else {
      // Otherwise, select the previous step.
      mPager.setCurrentStepPosition(mPager.getCurrentStepPosition() - 1);
    }
  }

  @Override
  public void onFragmentInteraction(String phonenumber) {

    this.mobilePhoneNo = phonenumber;
    pages.add(FirebaseSignupFragment.newInstance(mobilePhoneNo,""));
    mPagerAdapter.notifyDataSetChanged();
    mPager.setCurrentStepPosition(1);
  }

  @Override
  public void onFragmentInteraction(String mVerificationId, PhoneAuthProvider.ForceResendingToken mResendToken) {
    this.mVerificationId = mVerificationId;
    this.mResendToken = mResendToken;
    pages.add(1,CodeVarifcationFragment.newInstance(mVerificationId, ""));
    mPagerAdapter.notifyDataSetChanged();

  }

  @Override
  public void verifyPhoneNo(String phoneNo,EditText editText, StepperLayout.OnNextClickedCallback callback) {

    progressBar.setVisibility(View.VISIBLE);

    this.phoneNo = phoneNo;
    PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

      @Override
      public void onVerificationCompleted(PhoneAuthCredential credential) {
        // This callback will be invoked in two situations:
        // 1 - Instant verification. In some cases the phone number can be instantly
        //     verified without needing to send or enter a verification code.
        // 2 - Auto-retrieval. On some devices Google Play services can automatically
        //     detect the incoming verification SMS and perform verificaiton without
        //     user action.
        Log.d(TAG, "onVerificationCompleted:" + credential);
        progressBar.setVisibility(View.INVISIBLE);




        FirebaseAuth.getInstance().signInWithCredential(credential).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
          @Override
          public void onComplete(@NonNull Task<AuthResult> task) {

            if (task.isSuccessful()){
              phoneUser = task.getResult().getUser();
            }
          }
        });
        mPager.setCurrentStepPosition(2);

        callback.goToNextStep();

        //signInWithPhoneAuthCredential(credential);
      }

      @Override
      public void onVerificationFailed(FirebaseException e) {
        // This callback is invoked in an invalid request for verification is made,
        // for instance if the the phone number format is not valid.
        Log.w(TAG, "onVerificationFailed", e);
        progressBar.setVisibility(View.INVISIBLE);

        if (e instanceof FirebaseAuthInvalidCredentialsException) {
          // Invalid request
          Toast.makeText(SignupFlowActivity.this, e.getMessage().toLowerCase() +" "+ ((FirebaseAuthInvalidCredentialsException) e).getErrorCode() , Toast.LENGTH_LONG).show();

          if (((FirebaseAuthInvalidCredentialsException) e).getErrorCode().equalsIgnoreCase("ERROR_INVALID_PHONE_NUMBER")){
            editText.setError("Error Invalid_phone_number");
          }


          // ...
        } else if (e instanceof FirebaseTooManyRequestsException) {
          // The SMS quota for the project has been exceeded
          // ...
        }

        // Show a message and update the UI
        // ...
      }

      @Override
      public void onCodeSent(String verificationId,
                             PhoneAuthProvider.ForceResendingToken token) {
        // The SMS verification code has been sent to the provided phone number, we
        // now need to ask the user to enter the code and then construct a credential
        // by combining the code with a verification ID.
        Log.d(TAG, "onCodeSent:" + verificationId);

        // Save verification ID and resending token so we can use them later
        mVerificationId = verificationId;
        mResendToken = token;

        progressBar.setVisibility(View.INVISIBLE);

        new CountDownTimer(60000, 1000) {
          @Override
          public void onTick(long l) {
            EventBus.getDefault().post(new CodeExpUpdate(l));
          }

          @Override
          public void onFinish() {
            EventBus.getDefault().post(new CodeExpUpdate(0));
          }
        }.start();

        callback.goToNextStep();
        callback.getStepperLayout().hideProgress();


        // ...
      }
    };






    ShbService.getInstence().getRetrofit().create(UserService.class).canThisPhoneNumberRegistere(phoneNo.replaceFirst("00","+")).enqueue(new Callback<ResponseBody>() {
      @Override
      public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
        if (response.isSuccessful()){

          PhoneAuthProvider.getInstance().verifyPhoneNumber(
                  phoneNo,        // Phone number to verify
                  60,                 // Timeout duration
                  TimeUnit.SECONDS,   // Unit of timeout
                  SignupFlowActivity.this,               // Activity (for callback binding)
                  mCallbacks);        // OnVerificationStateChangedCallbacks

        }else {
            progressBar.setVisibility(View.INVISIBLE);
            editText.setError("Phone number already exist");
            Toast.makeText(getBaseContext(), "Phone number exist", Toast.LENGTH_SHORT).show();
        }
      }

      @Override
      public void onFailure(Call<ResponseBody> call, Throwable throwable) {

          progressBar.setVisibility(View.INVISIBLE);
          Toast.makeText(getBaseContext(), "Phone  failed", Toast.LENGTH_SHORT).show();
      }
    });
  }

  @Override
  public void onCodeSubmitted(String code, EditText editText) {


    if (isSubmitingVerificationCode) return; // ignore multiple call

    isSubmitingVerificationCode = true;
    PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, code.trim());

    Log.d("mushy", "cooode " + code.trim() );
    progressBar.setVisibility(View.VISIBLE);
    FirebaseAuth.getInstance().signInWithCredential(credential)
            .addOnCompleteListener(SignupFlowActivity.this, new OnCompleteListener<AuthResult>() {
              @Override
              public void onComplete(@NonNull Task<AuthResult> task) {
                progressBar.setVisibility(View.INVISIBLE);
                if (task.isSuccessful()) {
                  // Sign in success, update UI with the signed-in user's information
                  Log.d(TAG, "signInWithCredential:success");

                  FirebaseUser user = task.getResult().getUser();
                 // Toast.makeText(SignupFlowActivity.this, "Great", Toast.LENGTH_SHORT).show();

                  phoneUser = user;




                    mPager.proceed();

                  if (mPager.getCurrentStepPosition() + 1 < mPagerAdapter.getCount())
                    mPager.setCurrentStepPosition(mPager.getCurrentStepPosition() + 1);

                    mPager.hideProgress();
                  isSubmitingVerificationCode = false;


                  // ...
                } else {
                  isSubmitingVerificationCode = false;
                  // Sign in failed, display a message and update the UI
                  Log.w(TAG, "signInWithCredential:failure", task.getException());
                  if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                    // The verification code entered was invalid
                    editText.setError("verification code was invalied");
                  }
                }
              }
            });
  }

  @Override
  public void resendPinNo() {

    PhoneAuthProvider.getInstance().verifyPhoneNumber(
            phoneNo,        // Phone number to verify
            60,                 // Timeout duration
            TimeUnit.SECONDS,   // Unit of timeout
            SignupFlowActivity.this,               // Activity (for callback binding)
            new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
              @Override
              public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                super.onCodeSent(s, forceResendingToken);
                Toast.makeText(SignupFlowActivity.this, "Code has been resend",Toast.LENGTH_SHORT).show();

                new CountDownTimer(60000, 1000) {
                  @Override
                  public void onTick(long l) {
                    EventBus.getDefault().post(new CodeExpUpdate(l));
                  }

                  @Override
                  public void onFinish() {
                    EventBus.getDefault().post(new CodeExpUpdate(0));
                  }
                }.start();

                mVerificationId = s;
                mResendToken = forceResendingToken;

              }

              @Override
              public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {

              }

              @Override
              public void onVerificationFailed(FirebaseException e) {

              }
            }, mResendToken);        // OnVerificationStateChangedCallbacks


  }


  void linkAccount(FirebaseUser phoneUser, String email, String password, String username){
      if (isPosting) return; // prevent double click

    progressBar.setVisibility(View.VISIBLE);
      isPosting = true;

    // prepare user
    final User userData = new User();
    //userData.setUid(task.getResult().getUser().getUid());
    userData.setPhoneNumber(phoneUser.getPhoneNumber());
    userData.setUsername(username);
    userData.fireBaseUsername = email;
    userData.setPassword(password);
    userData.setEmail(email);



    ShbService.getInstence().getRetrofit().create(UserService.class).createUser(userData).enqueue(new Callback<JsonObject>() {
      @Override
      public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {


        if (response.isSuccessful()) {

          String token = response.body().get("token").getAsString();

            isPosting = false;
            FirebaseAuth.getInstance().signOut();

            new AlertDialog.Builder(SignupFlowActivity.this)
                    .setMessage(R.string.msg_registeration_succ)
                    .setNeutralButton("ok", (dialogInterface, i) -> {


                      finish();
                      Intent intent = new Intent(SignupFlowActivity.this, LoginActivity.class);
                      startActivity(intent);
                    }).create().show();


          /*

          //Task<AuthResult> emailUserTask = FirebaseAuth.getInstance().signInWithEmailAndPassword(email, password);
          Task<AuthResult> tokenUserTask = FirebaseAuth.getInstance().signInWithCustomToken(token);

          Tasks.whenAll(tokenUserTask).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
              if (task.isSuccessful()){

                tokenUserTask.getResult().getUser().linkWithCredential(EmailAuthProvider.getCredential(email,password))
                        .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                          @Override
                          public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()){
                              finish();
                              Toast.makeText(SignupFlowActivity.this, "Ywahoo", Toast.LENGTH_LONG).show();
                            } else {

                              Toast.makeText(SignupFlowActivity.this, task.getException().getMessage(), Toast.LENGTH_LONG).show();
                            }
                          }
                        });
              } else {
                Log.d("mushy", task.getException().getMessage());
              }
            }
          });
          */

        }else {
          // Error
          progressBar.setVisibility(View.INVISIBLE);
            isPosting = false;
          //TODO show message
          try {
            Toast.makeText(SignupFlowActivity.this, "ERROR" + response.errorBody().string(), Toast.LENGTH_LONG).show();
          } catch (IOException e) {
            e.printStackTrace();
          }

          try {
            Log.d("Mushy", response.errorBody().string());
          } catch (IOException e) {
            e.printStackTrace();
          }

        }

        //finish();
      }

      @Override
      public void onFailure(Call<JsonObject> call, Throwable t) {

        progressBar.setVisibility(View.INVISIBLE);
      }
    });




  }

  @Override
  public void onEmailSubmited(String email, String password, String name, StepperLayout.OnCompleteClickedCallback callback) {


    Toast.makeText(SignupFlowActivity.this, "Email submitted",
            Toast.LENGTH_SHORT).show();
    linkAccount(phoneUser, email, password, name);


    /*
    FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
              @Override
              public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                  // Sign in success, update UI with the signed-in user's information
                  Toast.makeText(SignupFlowActivity.this, "Authentication Succ.",
                          Toast.LENGTH_SHORT).show();
                  Log.d(TAG, "createUserWithEmail:success");
                  FirebaseUser user = task.getResult().getUser();
                  ///updateUI(user)
                  ///;
                  linkAccount(phoneUser, email, password);
                } else {
                  // If sign in fails, display a message to the user.
                  Log.w(TAG, "createUserWithEmail:failure", task.getException());
                  Toast.makeText(SignupFlowActivity.this, "Authentication failed.",
                          Toast.LENGTH_SHORT).show();
                  //updateUI(null);
                }

                // ...
              }
            });
            */
  }


  /**
   * A simple pager adapter that represents 5 ScreenSlidePageFragment objects, in
   * sequence.
   */
  private class ScreenSlidePagerAdapter  extends AbstractFragmentStepAdapter {


    public ScreenSlidePagerAdapter(@NonNull FragmentManager fm, @NonNull Context context) {
      super(fm, context);
    }



    @Override
    public Step createStep(@IntRange(from = 0L) int position) {
      return pages.get(position);

    }

    @Override
    public int getCount() {
      return pages.size();
    }
  }


  /**
   * Event update the fragment for when the code will expired
   * */
  public class CodeExpUpdate{

    public long getToExp() {
      return toExp;
    }

    public void setToExp(long toExp) {
      this.toExp = toExp;
    }

    private long toExp;

    public CodeExpUpdate(long toExp) {
      this.toExp = toExp;
    }



  }
}




