package com.shb.sa.shb.user.model;

import com.airbnb.epoxy.TypedEpoxyController;
import com.shb.sa.shb.REST.models.Ad;

import java.util.List;

/**
 * Created by mujtaba on 24/08/2017.
 */

public class MyAdController  extends TypedEpoxyController<List<Ad>> {

    private final MyAdListiner myAdListiner;

    public MyAdController(MyAdListiner myAdListiner){

        this.myAdListiner = myAdListiner;
    }

    @Override
    protected void buildModels(List<Ad> data) {

        for (Ad ad: data){
            add(new AdItemModel_()
                    .id(ad.getId())
                    .title(ad.getTitle())
                    .subtitle(String.valueOf(ad.getPrice().doubleValue()))
                    .username(ad.getUser().getUsername())
                    .screenshotPath(ad.getMedias().get(0).getThumbnail())
                    .onClickListener((model, parentView, clickedView, position) -> {
                        myAdListiner.onItemClick(position);
                    })
            );
        }

    }


    public interface MyAdListiner{
        void onItemClick(int pos);
    }
}
