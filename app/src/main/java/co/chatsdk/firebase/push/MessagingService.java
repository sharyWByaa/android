package co.chatsdk.firebase.push;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.joda.time.DateTime;

import co.chatsdk.core.dao.Message;
import co.chatsdk.core.dao.Thread;
import co.chatsdk.core.dao.User;
import co.chatsdk.core.session.StorageManager;
import co.chatsdk.core.types.MessageType;
import timber.log.Timber;

/**
 * Created by ben on 9/1/17.
 */

public class MessagingService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {



        Timber.v("Received push");




        String title = remoteMessage.getData().get("title");
        String message = remoteMessage.getData().get("text");
        String mesBody = remoteMessage.getData().get("body");
        String username = remoteMessage.getData().get("chat_sdk_user_entity_id");
        String uid = remoteMessage.getData().get("chat_sdk_thread_entity_id");

        Message mss = StorageManager.shared().createEntity(Message.class);
        Thread thread = StorageManager.shared().createEntity(Thread.class);

        thread.setEntityID(uid);

        mss.setTextString(mesBody);
        mss.setText(mesBody);

        mss.setDate(new DateTime());
        User user = StorageManager.shared().createEntity(User.class);
        user.setEntityID(username);
        user.setName(title);
        mss.setSender(user);
        mss.setThread(thread);
        mss.setMessageType(MessageType.System);

        NotificationUtils.createMessageNotification(getApplicationContext(), mss);



    }




}
